# Python Reference

The python API is a useful tool to extend Anchorpoint and communicate with Anchorpoint from other applications. 
We offer 2 modules which perform different tasks.

The [apsync](modules/2-2-apsync.md) module is a standalone python module that can be used in Anchorpoint Actions as well as in other applications (if they support python) to exchange data and information between Anchorpoint and the server. Use apsync for example to:
- [Read and write attributes](modules/apsync/3-attributes.md)
- [Create new incremental versions](modules/apsync/7-versioncontrol.md)
- [Read and write settings](modules/apsync/6-settings.md)
- [Setting thumbnails](modules/apsync/5-thumbnails.md)

The [anchorpoint](modules/2-1-anchorpoint.md) module is an embedded python module which is only available in Anchorpoint Actions. It is the interface to the Anchorpoint Desktop client to extend it e.g. with own dialogs and actions or to query certain states like the currently selected files.

## 3rd Party Python Packages

Anchorpoint ships with it’s own Python interpreter to not depend on local installations. Hence, Anchorpoint is not using the locally installed python packages. Installing your own python packages for your actions is simple. Just [specify your required packages](https://docs.anchorpoint.app/docs/actions/yaml/#python-packages) in the action or packge YAML and Anchorpoint will install them, if needed.

## Working with Visual Studio Code

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/1fee1558fc914b1296fcb328792fb2e8" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

When working with python scripts it is highly recommended to use a python IDE (integrated development environment) such as the excellent and free [Visual Studio Code](https://code.visualstudio.com). VSCode offers an impressive amount of powerful extensions that simplify working with Anchorpoint's python API. After installing VSCode, we recommend you install the python extension from Microsoft.

![VSCode Python Extension](images/vscode_extension.png)

After that we need to tell VSCode were to find the python interpreter of Anchorpoint. Click Ctrl+Shift+P and type "Python: Select Interpreter". Click "Enter interpreter path" and provide the path to the python.exe (windows only): %APPDATA%/../Local/Anchorpoint/python.exe

Alternatively, you can install the apsync module for your local interpreter. Just copy the apsync module to your site-packages folder of your interpreter. You can find the apsync module here:

- On windows: %APPDATA%/../Local/Anchorpoint/app-*latestversion*/plugins/python/Lib/site-packages/apsync
- On mac: /Applications/Anchorpoint.app/Contents/Resources/python/lib/python3.9/site-packages/apsync

Now, you should be able to get a fully integrated documentation and auto completion for the anchorpoint and the apsync python module as well as the Anchorpoint pre-installed extensions such as PySide2:

![VSCode settings.json](images/vscode_intellisense.png)

Please note that VSCode will continue to complain "Import "anchorpoint" could not be resolved". This is due to the fact that the anchorpoint python module is embedded and not a standalone python extension. That being said, the auto completion engine of VSCode will still work as expected.


:::{eval-rst}
.. toctree::
    :glob:
    :maxdepth: 2
    :hidden:

    modules/2-1-anchorpoint.md
:::

:::{eval-rst}
.. toctree::
    :glob:
    :maxdepth: 2
    :hidden:

    modules/2-2-apsync.md
:::