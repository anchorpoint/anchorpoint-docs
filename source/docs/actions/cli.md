# CLI

Anchorpoint has a command line interface that can be used to control file locks, trigger Git commands or create custom scripts for pipeline development. The CLI can be run from the terminal or any other external application. 

## Path to executable

Windows: C:\Users\[USERNAME]\AppData\Local\Anchorpoint\app-[LATESTVERSION]\ap.exe

macOS: /Applications/Anchorpoint.app/Contents/Frameworks/ap

Note: You can also add the cli to your PATH if you want to access it more easily.

### Note on Anchorpoint updates

After each Anchorpoint update, the version number contained in the name of the Anchorpoint installation directory changes. If you are building a plugin that uses the CLI, make sure you add the appropriate logic to navigate to the latest version of Anchorpoint and select the CLI executable from there. 

```python
def get_executable_path()
	base_path = os.path.join(os.getenv('LOCALAPPDATA'), "Anchorpoint")
	
	pattern = r"app-(\d+\.\d+\.\d+)"
	cli_executable_name = "ap.exe"  
	
	# Get directories matching the pattern
	versioned_directories = [
		d for d in os.listdir(base_path)
		if re.match(pattern, d)
	] 
	
	# Sort directories by version
	versioned_directories.sort(key=lambda d: tuple(map(int, re.match(pattern, d).group(1).split('.'))), reverse=True)  
	
	if versioned_directories:
		latest_version_path = os.path.join(base_path, versioned_directories[0])
		cli_path = os.path.join(latest_version_path, cli_executable_name)
	return cli_path
```
An example written in Python how to get the proper CLI path

## CLI commands

In each call, you have to provide the current project directory of you Anchorpoint project, and then the commands. The basic structure is 

`ap [OPTIONS] [SUBCOMMAND]`

The following command is similar to a `git status` on your project, but it includes information about outdated and locked files:

`ap status`

By default, the CLI will look for the project in the current working directory. You can always overwrite your current working directory using the --cwd option, e.g.:

`ap --cwd "C:\Repositories\myGameProject" status`

### Options

|                |                                                                                                             |
| -------------- | ----------------------------------------------------------------------------------------------------------- |
| -h,--help      | Print this help message and exit                                                                            |
| --config       | Read an ini file.                                                                                           |
| --print-config | prints the command configuration that can be used with the --config parameter. Does not execute the command |
| --cwd TEXT     | Set the current project directory                                                                           |
| --json         | print output as json to stderr                                                                              |
| --apiVersion   | the version of the json output (and input). Set it to 1 to ensure consistency                               |

### Subcommands

|                                      |                                                                            |
| ------------------------------------ | -------------------------------------------------------------------------- |
| [sync](#sync-files)                  | Syncs the current state of the project with the server                     |
| [commit](#commit-files)              | Commits the current state of the project                                   |
| [status](#status)                    | Shows file status including locks and outdated files                       |
| [pull](#pull-files)                  | Pulls the latest changes from the server                                   |
| [push](#push-files)                  | Pushes the latest changes to the server                                    |
| [revert](#revert-files)              | Reverts changed files                                                      |
| [user](#list-all-users)              | User operations, most importantly user list                                |
| [lock](#file-locking)                | Lock operations, e.g create, list, remove                                  |
| [thumbnail](#get-and-set-thumbnails) | Thumbnail operations, e.g. set, get                                        |
| [config](#configuration)             | Update configuration values using set, get, and unset                      |
| [python](#run-python-script)         | Runs a python script with Anchorpoint context                              |
| [git](#run-any-git-command)          | Run any Git operation                                                      |
| [connect](#connect-with-ipc)         | Starts ap CLI as a daemon that connects to running instance of Anchorpoint |

A script would always pass `--json` to make parsing simpler. When JSON is enabled, it will output the JSON to stderr to separate the output from non json.
You can pass `—-cwd` to set the current project directory. That should usually be the project path where Anchorpoint also stores the .approj file. Note that an Anchorpoint project is required for most commands.
If you terminate ap.exe, the operation is cancelled gracefully. Some commands will output progress, see `sync` example.
Please make sure to always call the ap.exe with `—-apiVersion 1` so that the output protocol is guaranteed.
If an error occurs with `—-json` flag being set, the error is in JSON format as well.

Output
```
{"error": "whatever"}
```

## Examples

### Sync files

Syncs files to the server (this internally invokes Git commit, pull, and push including Anchorpoint's optimizations). It also releases locks.

#### Command
`sync`

#### Example
`ap —-json sync —-files “relative_path_to_file_A.png” “another_file.txt” —-message “Some commit message”`

It will output progress, again here with —-json flag. It has an optional progress-value from 0 to 100. The upload progress is currently only text based. The progress value for upload is number of file based. So uploading 4 files will jump from 0% to 25% if file 1 is uploaded - even if the other files are bigger or smaller.

Output
```json
{"progress-text": "Finding binary files"}
{"progress-text": "Staging files"}
{"progress-text": "Pushing Changes"}
{"progress-text": "Pushing Git Changes"}
{"progress-text": "Uploading Files: 0% (0/2), 0 B | 0 B/s"}
{"progress-text": "Uploading Files: 0% (0/2), 1.3 MB | 0 B/s", "progress-value": 0}
{"progress-text": "Uploading Files: 5% (0/2), 2.1 MB | 956 KB/s", "progress-value": 0}
```

There are equivalent commands if you only want to commit, push, or pull files. Just invoke `ap commit —-help`, for example

> **Caution**: The command line on Windows and Mac only allow a certain amount of characters to be passed. So if the user wants to sync e.g. 1000 files, this most likely won't work. We recommend to use the print-config command and replicate that format and read it with --config so that the sync command reads the files from the config and not through the terminal


### Commit Files

The commit command commits the current state of the project. It stages changes, adds binary file types as LFS automatically and creates a commit in the repository.

#### Command 
`commit`

#### Example
`ap —-files “relative_path_to_file_A.png” “another_file.txt” —-message “Some commit message”`

### Pull Files

The pull command fetches and integrates changes from the server into the local project. It also works when changed files are not committed yes as long as there are no conflicts.

#### Command 
`pull`

#### Example
`ap pull`

### Push Files

The push command uploads local changes to the server, updating the remote repository. Anchorpoint performs first a Git LFS push and then the Git push.

#### Command 
`push`

#### Example
`ap push`

### Status

Get status of files, locks, and outdated files (files that are newer on the server). 

#### Command 
`status`

#### Example 
`ap —-json status`

Output
```json
{
    "current_branch": "main",
    "staged": {},
    "not_staged": {
        "deleted_file.txt": "D",
        "modified_file.bin": "M",
        "conflicting_file.bin": "C",
        "added_file.dat": "A"
    },
    "locked_files": {
        "Assets/Blender/some.blend": "devs@anchorpoint.app",
        "image.png": "devs@anchorpoint.app"
    },
    "outdated_files": ["Assets/Blender/house.blend", "Assets/Blender/car.blend"]
}
```

You can optionally provide a list of paths to filter the status
`ap —-json status --paths "some fine file.blend" "assetFolder"`

You can also only show outdated files
`ap —-json status --outdated`

Output
```json
{
    "current_branch": "main",
    "outdated_files": ["Assets/Blender/house.blend", "Assets/Blender/car.blend"]
}
```

### Revert files

Reverts and unlocks provided files. If no files are provided it will revert ALL files.

#### Command
`revert`

#### Example
`ap revert --files "some_file.blend" "another.uasset"`

### List all users 

Lists all users of a given project with detailed information. The "current" user is the logged in user.

#### Command
`user list`

#### Example
`ap --json user list`

Output
```json
[
	{
		"id": "some id",
		"email": "peter@anchorpoint.app",
		"name": "Peter",
		"picture": "",
		"level": "admin",
		"pending": "0",
		"current": "1",
		},
		{
		"id": "another id",
		"email": "devs@anchorpoint.app",
		"name": "John",
		"picture": "",
		"level": "owner",
		"pending": "0",
		"current": "0",
	}
]

```

### File locking

Locks in Anchorpoint are usually handled automatically if the project is open in Anchorpoint and files are changed. That said you can lock and unlock files manually using the CLI.
You can also disable automatic locking for a file using the `config` command, see below.

#### Commands
`lock create`

`lock remove`

`lock list`

#### Example
`ap lock create --git —-files “a_file.bin” “another.txt”`

The `--git` flag is important, so that Anchorpoint knows to unlock the file automatically on push or revert

If you lock a file that is not yet changed, use `—-keep` to tell Anchorpoint to not automatically unlock the file as it's unchanged.

To remove locks, call
`ap lock remove —-files “a_file.bin” “another.txt”`

If you want to see all locks, either call `ap status` or `ap lock list`

### Get and set thumbnails

To update a thumbnail for a single file (`--file`) you can pass a detail and preview file. 

#### Commands
`ap thumbnail set`

`ap thumbnail get`

#### Example
`ap thumbnail set --file "some.uasset" —-detail “path to png” —-preview “path to other png”`

#### Another Example

If you ever need to, you can also get the thumbnails for files. 

`ap —-json thumbnail get --files "some.uasset" "another.unity"`

Output
```json
{
  "thumbnails": [
    {
      "filename": "some.blend",
      "thumbnails": [
        {
          "type": "preview",
          "path": "path to thumbnail png"
        },
        {
          "type": "detail",
          "url": "path to thumbnail png"
        }
      ]
    }, 
    {
      "filename": "another.unity",
      "thumbnails": []
    }
  ]
}
```

### Get project or file history

You could do this using git, but the ap cli offers a simpler shortcut.

#### Command

`log`

#### Example

`ap --json log --file "some.uasset" -n 10`

This will print the last 10 commits of a a file. The --file can be omitted to get the history of the project instead. If -n is omitted, it will print all commits (caution, can be a lot!). It's highly discouraged.

Output
```json
[
    {
        "commit": "8b614a5eb32aabca31345e094bfed4ea8ef4e415",
        "author": "devs@anchorpoint.app",
        "date": 1723551004,
        "message": "A commit message"
    },
    {
        "commit": "8d56be3aa601de72c1727314ea29bd4f9d2661f1",
        "author": "devs@anchorpoint.app",
        "date": 1723543736,
        "message": "A commit message"
    },
    {
        "commit": "97939fabdfb5215f6203cc8dc66c6d3529c396c7",
        "author": "devs@anchorpoint.app",
        "date": 1723282062,
        "message": "A commit message"
    }
]
```

### Configuration

You can get and set config values using to e.g. modify Anchorpoint's project settings.

#### Commands

`ap config get key`

`ap config set key value`

`ap config unset key`

#### Example
```
ap config set git-auto-lock false 
ap config get git-auto-lock
```

Setting git-auto-lock will toggle the Automatically lock Changed Files in the project settings.

#### Custom Configurations

You can also prefix keys to add them to your own settings instance.

`ap config set unreal/my_setting true`

You can access this config setting in python like so.

```
settings = apsync.Settings("unreal")
my_setting = settings.get("my_setting", False) # key, default
```

#### Disable automatic locking for paths

You can also disable automatic locking just for a specific path in your project. The Unreal Engine plugin disables automatic locking for it's project directory, for example

`ap config set unreal/git-auto-lock-path false`


### Run python script

You can use the built-in Anchorpoint Python interpreter to run arbitrary scripts. Our interpreter comes with built-in Anchorpoint context and anchorpoint and apsync Python modules.

#### Command

`ap python --script “absolute or relative path to script” --args 42 "test"`

Relative paths are scripts within your project. So if you call

`ap --cwd "C:\Repositories\MyGame" python --script “scripts\publish.py”` 

The script C:\Repositories\MyGame\scripts\publish.py is called.
Here is an example script, it can be called like this `ap python --script example.py --args arg1 arg2`

```python
import sys
import anchorpoint
import apsync

def main():
    print("ap python example")
    print("len(sys.argv):", len(sys.argv))
    print("sys.argv:", sys.argv)

    ctx = anchorpoint.get_context()
    print("anchorpoint context:", ctx)

    project = apsync.get_project(ctx.path)
    if project:
        print("anchorpoint project:", project.name)
    else:
        print("no anchorpoint project found")


if __name__ == "__main__":
    main()
```

### Run any Git command

It might be necessary to invoke git commands directly. You cannot rely on installed git versions, as some users only have Anchorpoint. Using our provided git version ensures a correct git LFS setup and injects git credentials automatically.

#### Command

`ap git -c “git commands”`

#### Example

If you need to pass quoted paths, use outer double and inner single quotes.

`ap git -c "checkout -- 'Content/Characters/Character Main.uasset'"`

### Connect with IPC

Using this command will connect your plugin to Anchorpoint using a constant connection. It will create a subprocess that is running constantly as a daemon, attached to your main application.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/266352e9e1524fb59a89952dd4423596?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

#### Command

`ap connect`

#### Example

`ap connect —-name “blender”`

The connect command will print information or commands to stdin (note: not stderr) as JSON.

The command is printing information about locked and outdated files as well as if the project is currently selected in Anchorpoint and if it's dirty (files have changed) (example using `—-pretty` flag):

```json
{
    "files": [
        "car.mtl"
    ],
    "id": "496c13f5-450c-4a83-8d63-84e49bc02c77",
    "type": "files locked"
}

{
    "files": [
        "car.mtl"
    ],
    "id": "2c24b517-9c5b-48c4-98a7-c5b8e4ed5d4f",
    "type": "files unlocked"
}

{
    "files": [
        "car.mtl"
    ],
    "id": "924f6de7-b753-4573-8c21-d429e4f990d2",
    "type": "files outdated"
}

{
    "files": [
        "car.mtl"
    ],
    "id": "c6b3d024-dd80-495d-b16f-987bc482247e",
    "type": "files updated"
}

{
    "id":"b0aebab6-985c-4deb-b23a-a01c773fb923",
    "type":"project opened"
}

{
    "id":"3caf8a74-67e4-4d14-8116-cd9651144488",
    "type":"project closed"
}

{
    "id":"7e07886a-3cc8-4a3b-ad03-407b25644fb1",
    "type":"project dirty"
}
```

This is only for your information and motivates you to call ap status for example to update the plugin UI.

A brief description of each info type:

|                |                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------- |
| files locked   | Files have been locked                                                                                  |
| files unlocked | Files have been unlocked                                                                                |
| project opened | The current project is currently opened in Anchorpoint. This enables files outdated, updated, and dirty |
| project closed | The project is not selected in Anchorpoint. files outdated, updated, and dirty are disabled             |
| files outdated | Files are outdated, aka they are not in sync with the state on the server                               |
| files updated  | The files are in sync with the server again                                                             |
| project dirty  | Some files in the project have changed. Good idea to call ap status command again                       |

> **A note about caching:** When “project opened” is emitted it is save to cache state in the integration plugin. You can cache all locked and outdated files as well as git modification state. It can be as simple as caching the result of ap status and re-populate the cache with ap status when an update info (locked, unlocked, outdated, updated, dirty) is emitted. When “project closed” is emitted, you can only cache locks, they are emitted independent of a project.

#### Command actions

When running connect command, there are a few optional parameters that will tell Anchorpoint that the plugin can perform certain **actions**. All actions assume a JSON response from the plugin. If the response is not received within 5s, the commands ignores the plugin response.

To respond to a command provide this to stdin, this will tell Anchorpoint that you did perform the requested action.

Positive Response
```json
{"id":"924f6de7-b753-4573-8c21-d429e4f990d2"}
```

If you cannot perform the action (e.g. your game engine is in an invalid state), provide an error

Error Response
```json
{"id":"924f6de7-b753-4573-8c21-d429e4f990d2", "error":"Engine is in play mode"}
```

The actions are the following:

`ap connect -n “engine” —-projectSaved`

Anchorpoint will ping the plugin to verify that files are saved (or all files if files argument is missing).

```json
{
	"files": [
	"car.mtl"
    ],
    "id": "924f6de7-b753-4573-8c21-d429e4f990d2",
    "type": "project saved"
}
```

If the project is saved, response positive, if not respond with an error. 
Ideally, make sure that the error message follows this pattern: "foo.uasset is unsaved\nfolder/bar.uasset is unsaved"
This will allow Anchorpoint to present a custom dialog to the user instead of just an error toast.

`ap connect -n “engine” —-changedFiles`

Anchorpoint will ping the plugin with “files about to change” and “files changed”. Use this to e.g. unlink files in your application so that they can be overwritten and reloaded accordingly

```json
{
    "files": [
        "car.mtl"
    ],
    "id": "924f6de7-b753-4573-8c21-d429e4f990d2",
    "type": "files about to change"
}
```


`ap connect -n "unity" —-thumbnails "unity" "whatever"`

Anchorpoint will ping the plugin to provide thumbnails for files that match the specified extensions

```json
{
    "files": [
        "car.unity"
    ],
    "id": "924f6de7-b753-4573-8c21-d429e4f990d2",
    "type": "request thumbnails"
}
```

The plugin should set the thumbnails using the `ap thumbnail set` command and then respond positively.