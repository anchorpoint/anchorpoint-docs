# Action Packages

You can package a set of actions in an **action package**. Action packages have 2 main advantages:

- They can be distributed using the Action Distribution System
- They can be enabled and disabled for the entire workspace

To define an action package set the type to **package** and provide a list of **actions**.

```yaml
version: 1.0
action:
  name: Example Package

  id: ap::package::example
  type: package
  enable: false
  author: Anchorpoint Software GmbH
  description: An example package
  details: '
    An example package that demonstrates how to create packages.
    The detail section supports basic <b>HTML</b>:
    '
  icon:
    path: an_icon.svg

  actions:
    - example::action::id
    - another::example::id

  platforms:
    - mac
    - win
```

The YAML for a package features 2 main differences to a normal action YAML:
- The type is set to "package"
- A list of action ids is provided through the "actions" key

For workspace owners or administrators the packages show up in the Action settings for your workspace:

![image](../images/examplePackage.png) 