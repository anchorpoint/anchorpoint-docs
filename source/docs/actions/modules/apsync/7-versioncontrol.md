# Version Control

:::{eval-rst}
.. currentmodule:: apsync

.. autofunction:: toggle_version_control
.. autofunction:: comment_file
.. autofunction:: create_next_version
.. autofunction:: get_next_version_path
    
:::