# Api

:::{eval-rst}
.. currentmodule:: apsync

.. autofunction:: get_api

.. autoclass:: Api
    :members: 
:::