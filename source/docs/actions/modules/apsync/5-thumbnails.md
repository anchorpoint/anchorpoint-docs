# Thumbnails

:::{eval-rst}
.. autofunction:: apsync.attach_thumbnail
.. autofunction:: apsync.get_thumbnail
.. autofunction:: apsync.generate_thumbnails
.. autofunction:: apsync.generate_thumbnail
:::
