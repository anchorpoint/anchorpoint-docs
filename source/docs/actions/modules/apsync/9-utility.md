# Utility

:::{eval-rst}
.. currentmodule:: apsync

.. autoclass:: Icon
    :members: 

.. autoclass:: ApiVersion
    :members: 

:::