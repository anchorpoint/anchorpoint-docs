# Tasks

The Tasks class can be used to to create and manage tasks and list of tasks. Use the apsync.Api object to access a set up instance.

:::{eval-rst}
.. currentmodule:: apsync

.. autoclass:: Tasks
    :members: 

.. autoclass:: Task
    :members: 

.. autoclass:: TaskList
    :members: 

:::
