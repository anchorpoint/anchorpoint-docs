# Attributes

The Attributes class can be used to set and read attributes on files, folders, and tasks. Use the apsync.Api object to access a set up instance.
A fully-fledged example that sets attributes on all selected files and folders can be found on [GitHub](https://github.com/Anchorpoint-Software/ap-actions/blob/main/examples/attributes/create_attributes.py).

:::{eval-rst}
.. currentmodule:: apsync

.. autoclass:: Attributes
    :members: 

.. autoclass:: AttributeType
    :members: 

.. autoclass:: Attribute
    :members: 

.. autoclass:: AttributeTag
    :members: 

:::

## Copy and Rename

When copying or renaming files and folders in Anchorpoint, metadata such as attributes are adjusted on the server. Unfortunately, this information is lost when doing rename and copy operations through the OS. Instead, use the following utility functions:

:::{eval-rst}
.. autofunction:: copy_from_template
.. autofunction:: copy_file_from_template
.. autofunction:: copy_file
.. autofunction:: copy_folder
.. autofunction:: rename_file
.. autofunction:: rename_folder
.. autofunction:: resolve_variables

:::
