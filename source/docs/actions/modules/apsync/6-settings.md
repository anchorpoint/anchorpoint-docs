# Settings

The Settings class is used to store settings for your python scripts and YAML files. Settings are always stored per Anchorpoint user account. Find a fully-fledged example for settings on [GitHub](https://github.com/Anchorpoint-Software/ap-actions/blob/main/examples/settings/settings_example.py).
In contrast to Settings, SharedSettings are stored for the entire workspace instead.

:::{eval-rst}
.. currentmodule:: apsync
.. autoclass:: Settings
    :members: __init__, set, get, store, remove, clear, contains

.. autoclass:: SharedSettings
    :members: __init__, set, get, store, remove, clear, contains
:::