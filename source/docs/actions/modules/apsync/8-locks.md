# File Locking

:::{eval-rst}
.. currentmodule:: apsync

.. autoclass:: Lock
    :members:

:::
