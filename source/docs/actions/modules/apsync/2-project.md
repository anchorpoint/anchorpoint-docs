# Project

:::{eval-rst}
.. currentmodule:: apsync

.. autofunction:: get_project
.. autofunction:: get_projects
.. autofunction:: is_project
.. autofunction:: remove_project

.. autoclass:: Project
    :members: get_metadata, update_metadata
:::