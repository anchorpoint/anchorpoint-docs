# Event Hooks

Beside writing powerful actions that extend the UI of Anchorpoint through a [registration](https://docs.anchorpoint.app/docs/actions/yaml/#register), Anchorpoint offers a set of event hooks that can be implemented in Python. Every time a certain event is happening within the application, a Python callback is triggered.

To get started, simply implement one of the following callback functions within an existing action. Of course, you can also create a new action - a registration is not required. Each callback gets passed a unique set of arguments. One argument is the same for all actions: The current __anchorpoint.Context__ object which contains the state of the current active browser state.

:::{eval-rst}

.. note:: 

   By loading up a Python script, the entire non-guarded code is executed, hence you might observe unwanted side effects if you don't **guard** your code.
   
   This becomes especially important when mixing event hook callbacks with *normal* action code make sure to **guard** your *normal* code using the infamous **if \_\_name\_\_ == \_\_main\_\_:** syntax.
   
   Example::
      
      import anchorpoint as ap
   
      def on_timeout(ctx: ap.Context):
         # This code will be automatically executed once every minute
         print("timeout callback")

      if __name__ == __main__:
         # This code will be executed when the user triggers the action
         print("Action Invoked!")

:::

### on_timeout

:::{eval-rst}

..  py:function:: on_timeout(ctx: anchorpoint.Context) -> None

    This function is called once every minute. Userful to check certain state on the file system, for example.

    Example:
    
        >>> def on_timeout(ctx: ap.Context):
        >>>     # This code will be automatically executed once every minute
        >>>     print("timeout callback")

:::

### on_folder_opened

:::{eval-rst}

.. py:function:: on_folder_opened(ctx: anchorpoint.Context) -> None

   This function is called whenever the user navigates to a folder in Anchorpoint.

   Example:
      
        >>> def on_folder_opened(ctx: ap.Context):
        >>>     print("folder opened: " + ctx.path)

:::

### on_project_directory_changed

:::{eval-rst}

.. py:function:: on_project_directory_changed(ctx: anchorpoint.Context) -> None

   Called whenever a change within the project directory is detected, e.g. when a file was modified. This only works for the active project in Anchorpoint.

   Example:
      
        >>> def on_project_directory_changed(ctx: ap.Context):
        >>>     print("something has changed")

:::

### on_application_started

:::{eval-rst}

.. py:function:: on_application_started(ctx: anchorpoint.Context) -> None

   Called once for each workspace on startup of Anchorpoint.

   Example:
      
        >>> def on_application_started(ctx: ap.Context):
        >>>     print("application has started")

:::

### on_application_closing

:::{eval-rst}

.. py:function:: on_application_closing(ctx: anchorpoint.Context) -> None

   Called just before Anchorpoint is closing. 

   Example:
      
        >>> def on_application_closing(ctx: ap.Context):
        >>>     print("application is closing")

:::

### Task Hooks

:::{eval-rst}

.. py:function:: on_task_created(task_id: str, source: anchorpoint.ChangeSource, ctx: anchorpoint.Context) -> None

   Called when a task has been created.

   Example:
      
      >>> def on_task_created(task_id, src, ctx):
      >>>    api = ap.get_api()
      >>>    task = api.tasks.get_task_by_id(task_id)
      >>>    print(f"Task {task.name} created")

.. py:function:: on_task_changed(task_id: str, source: anchorpoint.ChangeSource, ctx: anchorpoint.Context) -> None

   Called when a task has been changed.

.. py:function:: on_task_removed(task_id: str, source: anchorpoint.ChangeSource, ctx: anchorpoint.Context) -> None

   Called when a task has been removed.

.. currentmodule:: anchorpoint

.. autoclass:: ChangeSource

:::

### on_attributes_changed

:::{eval-rst}

.. py:function:: on_attributes_changed(parent_path: str, attributes: list[anchorpoint.AttributeChange], ctx: anchorpoint.Context) -> None

   This function is called when attributes have changed. The list of :class:`~anchorpoint.AttributeChange` is provided for a specific **parent_path**.
   You can check the :class:`~anchorpoint.ChangeSource` of the attribute change. The source is :class:`~anchorpoint.ChangeSource.You` if you have changed the attribute in the Anchorpoint UI, for example.

   Example:
      
       >>> def on_attributes_changed(parent_path: str, attributes: list[ap.AttributeChange], ctx: ap.Context):
       >>> print("attributes have changed")
       >>>     for attribute in attributes:
       >>>        if attribute.task_id:
       >>>             task = ap.get_api().tasks.get_task_by_id(attribute.task_id)
       >>>             print(f"attribute of name {attribute.name} has changed from {attribute.old_value} to {attribute.value} for object {task.name}")
       >>>        else:    
       >>>             print(f"attribute of name {attribute.name} has changed from {attribute.old_value} to {attribute.value} for object {attribute.path}")
    
.. currentmodule:: anchorpoint

.. autoclass:: AttributeChange

:::

### on_is_action_enabled

This is a special event hook that is called when Anchorpoint checks whether or not an action should be presented to the user. 
To make this hook work you have to tell Anchorpoint in the YAML when to call the hook:

```yaml
register:
    folder:
        enable: script_with_hook.py
```

:::{eval-rst}

.. py:function:: on_is_action_enabled(path: str, type: anchorpoint.Type, ctx: anchorpoint.Context) -> bool

   Args:
        path (str): The path that should be used to check if the action is enabled. Might be different to ctx.path
      
        type (:class:`~anchorpoint.Type`): The type of the action

        ctx (:class:`~anchorpoint.Context`): The context of the current browser

   Example:
    
        >>> def on_is_action_enabled(path: str, type: anchorpoint.Type, ctx: anchorpoint.Context) :
        >>>     # Enable the action when the path contains the word "shot"
        >>>     return "shot" in path:

:::