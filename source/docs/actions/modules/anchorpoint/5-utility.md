# Utility

:::{eval-rst}
.. currentmodule:: anchorpoint

.. autofunction:: check_application
.. autofunction:: temp_dir
.. autofunction:: copy_files_to_clipboard
.. autofunction:: get_application_dir

:::
