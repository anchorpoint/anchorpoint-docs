# User Interface

## Dialogs
:::{eval-rst}
.. currentmodule:: anchorpoint

.. autoclass:: anchorpoint.Dialog

    .. automethod:: add_text
    .. automethod:: add_input
    .. automethod:: set_browse_path
    .. automethod:: add_info
    .. automethod:: add_checkbox
    .. automethod:: add_switch
    .. automethod:: add_dropdown
    .. automethod:: add_button
    .. automethod:: add_separator
    .. automethod:: add_empty
    .. automethod:: start_section
    .. automethod:: end_section
    .. automethod:: show(settings: apsync.Settings, store_settings_on_close: bool)
        
        Shows the Dialog to the user. You can optionally provide a :class:`~apsync.Settings` object to persist the user input.

                Examples:
                    >>> dialog = anchorpoint.Dialog()
                    >>> dialog.add_text("Hello Anchorpoint Users")
                    >>> settings = apsync.Settings()         
                    >>> dialog.show()

    .. automethod:: show(settings: apsync.SharedSettings, store_settings_on_close: bool)
        :noindex:
        
        Shows the Dialog to the user. You can optionally provide a :class:`~apsync.SharedSettings` object to persist the user input.

                Examples:
                    >>> dialog = anchorpoint.Dialog()
                    >>> dialog.add_text("Hello Anchorpoint Users")
                    >>> settings = apsync.Settings(project.workspace_id, "Blender Action Settings")                    
                    >>> dialog.show()

    .. automethod:: close
    .. automethod:: get_value
    .. automethod:: set_value
    .. automethod:: set_enabled
    .. automethod:: hide_row
    .. automethod:: store_settings

    .. autoproperty:: title
    .. autoproperty:: icon
    .. autoproperty:: icon_color
    .. autoproperty:: callback_closed

.. autoclass:: anchorpoint.BrowseType
:::

## Utility
:::{eval-rst}
.. currentmodule:: anchorpoint

.. autoclass:: UI

    .. automethod:: show_success
    .. automethod:: show_error
    .. automethod:: show_info
    .. automethod:: navigate_to_folder
    .. automethod:: create_tab
    .. automethod:: open_tab
    .. automethod:: reload
    .. automethod:: replace_thumbnail
    .. automethod:: replace_thumbnail_tool
    .. automethod:: show_busy
    .. automethod:: finish_busy
    .. automethod:: show_console
:::

