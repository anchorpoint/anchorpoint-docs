# Progress

:::{eval-rst}
.. currentmodule:: anchorpoint

.. autoclass:: Progress
    :members: __init__, set_text, report_progress, finish

:::
