# Context

:::{eval-rst}
.. currentmodule:: anchorpoint

.. autofunction:: get_context
.. autofunction:: get_api

.. autoclass:: Context
    :members: run_async, create_project, install

.. autoclass:: Type

:::
