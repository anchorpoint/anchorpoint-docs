# File Locking

:::{eval-rst}
.. currentmodule:: anchorpoint

.. autofunction:: lock
.. autofunction:: unlock
.. autofunction:: update_locks
.. autofunction:: get_locks
    

:::
