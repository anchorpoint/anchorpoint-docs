# Read Attributes

In this example we write a Python script that can read attributes. If you want to use IntelliSense for this, have a look at this [description](../python.md) first.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/7db7e9a39caa459fac817a47b9f62fed?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

## Files
The files are placed in the **attributes** folder. This is located in the Action repository, which can be found via **Workspace Settings / Actions**. In the action list you can use the **Show code** button to get to the action codes.

### YAML
We register the action on the file and folder context menu. 

```yaml
version: 1.0
action:
  name: Example / Read Attributes

  version: 1
  id: ap::examples::attributes::read
  category: utility/code/examples/attributes
  type: python
  enable: false
  author: Anchorpoint Software GmbH
  description: Example action to demonstrate how to read all kinds of attributes
  icon:
    path: :/icons/action.svg

  script: read_attributes.py

  register:
    file: 
      enable: true
    folder:
      enable: true
```

### Python
We use the `get_context()` module to get the file paths, the `UI()` module to display the toast and the `get_api()` module to read the attributes database.

```python
import anchorpoint

ctx = anchorpoint.get_context()
api = anchorpoint.get_api()
ui = anchorpoint.UI()

#Get the current selection of files and folders
selected_files = ctx.selected_files
selected_folders = ctx.selected_folders

def read_attribute(path):
    #Get all attributes in the project (everything what is under "Recent Attributes")
    proj_attributes = api.attributes.get_attributes()
    #Collect the output in a string
    output = ""

    #Get the Attribute field of the file/folder
    for attribute in proj_attributes:
        atttribute_value = api.attributes.get_attribute_value(path,attribute.name)

        #If the Attribute field is not empty, add it to the output string. Add a linebreak at the end
        if(atttribute_value is not None):
            output += attribute.name + ": " + str(atttribute_value) +"<br>"

    #Show a toast in the UI
    ui.show_info("Attributes",output)


for f in selected_files:
    read_attribute(f)

for f in selected_folders:
    read_attribute(f)
```