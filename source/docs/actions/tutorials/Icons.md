# Icons

You can set icons with Python and use Anchorpoint's icons from the icon picker. These icons can be used in your YAML file for setting the icon in the menu entry or in Python, if you would like to create a dialog and you need an icon. 

```yaml
icon:
  path: :/icons/hardDrive.svg
```

That way you can create icons in e.g. context menus by modifying the YAML file

<img class="content-image-middle" src="../../images/icons.webp">

Do a right click on any icon and copy it's filepath.