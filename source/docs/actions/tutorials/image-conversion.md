# Image Conversion

In this example we will create an image converter that exports PNGs from PSD and EXR images and converts them to the same file format. We use the Commanline application [Image Magick](https://imagemagick.org/script/index.php) for this purpose.

## The simple way
This will cost us less than 10 minutes

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/c2fd47836fb64c0384b3326bf8629a3e?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

1. First we need to download Image Magick. For us, the portable version that can be downloaded here for [Windows](https://imagemagick.org/script/download.php#windows) and [macOS](https://imagemagick.org/script/download.php#macosx) is enough.

2. we go to the folder Documents/Anchorpoint/actions and create a new folder "ImageMagick". There we add the magick.exe and create an empty YAML file, named "convertImage.yaml".

![image](../images/imageMagickYAML.png) 

3. Copy the following content into your "convertImage.yaml. Make sure that the indentations are correct. 

```yaml
version: "1.0"

#Action body
action: 
  #Base parameters
  name: "Convert to PNG"
  id: "ap::imageMagicConvert"
  type: command

  #The parameters for Image Magick
  command: "${yaml_dir}/magick.exe" 
  arguments: "convert ${path}[0] -colorspace sRGB ${folder}/${filename}.png"

  #Where should the action appear in the context menu
  register:
    file: 
      filter: "*.psd;*.exr;"

  #Handle toast messages
  toast:
    success:
      message: "Image conversion finished"
    error:
      message: "Something went wrong"    
```

**version**:<br/>
The version of the action <br/><br/>
**name**<br/>
The name of the action, which is shown in the context menu<br/><br/>
**id**<br/>
The action id, which has to be unique<br/><br/>
**type**<br/>
This is either command (for CLI applications) or python (for Python scripts)<br/><br/>
**arguments**<br/>
The parameters which are required to control the CLI application. To enter the proper arguments you have to read the documentation of the CLI application.<br/><br/>
**register**<br/>
Here you specify where the action can be called. In this example we use "file" for files and filter the files with file extensions "psd" and "exr".<br/><br/>
**toast**<br/>
The message that is displayed when the action is completed.

