# Creating Actions

> ℹ️ Requires a Team plan

With just a little scripting knowledge you can quickly build small automation tools that speed up the workflow in your studio. The best way to do this is to start with our examples.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/3701c24e4d8447fe86fcc7b1892b801b?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

### Start with our examples
You can find a number of Actions examples on our [GitHub](https://github.com/Anchorpoint-Software/ap-actions) page, which you can download and configure freely. The following examples are included:

- [Convert files with FFmpeg](https://github.com/Anchorpoint-Software/ap-actions/tree/main/ffmpeg)
- [Create folder structure templates](https://github.com/Anchorpoint-Software/ap-actions/tree/main/template)
- [Creating a custom UI](https://github.com/Anchorpoint-Software/ap-actions/tree/main/examples/ui)
- [Setting attributes](https://github.com/Anchorpoint-Software/ap-actions/tree/main/examples/attributes)
- [Creating a referenced file from incremental saves](https://github.com/Anchorpoint-Software/ap-actions/tree/main/publishVersion)
- [Mapping a folder to a drive letter on Windows](https://github.com/Anchorpoint-Software/ap-actions/tree/main/drives)

Executables (e.g. FFmpeg or ImageMagick) must be downloaded from the respective website.

## Distribution

The **Workspace Action Distribution System** is the hub for the workspace owner and admins to control what actions are accessible by the team. 

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/669c5a2dcb7f475e85a86665d08be39e?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

By default, Anchorpoint adds the [Anchorpoint action repository](https://github.com/Anchorpoint-Software/ap-actions) from GitHub to every workspace. To adjust what actions are enabled in your workspace or to modify the repositories open the action settings dialog.  
You can find the settings for the action distribution on the action settings of your workspace:

When adding a new Action repository you can either provide a link to a public GitHub repository (e.g https://github.com/Anchorpoint-Software/ap-actions) or to a local folder. 

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Tutorials
   :glob:

   tutorials/*
:::