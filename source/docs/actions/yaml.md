# YAML Reference

The YAML file describes how an action behaves in Anchorpoint, registers it and can control Python scripts or command line applications. Each action needs a YAML file. The YAML file is declaratively described with key - value pairs.

```yaml
action: 
  name: "Convert Video to mp4"  
  version: 1
  id: "ap::ffmpeg::avitomp4"
  category: "examples/ffmpeg"
  type: command
  author: "Anchorpoint Software GmbH"
  description: "Creates an mp4 from an avi"

  command: "ffmpeg.exe" 
  arguments: "-i ${path} ${root}/03_Export/Videos/yamlExample.mp4"

  register:
    file: 
      filter: "*.avi*"

  toast:
    success:
      message: "Video successfully created."
    error:
      message: "Error when creating video."
```
This example uses [FFmpeg](https://www.ffmpeg.org/) to convert an avi file into an mp4 video.

## Triggers in Anchorpoint

You can trigger actions from various places in the UI. To add triggers in the YAML file, scroll down to the "register" section.

![](images/yaml_register_1.png)

Register actions for `sidebar` , `new_task`, `new_folder` and `new_file`. On the sidebar, the name of the action will appear as a button, for the "new" registrations, the buttons will appear each in a context menu.

<img class="content-image-middle" src="images/yaml_register_2.png">

Opening the context menu for `new_task`, `new_folder` and `new_file`. All the actions registered under these types, will be listed here.

<img class="content-image-small" src="images/yaml_register_6.png">

Here you can register `workspace_overview` actions and `new_drive` actions. The `drive` register is an entry in the context menu when you do a right-click on a drive.

<img class="content-image-middle" src="images/yaml_register_5.png">

Examples for registering the `file` action, which will show up in the context menu of a file.


<img class="content-image-middle" src="images/yaml_register_3.png">

Examples for a `folder` action, which will show up in the context menu of a folder.


## Basic Keys

- [name](#name)
- [id](#id)
- [version](#version)
- [category](#category)
- [type](#type)
- [command](#command)
- [arguments](#arguments)
- [script](#script)
- [author](#author)
- [description](#description)
- [details](#details)
- [registration](#register)

## Advanced Keys

- [inputs](#inputs)
- [platforms](#platforms)
- [python_packages](#python-packages)
- [toast](#toast)

## Variables

- [path](#path)
- [relative_path](#relPath)
- [project_path](#root)
- [yaml_dir](#yaml-dir)
- [folder](#folder)
- [filename](#filename)
- [suffix](#suffix)


## Basic Keys
See the following list to learn about the basic keys that can be used to define your action.

### name
Provides the name that shows up in Anchorpoint e.g. in the context menu
```yaml
action:
  #This will appear in the UI
  name: "Convert Video to mp4"
```


### id
A unique identifier. It is generally a good idea to provide a "namespace" for your action so that there is a lower chance to clash with Actions provided by other users.
```yaml
action:
  id: "ap::ffmpegConvertVideo"
```

### version
A number that is used to identify the version of your action - very useful if you have to keep around multiple versions of the same action.
```yaml
action:
  version: 1
```
default: 1

### category
You can categories your action so that it appears grouped in the Anchorpoint UI.
```yaml
action:
  category: "examples/ffmpeg"
```
default: "general"

### type
What type of action, either "command", "python", or "package".


### command
The path to the executable of a command line application like [FFmpeg](https://www.ffmpeg.org/) (e.g. ffmpeg.exe) if the action is of type "command". The path can be absolute or relative to the yaml file.


### arguments
The variables to be passed to the command line application. In this example, instead of an absolute path, we take the [${path}](#path) variable, which returns us the file we selected in Anchorpoint. <br />
We also use the [${root}](#root) variable, which gives us the path of the project.But this also means that this action can only be called in a project that contains a "03_Export/Videos" folder. 
```yaml
action:
  type: command
  command: "ffmpeg.exe" 
  arguments: "-i ${path} ${root}/03_Export/Videos/yamlExample.mp4"
```

### script
The path to the Python script if the action is of type "python". It expects a Python 3.9 script. The path can be absolute or relative to the yaml file.
```yaml
action:
  type: python
  script: "save_as_template.py"
```

### author
Who created this action.
```yaml
action:
  author: "Anchorpoint Software GmbH"
```

### description
What does your action do?.
```yaml
action:
  author: "Create a video from all files that are sharing the same suffix"
```
### details
A detailed description of your action. Currently this is only used for packages. The content supports basic [HTML](https://doc.qt.io/qt-5/richtext-html-subset.html).
```yaml
action:
  author: "Create a video from all files that are sharing the same suffix"
```

### register
Specify where your action should be registered within Anchorpoint. Actions can be registered for the:
- file and folder context menu
- for the new file, folder, and drive buttons
- the left sidebar in the browser.
Use the filter wildcard key to only register the action if the filter matches. Separate multiple filters with a semicolon.
```yaml
register:   
  file:       
    filter: "*.png;*.exr" #Wildcard matching    
  folder:      
    filter: "*/assets/*"       
  new_file:
    enable: true
  new_folder:
    filter: "*/shots/*" 
  new_task:
    enable: true
  drive:
    enable: true
  new_drive:
    enable: true
  sidebar:
    enable: true
  workspace_overview:
    enable: true
```

## Advanced Keys
This section handles more advanced YAML keys that can be used to specify how your action functions exactly.


### inputs
A list of inputs for your action that can be defined in YAML and read in Python. Here it is worthwhile to store parameters, such as paths or certain settings, so that you don't have to modify your python script. 
```yaml
inputs:     
  URL: "anchorpoint.app"    
  target: ${root}/03_Export/Videos
```

You can also show basic prompts to ask the user for input. See more examples [here](https://github.com/Anchorpoint-Software/ap-actions/blob/main/distribution/examples/action%20input/action_input_example.yaml).
```yaml
inputs:     
  URL: 
    message: "What website do you like the most?"
    default: "anchorpoint.app"  
```

### platforms
By default, actions are supported on all platforms. With the **platforms** key you can explicitly list the supported platforms:
```yaml
platforms:    
  - win
  - mac
```

### python_packages
Anchorpoint ships with it's own Python interpreter to not depend on local installations. Hence, Anchorpoint is not using the locally installed python packages.
Installing your own python packages for your actions is simple. Just specify your required packages in the action or packge YAML and Anchorpoint will install them, if needed.
```yaml
python_packages:    
  - pillow
  - opencv-python
```


### toast
With the toast key you can control whether or not Anchorpoint shows a toast message on success or error. By default, Anchorpoint will show no message at all. In python you can fire up toasts directly.
```yaml
toast:    
  success:      
    enable: false    
  error:      
    message: "Could not export file, please check your file system"
```

## Variables
Within the YAML file you can access predefined and environment variables as well as defined inputs easily with the ${var} syntax.
```yaml
inputs:     
  blender_path: "${BLENDER_DIR}/blender.exe" #environment variable BLENDER_DIR
command: ${blender_path}
arguments: --scene ${path} #access the predefined path variable
```

### path
The absolute path to the file or folder.
```yaml
inputs:     
  URL: "anchorpoint.app"    
  target: ${path}/ExportedFiles
```

### relative_path
Path to the file or folder relative to the active project.

### project_path
The project root folder.
```yaml
inputs:     
  URL: "anchorpoint.app"    
  target: ${project_path}/03_Export/Videos
```

### yaml_dir
The directory containing this YAML file.
```yaml
action:     
  script: "${yaml_dir}/sbsToTextures.py"
```

### folder
The active folder where you executed that action.

### filename
The active filename without the path and without the suffix.

### suffix
The suffix of the file, e.g. PNG.

:::{eval-rst}
.. toctree::
    :glob:
    :maxdepth: 2
    :hidden:

    packages/packages.md
:::