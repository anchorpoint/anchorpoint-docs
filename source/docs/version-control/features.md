# Git next steps

> ℹ️ Requires a Team plan

## Git LFS
Anchorpoint uses [Git LFS](https://git-lfs.github.com/) for binary data. Basically, anything that is not a plain text file is a binary file and is versioned via Git LFS. You don't need to install any additional plugins. LFS data is not put into the Git repository, but into a separate cache. There is no limit to the size of a single file.

### What if something goes wrong
Version control systems can feel like black boxes because all file versions are stored in a cache folder. Sometimes you also get into states that cannot be fixed without command line commands. You can open the command line application from any folder within your Git repository. Git has [several commands](features/commands.md). It's best to have someone on your team who knows Git and can help you. Otherwise, you can always ask us on Discord.

### gitignore
The gitignore filters file types and folders from the version tracking. They are also never uploaded to the server. These are often cache files or temporary files created by the game engine.

Anchorpoint provides presets for the gitignore. You can also create your own presets. It will also check if a gitignore is available in the project and display a warning if it is not.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/9e04b98976b242bcb5b30c96fd3aae45?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Check-in/check-out files
If you are coming from tools like Perforce, you may be familiar with the check-in/check-out workflow. Git has no such concept. You just edit your files and commit them. A commit is like a changelist in Perforce. 

Usually check-in/check-out workflows are used to lock files. You can also do this in Anchorpoint by right-clicking on a file in the browser and clicking "Lock".

### File locking
File locking is an essential feature when collaborating in Unreal/Unity or Godot projects. With file locking you prevent multiple team members from working on the same scene. Anchorpoint automatically locks files when the first person saves them. For all others these files are then locked. [Learn more about file locking](../general/workspaces-and-projects/file-locking.md).

## Use an existing Git repository
In this video, you'll learn how to clone a git repository from platforms like GitLab, GitHub, or Azure DevOps into Anchorpoint. The process involves creating a new project in Anchorpoint, selecting the git repository option, and entering the repository's HTTPS link. Once the repository is downloaded, you can make changes and push them to Anchorpoint. Watch the video for a step-by-step guide.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/822707b2bbae4da1b32ea6ee4bd65fac?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Storage requirements
Because Git is a distributed version control system, it keeps a copy of the entire version history on your hard drive. If you have used other Git desktop applications, you will find that you need at least twice as much disk space as your project, plus additional space for committing new versions. 
Unlike other Git applications, Anchorpoint cleans up the storage overhead for file versions older than x days. You can adjust the interval period in the Workspace Settings / Git.

<img class="content-image-large" src="../images/git_prune.webp">

You can control how often Anchorpoint clears the cache on push and pull.

"Always" - Anchorpoint will clean immediately
"Files older than one week" - Anchorpoint will clean files older than one week after each push and pull.
"Never" - You must clean these files manually

If you want to clean files manually, you can do this in the Project Settings / Git and run the Clear Cache command.

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Git features
   :glob:

   features/*
:::