# GitLab
### I cannot choose the group I want to use with Anchorpoint when connecting to Gitlab

Anchorpoint filters available groups for an access level of at least "maintainer" to allow inviting additional members to the group. So please check if you have the necessary access level to the group in Gitlab.

### I cannot create a Gitlab project in a group when creating a project in Anchorpoint

You need at least "maintainer" access level in the group to create new projects. Please check if you have the necessary access level in the group.

### User is not removed from Gitlab group when removing from Anchorpoint workspace

Anchorpoint will search for the user with their email in Gitlab. If the user has no public email or their username does not relate to the part of the email (e.g. username: johnhunt and email: johnhunt@anchorpoint.app), Anchorpoint will not find the group member and the deletion will fail. You can go to https://gitlab.com/groups/GROUP-NAME/-/group_members and delete the user directly from "Members" or "Invitations".

### User could not be removed from Gitlab project, but was removed from Anchorpoint project

Anchorpoint will search for members based on the Anchorpoint account email address of the user. If the user has no public email in Gitlab or their username does not relate to the part of the email (e.g. username: johnhunt and email: johnhunt@anchorpoint.app), Anchorpoint will not find the organization member and the removal will fail. You can manually remove a member from a project under https://gitlab.com/GROUP_NAME_OR_USERNAME/PROJECT_NAME/-/project_members.

## Git LFS: Access forbidden error

This is a bug on a GitLab server which has [been fixed in 2023](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/105130). If you are running on an older GitLab version do the following:

1. Undo the commit by clicking on the commit and then click on "Undo Commit"    
2. Make some slight modification to your files. E.g. on a PSD file, just rename a layer and save it.   
3. Sync your files again with a new message