# Connect without using the integration

Sometimes it is not possible using the Azure DevOps integration due to various reasons. There is also the possibility to connect a repository, that you will create on the Azure DevOps website and connect it via https to Anchorpoint.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/e547810bcac94a0f8598bf3e3a8d4921?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>