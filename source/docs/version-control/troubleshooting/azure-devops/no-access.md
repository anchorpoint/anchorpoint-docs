# No Access to Repository

This error can happen, because nobody added you to the repository on Azure DevOps or, because you don't have the proper access level in your Azure DevOps Organization. Do the following: 

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/6598f27478d94acbb166e0fcee86c8a4?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>
