# Using a custom credential set

If you are having trouble logging in with your username and password using the Git credential manager (the white popup dialog), you can try using custom credentials. Custom credentials use a PAT (Personal Access Token), which is an alternative to a username/password combination.

PATs have a limited lifetime. Make sure you set it long enough. When a PAT expires, Anchorpoint will prompt you to re-enter your credentials. In this case, you will need to create a new PAT in your account settings.


<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/f0dccf22121c4b4db0703094dadb95d2?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


