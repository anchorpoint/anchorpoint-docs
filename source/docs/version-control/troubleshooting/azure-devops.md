# Azure DevOps

Setting up a repository on Azure DevOps is not always smooth sailing. There are many issues that can occur due to the Microsoft accounts associated with it. We strongly recommend NOT using Microsoft accounts from other services (e.g. GitHub) when using Azure DevOps. In our experience, this has led to a lot of authentication errors when creating or joining a Git repository.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/2608040a3e31494199afc229dd47da6e?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## I don't have access to the Azure DevOps repository
First, check that you can access the repository (with your user account) on the Azure DevOps website. If that's not possible, try [this solution](https://developercommunity.visualstudio.com/t/admin-user-cant-login-to-azure-devops-401-uh-oh-yo/743785). Sometimes there are problems with multiple Microsoft accounts.

Another issue could be permissions. Go to "Organization Settings" / "Users" and change your team member from "Basic" to "Stakeholder" so that they can read and edit data in the repository.

## I am getting an access denied error
Make sure that you have enabled "Third-party application access via OAuth". To do that go to your [Organization](dev.azure.com/), click on "Organization Settings" / "Policies" and enable "Third-party application access via OAuth".

## Member cannot create Azure DevOps projects from Anchorpoint

If a member is unable to create Azure DevOps projects through Anchorpoint, you'll need to check their group permissions. Navigate to https://dev.azure.com/YOUR-ORGANIZATION/_settings/groups and click on "Project Collection Administrators," followed by the "Members" tab. If the member is not listed here, they won't have the necessary permissions to create DevOps projects either in Anchorpoint or directly in Azure DevOps. To resolve this, add the member to the "Project Collection Administrators" group.

## Free member limit reached

When attempting to invite a new member to your Anchorpoint workspace or a specific project, you may encounter an error message stating, "Cannot add member to Azure DevOps. You need to set up billing to invite more members." This indicates that you've reached the free member limit for your Azure DevOps organization. To resolve this, navigate to https://dev.azure.com/YOUR-ORGANIZATION/_settings/billing and click on "Set up billing" to extend your member limit.

By following these troubleshooting steps, you can resolve common issues related to Azure DevOps integration and ensure a smoother collaboration experience within your Anchorpoint workspace.

## Network configuration

At times, Azure DevOps may experience malfunctions when accessed through the IPv6 network protocol. To address this issue, Anchorpoint offers a solution that involves modifying your network configuration to ensure that only Azure DevOps utilizes the IPv4 protocol. We do this by making adjustments to the [hosts configuration file](https://stackoverflow.com/a/68259166/13494233).

Alternatively, you have the option to completely disable IPv6 on your system. Follow the steps outlined in this [guide](https://support.nordvpn.com/Connectivity/Windows/1047410402/How-to-disable-IPv6-on-Windows.htm) to disable IPv6.

## Member does not receive an Azure DevOps invite

If a member reports that they haven't received an invitation to join your Azure DevOps workspace, the first step is to verify the email address you used for the invitation. Make sure it matches the one the member provided. Next, navigate to https://dev.azure.com/YOUR-ORGANIZATION/_settings/members to check if the member is listed under the "Basic" Access Level. If the member is not visible, you can invite them by clicking on "Add Members." Make sure to set their Access Level to "Basic" to ensure they have the appropriate permissions.

## I cannot logout from Azure DevOps to login with a new account

Due to Microsoft's account linking, it's sometimes hard to fully logout. Here is a [forum entry](https://support.microsoft.com/en-us/office/which-account-do-you-want-to-use-2b5bbd7a-7df6-4283-beff-8015e28eb7b9) that describes how to solve that issue.

## 401 - Uh oh, you do not have access

If this happens to you, even if you think you are logged in with the correct account take a look at this [forum entry](https://developercommunity.visualstudio.com/t/admin-user-cant-login-to-azure-devops-401-uh-oh-yo/743785) .

## No organization found

If you have a new account without an organization and you did not reach the organization creation process for some reason, you can create a new DevOps organization from the Azure portal.

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/XME79S1CNR8?si=1Z_VFImbHdG6cA7L" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
## Cannot join Git repository - clone from the terminal

If you still face issues on joining the Git repository, and you have access to Azure DevOps and Basic user rights, try to clone the Git repository from the terminal. You can find the URL on the web page of your repository.

1. Go to "Project Settings" / "Git" -> Open Console/ terminal
2. In the terminal, drag the folder where you would like to download the Git repository and press enter
3. Then in the command line type in `git clone YOURAZUREDEVOPSREPOSITORYURL`
4. press enter


:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Alternative login solutions
   :glob:

   azure-devops/*
:::