# GitHub
### Anchorpoint cannot create a GitHub repository in an organization

You need to allow access for the Anchorpoint OAuth app in your organization under https://github.com/organizations/YOUR-ORGANIZATION/settings/oauth_application_policy so that the OAuth app can act on behalf of the user inside your organization. After the first attempt to make changes in the organization via Anchorpoint, an entry will pop up on the given URL that someone with admin rights in the organization needs to accept.

### User is not invited to GitHub organization

1. Check if the organization is selected in integration settings. We cannot invite users to your personal GitHub because there is no concept of members there.
2. Check if a user invite is already created for the invited user under https://github.com/orgs/YOUR-ORGANIZATION/people/pending_invitations. If there is already a pending invite, maybe delete it and invite the user again from this page.

### User is not removed from GitHub organization

Anchorpoint will search for the user with their email in a GitHub organization. If the user has no public email or their username does not relate to the part of the email (e.g. username: johnhunt and email johnhunt@anchorpoint.app), Anchorpoint will not find the organization member and the delete will fail. You can go to https://github.com/orgs/YOUR-ORGANIZATION/people and delete the user directly from Members or from Invitations.

### User has no access to GitHub repository but was invited into the Anchorpoint project

1. Anchorpoint cannot invite people into personal repositories, only into organization repositories. So check your integration settings if you have selected your organization.
2. Anchorpoint will search for organization members based on the Anchorpoint account email address of the user. If the user has no public email in GitHub or their username does not relate to the part of the email (e.g. username: johnhunt and email johnhunt@anchorpoint.app), Anchorpoint will not find the organization member and the invite will fail. You can manually invite an organization member into a repo under https://github.com/YOUR-ORGANIZATION/PROJECT-NAME/settings/access.

### User could not be removed from GitHub repository but was removed from Anchorpoint project

Anchorpoint will search for organization members based on the Anchorpoint account email address of the user. If the user has no public email in GitHub or their username does not relate to the part of the email (e.g. username: johnhunt and email johnhunt@anchorpoint.app), Anchorpoint will not find the organization member and the removal will fail. You can manually remove an organization member from a repo under https://github.com/YOUR-ORGANIZATION/PROJECT-NAME/settings/access.