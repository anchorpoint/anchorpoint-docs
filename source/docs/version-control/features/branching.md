# Git branching

When a project becomes more complex or the team gets bigger, friction arises. Work in progress commits from e.g. developers can disturb the work of the artist. To prevent this, you can split the project into certain zones. Each zone can then be used by a developer or artist without getting in each other's way. When the work is finished, these zones are dissolved and merged into the project.
Version control systems like Git call such a thing Branches.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/47a81a52de0a407ea59bd4886602cd9a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Options

<img class="content-image-middle" src="../../images/branches.webp">

An overview of all existing branches in your Git repository, sorted by modification date

### Change branches
Simply select the branch you want to work on from the dropdown in the Timeline. Anchorpoint will then switch to that branch and you will only see the commits from that branch.

### Create branches
Click on the "Create new Branch" option in the Branches dropdown and give it a name. Git allows you to create an infinite number of branches, but it is advisable to keep the number of branches low to avoid making the project unnecessarily complex.

### Merge into current branch
You can merge any branch into your current branch. Hover over the branch you want to get the changes from and click "Merge into Current Branch".