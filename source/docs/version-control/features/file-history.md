# File history
You can access the history of an individual file by browsing to it in the File Browser and then opening the Versions & Reviews page. On the right, under the "Versions" section, you will see the current status of the file and a drop-down list showing all previous versions. You can review these versions by clicking on the file. 

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/cfa5717d615d460a9abd056cb660185c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>
## Commands
### Revert
Just like reverting files in the timeline, "Revert" will revert your file to the same state as it was in the remote repository. It will basically delete your changes that you have not yet committed.

### Restore
This will overwrite the file with a previous version from the history. If you have modified the file, keep in mind that your changes (which you have not yet committed) will be lost. If you don't want this, you can restore the file as a copy.

### Restore as Copy
This will create a new file next to your current file that is exactly the same as the history entry. It's basically extracting a file from the history. With this command, you can place both files side-by-side and open them in your editing application to merge elements of those files.