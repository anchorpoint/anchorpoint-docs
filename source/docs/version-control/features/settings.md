# Git settings
You can customize how Git behaves in Anchorpoint. There are global and project based settings which you can change.

## Workspace Settings

These settings are available under "Workspace Settings" / "Git" and are applied for all projects. They are not shared between team members.

### Automatically lock Changed Files

Anchorpoint will lock files for other members once you have edited them. If you want to lock all files manually, uncheck this option.
### Show Notifications for new Commits

As soon as Anchorpoint detects new commits on your Git server, you will receive a push notification.

### Open Git Console/ Terminal

Allows you to open the Git command line. This version of Git is also set up correctly, uses Anchorpoint's version of Git LFS and also uses the shared Git configuration parameters (see below).

### Edit Git config

The Git config allows you to adjust the parameters of how Git behaves by default. You often tweak it when something is not working the way you want it to. But when you have a team of 50 people, tweaking everyone's Git config can be daunting. That's why Anchorpoint allows you do add shared Git config parameters that are automatically applied to every Anchorpoint user in your workspace.

Say you're using a self-hosted Git server, and you need to adjust the postbuffer because large pushes don't work out of the box. If you add this parameter to Anchropoint's shared Git config, it will automatically be applied to everyone else.

In general it's recommended to test parameters locally before you add them to the shared Anchorpoint Git config. Parameter that are setup in the Anchorpoint Git config, will override the local Git config file.

### Clear Git File Cache automatically

The Git LFS stores committed files in a cache under the .git folder. This cache grows as your commit history grows. Anchorpoint can automatically clean it up, saving you disk space.



## Project Settings

These settings are available under "Project Settings/ Git"

### Repository URL

Shows the remote URL. Normally you don't need to change that.

### Create .gitkeep files in new folders

By default, Git does not understand empty folders. If you e.g. want to create a folder structure with empty folders in a Git repository, Anchorpoint will add a hidden .gitkeep file so they can be uploaded to Git.

### Automatically track all binary files as LFS

Anchorpoint creates the .gitattributes file automatically by adding all binary files which it detects in a commit. If you want to modify this file manually, disable the option.

### Auto Lock Text Files

Anchorpoint locks binary files by default because they cannot be merged. Text files can be merged, so locking is generally not necessary. However, there are some exceptions when it comes to complex document files, such as .unity or .ma (Maya ASCI) files that were edited by the application rather than a text editor. It often makes sense to lock them, because merging them would be extremely difficult in case of conflicts.

### Open Git Console/ Terminal

Opens a command line application, so you can use any Git commands. This is more suited for programmers or debugging purposes.

### Clear Cache

This will delete old versions from your hard drive, but not from the server, to free up space on your computer. It will only remove cached files that have already been pushed to your Git server. It will never remove local commits that have not yet been pushed.

This command is called "LFS prune" in Git parlance, and cleans up the entire LFS cache.
Anchorpoint also does this automatically based on an interval set in "Workspace Settings" / "Git". Per default it's removing cache files older than one week.

### Update Credentials

Opens the login interface where you authorize Anchorpoint with the Git hosting provider (GitHub, GitLab, Azure DevOps etc.). This is useful when you e.g. changed your account.

### Refresh Sparse Checkout

If you see incorrect states in the tree view in the right pane of the Explorer (e.g., wrong cloud icon), you can update the selective checkout index so that Anchorpoint scans all files that have been checked out to show the correct state.


