# Git resolving conflicts

Conflicts arise when several members work on the same files. They always become visible after a pull or after a merge of a branch. The person who is currently pulling must then decide which change is saved as the last version. 

In Anchorpoint you are mostly committing your files before resolving a conflict. This means, that files will never get lost. Resolving a conflict is simply deciding, what will be the latest version.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/49e81f67d2ab4e299079df3cb141f706?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

