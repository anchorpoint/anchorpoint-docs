# Git commands

A Git workflow basically has commands that are executed continuously and commands that are needed from time to time.

## Timeline commands

These commands are available in the Timeline.

### Push

The push uploads the commits (i.e. the project checkpoints) to the server. Once they are uploaded, the team can download them via a pull.

### Pull

The pull downloads commits from the server. After the pull, you are up to date. The pull also happens explicitly, so you can't break anything while working. If a commit has caused problems (e.g. the level in the game engine no longer loads), it can be undone via Undo Commit.

## Submitting new changes

These commands are available when clicking on "Changed Files" in the Timeline.

### Commit

This will "freeze" the file versions. A commit is so to speak a version of the project, so a kind of checkpoint. When committing, Anchorpoint duplicates the files in the changed files and puts them in a cache folder. So you can continue to work on the file, for example, overwrite it, because the committed version is practically secured.

### Sync

Sync combines a Commit+Fetch+Pull+Push in one command. 

## Restoring previous states

In Git, there are multiple ways to restore previous states of files.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/d83b8a6a6cc34e388f973b351a6b96d4?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Revert

A revert resets your files to the last commit. This means that all changed files are removed. This is useful if you have made accidental changes to a project and want to jump back. Be careful with this operation because it cannot be undone.

Revert is often used when you are experimenting on a scene, for example. You download many assets to try out. When committing, you only want to commit the assets that are in the level. You can revert all unused files with this.

### Undo Commit

Undo commit reverts all file changes in the selected commit. If your commit has not yet been pushed, Undo commit simply reverts that commit. If your commit has already been pushed to the server, Undo commit will create reverted files.

If new files were added in the commit, Undo Commit removes them. They then show up as removed files in the changed files. If a file has been modified, Undo Commit restores the file to its previous state, creating a changed file.
If the changed files are then committed, the old commit has actually been undone.
For commits that are not yet pushed to the Git server, Undo commit basically removes the commit so that you are in a state before you've entered the commit message.

### Restore

You can restore file versions from any commits. To do this, select a commit in your project. Select the files and choose "Restore" from the context menu. You have two options

**Overwrite**: Overwrites the current file version with the version from the commit you selected.

**Keep Original**: Don't touch the current file version and create a new file with the appendix "\_restored".

### Reset Project

You can reset the entire project to a previous state. Click on a commit and select "Reset Project". This command works only on repositories with access to remote. You must also have committed all changed files beforehand.

## Other commands

### Copy ID

This command is available when you click on a commit, and copies the commit ID to the clipboard. This is useful if you want to use the Git command line.