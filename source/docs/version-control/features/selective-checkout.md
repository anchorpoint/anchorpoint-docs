# Selective checkout

To save storage space, you can select which folders of a Git repository should be synchronized and which should not. This is useful if, for example, you store your kind assets in the Git repository and the programmer in your team does not want to download the kind assets.

Another benefit is that selective checkout improves your file tracking performance, especially if you have a repository with millions of files, but you only need to work on a subset of them.

Selective checkout (also called Git sparse checkout) does not work with other Git clients like GitHub Desktop. If you want to work with them in parallel, you will need to download the entire project.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/6063ba76020747b59f437f751dd16e65?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Options

### Checkout Folder/Remove Folder
This command is available in the context menu of the tree view on the left. Downloads the contents of the Git repository or frees up space.

### Load Thumbnail
If you're browsing an online-only folder (not checked out), you won't be able to see thumbnails immediately. To view them, you need to select the files and click the 'Load Thumbnail' button in the top right-hand corner. This will download the file to a cache location and generate a temporary thumbnail. This only works for binary files, not text-based files.

### Create Empty Folder
For example, if you want to create an asset in an "assets" folder (which is your database), but don't want to download all the content in it, you can simply create the empty folder (in this case "assets") as an empty folder, so you can create subfolders or add files to it.