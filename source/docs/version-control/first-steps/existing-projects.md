# Other Git clients

If you already use Git with SourceTree, GitHub Desktop, or similar, you can use the same Git project with Anchorpoint. Both applications (even the command line) can work in parallel. This video shows you how to import an existing Git repository into Anchorpoint.


<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/f653fc36a2664eca8a0ae343afb6b83b?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Connecting Anchorpoint with your Git hosting provider (e.g. GitHub)
The first time you push or pull with Anchorpoint, you may see a popup asking for a username and password. These are the credentials used by your Git hosting provider (e.g. GitHub) to allow Anchorpoint to upload and download files.