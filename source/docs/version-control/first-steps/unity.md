# Git with Unity

Git works very well with Unity because it's text-based files, which are easy to merge and easy to commit. Anchorpoint also supports file locking capabilities for files like .unity and .prefab, as well as binary files that cannot be merged and should always be locked. It adds an additional metadata system to Git's versioning functionality that is needed for file locking, art asset management, and having a centralized way to manage your project.

Anchorpoint automatically configures Git LFS if needed. This is important for binary files, so you won't hit repository limits soon. It also comes with a default .gitignore template for Unity. You can connect Anchorpoint to any Git hosting provider such as GitHub, GitLab, Gitea or Azure DevOps.

Next to your Unity project, you can also use Anchorpoint for versioning and managing your art assets.


<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/f82fdf735d8345d091a493fb5a3c16f9?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


## Using the Unity plugin
Anchorpoint also provides a Unity plugin which allows you to commit directly from the Unity editor and see which files are locked. Although you can use Anchorpoint without the plugin, we strongly recommend that you use it, as it is the only way for file locking to work properly. You can learn more about the plugin in the [integrations](https://docs.anchorpoint.app/docs/general/integrations/unity/) section.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/b73100c4a8354e6396ab03e0815d5a4a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Beta status of the plugin
As of today, the plugin is in beta and must be manually downloaded and installed as a Unity package. We are aiming for a release version to be distributed via the Unity Asset Store in April 2025.

### Installing the plugin
[Download](https://www.dropbox.com/s/k25xktbze5v8wiq/ap-unity.zip?e=1&dl=1) the Unity package, unzip it and import it via the package manager.

Open the Unity Package Manager via Windows/Package Manager. Click on the plus sign in the top left corner and select "Install package from disc...".

After that, you can click on Window/ Anchorpoint to open up the Anchorpoint plugin UI in Unity.

## Workflow recommendations

### Use prefabs
When working in a team, avoid putting to much content in the .unity scene. Split your project into a logical prefab structure. Then, work on a prefab that can be locked, so you will avoid conflicts and you also do not block other team members from working on your project.

### Branching
Keep the amount of branches to a minimum. You can learn more about the term "trunk based development", where you only branch out from the main branch and merge back to main. You never branch out from a subbranch of main. For releases, have a release branch where you just merge your changes from main, once you have a release ready. 

### A proper .gitignore file
A .gitignore is a configuration that filters out files that should be pushed to your central repository, such as the Library folder, which mostly contains cached files that can be regenerated at any time. Anchorpoint comes with a default .gitignore file. If you want to configure it yourself, perhaps because you have a unique setup, take a look at our blog article on [how to configure a .gitignore file for Unity](https://www.anchorpoint.app/blog/how-to-set-up-a-gitignore-file-for-unity).

### Your Git hosting provider
Make sure your Git hosting provider provides enough LFS space for your projects. Providers like Azure DevOps do not charge for LFS space, but GitHub does. Just make sure you have it set up correctly. There is no need to configure anything else, just make sure that you have purchased enough storage packages.

### Art assets
It's highly recommended that you keep your art assets (Blender, Photoshop, etc.) under version control. Use a separate repository for this to keep things tidy. Only put export formats (e.g. FBX files) into your Unity project.

## Unity’s Smart Merge Tool
When you both work on the same scene but on different objects, your changes can be merged. Git can merge text files, but it is not intelligent enough to merge Unity scenes. For that, you have to use their UnityYAMLMerge tool. To use it, you have to modify the .gitconfig file.
1. Open your Unity project in Windows Explorer.
2. Make hidden files visible.
3. Open a .git folder and open the **config file** in a text editor.
4. Add the following code:

```yaml
[merge]
tool = unityyamlmerge

[mergetool "unityyamlmerge"]
trustExitCode = false
cmd = "'path to UnityYAMLMerge'" merge -p "$BASE" "$REMOTE" "$LOCAL" "$MERGED"
```

"path to UnityYAMLMerge" should be replaced by the real path to the UnityYAMLMerge.exe on your computer. Usually, you can find it under: “C:/Program Files/Unity/Hub/Editor/2021.3.21f1/Editor/Data/Tools/UnityYAMLMerge.exe”. Make sure that you use forward slashes (/) and not backslashes (\\) like Windows is doing.

## In case of issues
Feel free to join our [Discord](https://discord.com/invite/ZPyPzvx) and ask for help. If you are a customer, please use our support@anchorpoint.app email for faster response times.