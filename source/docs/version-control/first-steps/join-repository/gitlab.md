# Join a project on GitLab

If you are a team member working on a project that uses GitLab as a server, you will need to connect to it. Typically, you only need to connect once and it will be used for future projects.

## Step by step

1. Accept the email invitation from GitLab
2. Create an account (if needed)
3. Make sure you can see the files on GitLab online
4. Open Anchorpoint, click the project, and follow the instructions