# Join a project on GitHub

If you are a team member working on a project that uses GitHub as a server, you will need to connect to it. Typically, you only need to connect once and it will be used for future projects.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/0b31823736c442a8a74e6a2d7111de12?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Step by step

1. Accept the email invitation from GitHub
2. Create an account (if needed)
3. Make sure you can see the files on GitHub online
4. Open Anchorpoint, click the project, and follow the instructions