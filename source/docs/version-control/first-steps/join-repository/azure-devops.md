# Join a project on Azure DevOps

If you are a team member working on a project that uses Azure DevOps (an alternative to GitHub) as a server, you will need to connect to it. Typically, you only need to connect once and it will be used for future projects.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/121ccc9eb5bb4fcca02258e1edfee1cd?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


## Step by step
1. Accept the email invitation from Azure DevOps
2. Create an account if needed. Don't sign in with GitHub, Skype, or any other associated account. It will cause problems later.
3. Make sure you can see the files on Azure DevOps online
4. Open Anchorpoint, click the project, and follow the instructions

If you see that Azure DevOps or Anchorpoint is sometimes not responding, simply refresh the page in your web browser.

## How to solve login issues

### 1. Check if you have access

<img class="content-image-large" src="../../../images/devops_account_settings.webp">
Some issues can also happen when you don't have the proper user rights in Azure DevOps. Make sure that user rights are set to "Basic" in your organization settings.

### 2. Try an alternative login method

Another issue can happen due to linked accounts on Azure DevOps. If you cannot login with your account, you can also use a [custom credential set](https://docs.anchorpoint.app/docs/version-control/troubleshooting/azure-devops/custom-credentials/).