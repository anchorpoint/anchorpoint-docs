# Join a project on Bitbucket

If you are a team member working on a project that uses Bitbucket as a server, you will need to connect to it. Typically, you only need to connect once and it will be used for future projects.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/7b76b3f07c7b4e4abe5142dac8a2e3e8?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

## Step by step

1. Accept the email invitation from Bitbucket
2. Create an account (if needed)
3. Make sure you can see the files on Bitbucket online
4. Open Anchorpoint, click the project, and follow the instructions

## How to solve login issues

### 1. Check your repository URL in the Project Settings

By default, BitBucket creates a personalized Git URL, that includes the username@bitbucket. In this case only the person that created the Anchorpoint project will be able to clone the repository. To fix that, go to your Anchorpoint project, click on "Project Settings" / "Git" and fix the URL from
```
https://username1@bitbucket.org/myawesomeworkspace/myawesomerepository.git
```
to
```
https://bitbucket.org/myawesomeworkspace/myawesomerepository.git
```
