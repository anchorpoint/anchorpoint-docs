# Join a Git repository

When you are invited to work on a project at Anchorpoint, the project files are stored in a "Git repository," which is similar to a hard drive in the cloud. Have a look at the following cloud providers:

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Cloud providers
   :glob:

   join-repository/*
:::