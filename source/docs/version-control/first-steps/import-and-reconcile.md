# Import existing files

If you want to commit to a Git repository and you already have the project files on your computer, e.g. through another version control solution such as Perforce or SVN, or you've been using something like Google Drive, Anchorpoint can import those files so you don't have to download them again.

Anchorpoint will then check all the files against the files in the Git repository and only download any missing files that were not imported. Note that you will end up with the complete project on your computer. This process does not support selective checkout. You'll need to manually unload any folders you don't want once the import process is complete.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/1be13d569e134193b4c82ff27a94838a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>