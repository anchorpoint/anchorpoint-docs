# Git with Unreal Engine

Git is a widely-used version control system that enables efficient collaboration on Unreal Engine projects. This guide will walk you through setting up Git with Unreal Engine using Anchorpoint.

Anchorpoint automatically configures Git LFS if needed. This is important for binary files, so you won't hit repository limits soon. It also comes with a default .gitignore template for Unreal. You can connect Anchorpoint to any Git hosting provider such as GitHub, GitLab, Gitea or Azure DevOps.

Next to your Unreal project, you can also use Anchorpoint for versioning and managing your art assets.

## Setting up Azure DevOps as your central server
To work in a team, you need a central Git server. In most cases, this will be a server in a cloud, such as GitHub, Azure DevOps or Gitea Cloud, because it's easy to set up. You can also use a self-hosted Git server if you want more control over your data.

In this example, we'll set up a server using Microsoft Azure DevOps because it has a generous storage policy, which is useful for large Unreal Engine projects.


<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/a401f7eef11c47b683ce0bfaa97ba26c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Using the Anchorpoint Unreal plugin
Anchorpoint provides a Revision Control plugin for Unreal Engine. With the plugin you can:

- View locked, modified and obsolete files and actors in the Unreal Editor
- Commit changes from the Unreal Editor
- View blueprint diffs, a single file history, and resolve merge conflicts
- Pull new changes from Anchorpoint without closing Unreal Engine
- Open the Anchorpoint browser from anywhere in the Content Drawer

Although you can use Anchorpoint without the plugin, we strongly recommend that you use it, as you will have more control of which files you submit due to the checkout workflow. You can learn more about the plugin in the [integrations](https://docs.anchorpoint.app/docs/general/integrations/unreal/) section.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/668dc0e1ae464b18962c29ea106dbbbd?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Installing the plugin
Download the plugin and place it to your project/plugins directory. 

Download Links v0.28
- [Windows](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/internal/unreal/AP_Unreal_plugin_v0_28_Windows.zip)
- [macOS](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/internal/unreal/AP_Unreal_plugin_v0_28_Mac.zip)

Make sure that the plugin is not part of the .gitignore, so that it is submitted to the main repository. Make also sure, to whitelist the the Anchorpoint Unreal plugin, such as:
`!**/Plugins/ap-unreal/**
If you see warnings, that your Unreal project requires the Anchorpoint plugin, that usually means that it's not in your Plugins directory of your project. Your team members can then download and install the plugin manually.

### Don't use the inbuild Git plugin
Although Unreal Engine has its own Git integration, we recommend using an external Git client. The Git integration of Unreal Engine does not support LFS and file locking and has never left beta. Just don't use it.

## Workflow recommendations
### Use the Anchorpoint plugin in Unreal Engine
Using the Anchorpoint plugin gives you more control over which files to commit. Without the plugin, inexperienced users might commit too many irrelevant files to the Git repository, unnecessarily bloating it. 

### Use One File Per Actor
With Unreal Engine 5, one file per actor was introduced to make it easier to work with large open world maps. It's also enabled by default when using World Partition. Even without World Partition, it makes sense to enable it in "World Settings" -> Use External Actors.
Using external actors will make your commits much lighter and won't lock up the whole level you're working on. Unreal splits changes into small actor files instead of keeping them in the level file.
Anchorpoint can display the actor names instead of the hashed file name, so you see what you commit.

### Have a proper folder structure and a naming convention
Unreal Engine does not make it easy to rename and move files because it stores identifiers via a file path. Unity, for example, uses unique IDs that make it easier to reorganize the project. In Unreal you have to deal with resolving redirectors. 
Here is a quick guide [how to setup a folder structure and naming conventions](https://www.anchorpoint.app/blog/how-to-properly-reorganize-your-unreal-engine-project-structure) for an Unreal Engine project.

### Branching
Keep the amount of branches to a minimum. You can learn more about the term "trunk based development", where you only branch out from the main branch and merge back to main. You never branch out from a subbranch of main. For releases, have a release branch where you just merge your changes from main, once you have a release ready. 

### A proper .gitignore file
A .gitignore is a configuration that filters out files that should be pushed to your central repository, such as the DerivedDataCache folder, which mostly contains cached files that can be regenerated at any time. Anchorpoint comes with a default .gitignore file. If you want to configure it yourself, perhaps because you have a unique setup, take a look at our blog article on [how to configure a .gitignore file for Unreal Engine](https://www.anchorpoint.app/blog/how-to-set-up-a-gitignore-file-for-unreal-engine).


## In case of issues
Feel free to join our [Discord](https://discord.com/invite/ZPyPzvx) and ask for help. If you are a customer, please use our support@anchorpoint.app email for faster response times.