# Git troubleshooting

Git is in itself a very complex and powerful system that sometimes causes difficulties. In combination with external Git servers it can sometimes come to some mistakes, which are listed here.

## General recommendations

- Keep your Git repository on an internal drive. Avoid mapped/ network drives or RAID management software that creates custom volumes.
- Don't put your Git repository in a Dropbox, Google Drive, Cloud Drive, NAS or similar
- If you run out of storage, clear the Git cache by going to "Project Settings" / "Git" and clicking on "Clear Cache".
- Make sure that you have enough Git LFS storage on your Git hosting provider (GitHub, Azure, GitLab, etc).

## Errors on Commit/ Staging

These are problems that happen right after you press the "Commit" or "Push" button. During that process you see a huge progress indicator overlaying the application.

This problem often occurs if you are using a mapped/network or custom mounted drive with RAID management software. Git will not be stable here. It's recommended that you work on an internal hard drive. If you have created your project on a mapped drive, simply cut and paste the project folder (including the _.approj file) to your internal drive. Then open Anchorpoint by double clicking on the _.approj file.

## Errors on Push

These are problems that happen after the commit, so when Anchorpoint is uploading the files in the background.

### Cannot push to Azure or GitHub due to wrong username/ password

Normally the credentials manager (a white popup dialog) should open. If it does not, go to "Project Settings" / "Git" and select "Update Credentials".

If you have logged into Azure with a different account, logged into any Microsoft product (e.g. Office) with a different account, or have a Microsoft account with a different email, Anchorpoint may be using the wrong credentials. You can change this in the [Windows Credential Manager.](https://support.microsoft.com/en-us/windows/accessing-credential-manager-1b5c916a-6a16-889f-8581-fc16e8165ac0) You can access it by pressing the WIN key and typing "Credential Manager".

<img class="content-image-middle" src="../images/credentialManager.webp">

Entries like this confuse Anchorpoint. You may have other Microsoft related entries. Removing them does not cause harm. It's just removing credentials from a cache. If an application needs them, it will ask you to enter them again.

### Pushing on protected branches

This can happen if you are pushing to the main branch, which may be protected by your git server admin. You can fix this by either asking the git server admin to give you more rights, or by pushing to a different branch.

## A team member cannot join the Git repository

This may be because the team member was added on the Git server. Besides Anchorpoint, you also need to invite him to your repository on GitHub, Azure DevOps, Gitea, etc. so that he has access to the data. If a member does not have access, Anchorpoint will display the name of the Git server where you need to add your team member in a message.

## How to fix a broken Git repository

If your timeline is empty, or you see certain error messages, that don't make sense for you then your Git repository might be broken. This can happen due to various reasons:

- You re-formatted your computer
- Your computer crashed during a commit process
- You deleted something in the .git folder
- You were running out of disc space
- You played around with some Git commands that you did not understand

The simplest way to fix that is to re-initialize the Git repository. Anchorpoint will create a backup of your project and then re-download all the files from the remote server if needed. Go to "Project Settings" / "Git" and pick **Re-Initialize Project**.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/09ddd68c83fa462c9ae4ae0629a9e2d1?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

If this command did not solve the problem, you can also try this workaround:

1. Close Anchorpoint and close Unity/ Unreal and any other tool that you use right now
2. Rename your project folder to something like "myProjectFolder_backup"
3. Open Anchorpoint. It will ask you to re-join the Git repository. Pick a new empty folder and download your project again.
4. Then, from your backup (the renamed project folder) pick the files that you have changed and paste them into your newly downloaded project

## Cannot store credentials

This issue shows up when the Git is not able to store your credentials in the Windows Credentials Manager. Here is a [discussion on GitHub](https://github.com/desktop/desktop/issues/15217) if you want to know more. One of the reasons might be that the Credential Manager has too many credentials and you have to remove some old unused ones. You can access it by pressing the WIN key and typing “Credential Manager”, then click on "Windows Credentials" and remove some older entries.

<img class="content-image-middle" src="../images/credentials.webp">

## Issues with file/folder paths

Git does not allow certain characters in file/folder names such as `* : ? " < > |`. Another thing can be that you have used one of these folder names, which are technically not allowed on Windows such as: `'CON', 'PRN', 'AUX', 'NUL', 'COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6', 'COM7', 'COM8', 'COM9','LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5', 'LPT6', 'LPT7', 'LPT8', 'LPT9'`

You will need to locate the folder or file and fix the issue. 

- If the issue occurs when you want to commit a file, just rename it in Windows Explorer or macOS Finder. 
- If the problem occurs when pulling or selectively downloading folders from GitHub or Azure DevOps, for example, go to their website and find the file/folder there. Some hosting providers allow you to rename it and some don't. If you cannot rename a file/folder on your hosting provider, simply download the content (to have a backup copy) and then remove the file/folder. Don't forget to commit your change on their web portal. Then pull your change back into Anchorpoint, or rejoin your Git repository if you have not already done so.


## File not found

This can happen, when you use other Git clients (or a Git integration in your game engine), where Git LFS is not configured properly. This issue happens mostly on Pull. To fix this issue, do the following:

1. Download and install [Git for Windows](https://git-scm.com/downloads) on the computer of the user whose commit you cannot pull.
2. In your project open the command line. On Anchorpoint you can access it via "Project Settings" / "Git" -> Open Git Console/ Terminal
3. Type in `git lfs install`
4. Then, type in `git lfs push --all origin main`

After that, try to pull the changes again.


## Files being read only for no reason

If you see that files are set to read-only on your local system and they are not locked in Anchorpoint by another team member, this may be related to Git LFS file locking. In Anchorpoint, click on the Locked Files page in the left sidebar to see if any files are locked.

To check if files are locked by Git LFS, use this command in the terminal: 
`git lfs locks
Also check your .gitattributes file and remove any logic for applying file locks.

We advise you to not use Git LFS file locking, when working with Anchorpoint, because Anchorpoint uses it's own system for locking files. 

## Other issues

We are constantly collecting issues to either improve Anchorpoint or to provide help. To make our life easier report us your issue by doing the following:

- Close Anchorpoint
- Go to C:/Users/USERNAME/AppData/Local/Anchorpoint/app-1.XX.X/scripts/win and open **debug_mode.bat**
- If you are on macOS go to /Applications/Anchorpoint.app/Contents/MacOS/Anchorpoint and launch it with the "--debug" command at the end
- Try to do what went wrong (e.g. committing files, pulling, opening the timeline, reverting etc.)
- When the error shows up again send us a message by clicking on the "..." button in the top right corner

Describe your problem in the message. We will get a log file containing the error. This should give us all the information we need to fix it. Otherwise, you can also contact us at our [Discord server](https://discord.gg/ZPyPzvx)

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Troubleshooting Git servers
   :glob:

   troubleshooting/*
:::