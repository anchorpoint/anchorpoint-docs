# Unreal Engine integration beta phase

Thank you for participating in the beta testing phase of our Unreal Engine integration to Anchorpoint. The integration allows you to:

- See locked, changed and outdated files and actors in the Unreal Editor
- Submit changes from the Unreal Editor
- Connect with Anchorpoint’s desktop application, so that you can revert and pull changes from Anchorpoint without closing Unreal Engine
- Open the Anchorpoint browser from any place in Unreal Engine

👥 Feel free to share this page with your team!

## The beta phase

The best way to communicate with us is via Discord. We have a [closed Discord channel](https://discord.com/invite/ZPyPzvx) for our beta testers. If you are not part of it, please contact Matthäus (m.niedoba@anchorpoint.app) so he will add you to the channel.

We expect a beta phase of approx. 2 months. This will of course depend on the bugs you find and improvements that you request. After that, we will aim for a stable release. Keep in mind that this beta is rather focused on finding bugs and small improvements, rather than full new feature requests.

At the end of the beta phase we will publish a version in the FAB marketplace. If you think that this plugin improves your workflow, please leave us a rating. This will build trust towards new users and is one of the best way to market our company.

## Installation

You can use the plugin with your current Anchorpoint desktop application. Make sure that it’s updated to the latest state (v 1.24.0), which was shipped on 4th December. Then follow the steps below.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/c171a5cab4ba479eac5d4d97a56b2b23?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


1. Download the plugin for [Windows](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/internal/unreal/AP_Unreal_plugin_v0_27_Windows.zip) or [macOS](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/internal/unreal/AP_Unreal_plugin_v0_27_Mac.zip) 
2. Unzip it and copy the correct version of the plugin it to your “Plugins/ap-unreal” folder in your Unreal project.
3. Open your .gitignore and add for example `**/plugins/ap-unreal*` if you want to prevent that the plugin should be part of your project. Then every team member has to install the plugin on it’s own.


## Usage

Once the plugin is installed, you can use the Anchorpoint - Unreal Integration like any other Revision Control plugin. An additional benefit of using the plugin is that you can pull, reset the project and revert your project in Anchorpoint without closing the Unreal Editor.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/eb6ee0b3c13e4369a6a6e9f4c15b2ec0?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Bug reporting guideline

We try to keep it as simple as possible. Once you encountered an issue, do the following:

- Post it on the closed Discord channel
- Add a brief description with reproduction steps. For example:
    1. Opened a level in Unreal
    2. Modified a file
    3. Saved
    4. Clicked on Checkout File
    5. …
- Attach as many additional information as possible
    - Your Unreal Engine version
    - Screenshots
    - Log file. You can find your Unreal Log file under “YourUnrealProjectName/Saved/Logs”
- Please keep all the relevant conversation for your issue in a Discord thread. Don’t just reply to a message that is not in a thread.