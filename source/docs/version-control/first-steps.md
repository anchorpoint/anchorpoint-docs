# Git basics

For frictionless collaboration in a game engine, you need version control. A version control system replaces incremental saving and a file syncing service like Dropbox or Google Drive. Rather than synchronizing your files continuously, you upload and download a list of files manually.

Git is a version control (or source control) system that allows you to store snapshots of a file, without changing the filename. When you use Unity, Unreal or Godot or when you develop software, that's the way to do it. Game engines cannot handle increments like "_v001". They won't find the files anymore. A version control system allows you to store all older file versions on a central server as snapshots or checkpoints. This way, you can always roll back if something breaks.

Anchorpoint supports a subset of Git that is relevant for artists. This includes connecting to repositories, automatically configuring Git LFS, committing, pushing and pulling. Anchorpoint adds extra features such as file locking and understands file types such as .uasset, .umap, .fbx etx.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/d5bbbc3a69524b8db87abffdeb7fa078?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>


## What is Git
[Git](https://git-scm.com/) is a versioning system, which among other things is behind the well-known platform [GitHub](https://github.com/). The task of Git is to record all file changes within a folder (a repository) so that these can be saved (committed) as a version.  Git is the most popular versioning system in software development. It is open source and has lots of plugins and extensions. It is possible to host Git projects on numerous servers such as GitHub, GitLab, Bitbucket or Azure Devops. 


### Git servers
Next to [GitHub](../general/integrations/github.md), there are plenty of other solutions such as [Azure DevOps](../general/integrations/azure-devops.md), Bitbucket, [GitLab](../general/integrations/gitlab.md) or [Gitea](../general/integrations/gitea.md) if you want to self host a Git server. Anchorpoint can push and pull files to these servers and works like a normal Git client. 

You can use Anchorpoint's integrations to create new repositories on that severs. If you already have a repository on a Git server, use the option "[Connect existing repository via Https](troubleshooting/azure-devops/avoid-integration.md)".

## Using Git in Anchorpoint with other Git clients
You can use other Git clients with Anchorpoint such as SourceTree or GitHub desktop. Keep in mind that these tools cannot use [selective checkout](features/selective-checkout.md) or clean the Git cache. If you have previously cleared the git cache in Anchorpoint, you may find that when you pull into GitHub Desktop, it downloads the entire cache from the server. 

We recommend using it only for Git operations that are missing in Anchorpoint (e.g. cherry picking, rebasing, etc.), not for pulling and pushing.


:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Git first steps
   :glob:

   first-steps/*
:::