# Tasks

> ℹ️ Requires a Team plan

A task works like a Slack channel or Twitter feed. You can leave comments, mention people, and drop files. All actions on the files (added versions, reviews) are automatically displayed in the task. Use attributes to set a status or assign artists.

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe src="https://www.loom.com/embed/382829604f9542edbc51d79720b7bf99?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Moving, dragging, duplicating and other task commands
You can drag tasks within a task view or move them between views. This also works between multiple projects in the same workspace. Let's say you want to copy tasks from an older project to a new one to use as a template. Select them all, copy and paste them into the new project.

### Attributes
Attributes are also copied. You just need to make them visible in your new task list if you don't see them. Tags are merged. If you have multiple tags in the task you want to copy, Anchorpoint will take all of those tags and see if they are available in the location where you want to paste the tasks. It matches them by name, case sensitive.

### Paste tasks into specific views
If you have multiple views and want to paste a set of tasks into them, open the context menu of the task and choose **Paste**.

![Task menu](https://cdn.loom.com/images/originals/6e9c03b7b5a94f56ba6f87e927b65b19.jpg?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9jZG4ubG9vbS5jb20vaW1hZ2VzL29yaWdpbmFscy82ZTljMDNiN2I1YTk0ZjU2YmE2Zjg3ZTkyN2I2NWIxOS5qcGciLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2ODkwODM2MzF9fX1dfQ__&Key-Pair-Id=APKAJQIC5BGSW7XXK7FQ&Signature=ZbXrN11LF7O21HugYcRMzz7KEjlx5lGSc5WBT5UEbb-1tW7VmFx3gEe0PUbhnogr4Byul-z-yqTOTNDVTQ6wB378kDmjIzzhKEtcuUU5mSULLe1oltDfbXEvRWt6N5aVZsK5loYKCFD1ySjnev1JbYYmlbcu5dlk-lCgGf6bygb7UKO9S1ntQkLWhfOIaRZLzwUengGv4DaAwjTkxiNsabwy5H97PfJYUlZKsv1KPBDlIgu6Shbo%7ETUSu-sDOqpqJTAgrvRKnEuH5S1LkvBFI8L2fPDY4ir8luBFr6lLvfCKRLWKzLfoFA2AjepxJzN8dvtZLODZUfm9W8DW48LIEA__)