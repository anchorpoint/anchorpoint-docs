# Attribute settings

Attribute display settings can be customized under "Workspace Settings" / "Attributes". These settings are saved locally and individually for each member.

## Date Format
Changes the date display. In Europe, dates are displayed differently than in the US. You can see an example in the description when you change the date format.

## Location

The location Attribute shows up in the search and changed files and displays a folder path. Especially in the changed files, it makes sense to shorten the display of the location, which will lead to a nicer grouping of your files. You can type the folder name, that will be hidden or use a regular expression. 

![](attachments/location-settings.png)
You can hide folders from the location or truncate them from the end of the folder path.

### Examples

![](attachments/before-after-location-shortening.png)
One File Per Actor Folders at the end are hidden

![](attachments/Pasted%20image%2020240416114738.png)
The textures folder is shortened so that all files are shown under the asset name "Pottet_plant" and can be selected with one click

### Regular Expressions
In addition to typing the folder name, you can use [regular expressions](https://regexone.com/) to hide specific elements of a file path that match a pattern. The best way to use regular expressions is to ask Chat GPT to write them for you.

Here are some examples for regular expressions:
`\/[0-9a-zA-Z]{1,2}\/[0-9a-zA-Z]{1,2}` Hides subfolders for One File Per Actor files in Unreal Engine such as: 6/MV or 23/MI.

`\/tex(?:tures)?` Hides names such as "tex" and "textures"

`^_\w+` Hides all folders that start with an underscore e.g. "old" or "_archive"

