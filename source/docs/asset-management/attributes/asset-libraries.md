# Asset libraries

> ℹ️ Requires a Team plan

You can also use Anchorpoint as an asset library for your studio, tagging files and folders and adding descriptions. Anchorpoint's search function gives your team quick access to the right files.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/3b1eb671d0054cfcaa552fdf9a309c71?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Attributes
Attributes allow you to add metadata to files and folders. This metadata can be tags, checkboxes or description fields. Read more about [Attributes](../attributes.md).

You can also use the filter system to filter by file names, file extensions and attributes in your folders. You can share this filter view with your team. This way you have a clean and tidy asset library for your team.