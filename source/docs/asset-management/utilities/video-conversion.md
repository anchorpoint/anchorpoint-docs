# Video conversion

> ℹ️ Requires a Team plan

Anchorpoint has a set of build in video utilities, that allow you to convert image sequences to video, convert large video files into smaller mp4 files or replace an audiotrack in a videofile without re-encoding. Video utilities are basically [Actions](../../actions/create-actions.md) in Anchorpoint, which are based on the [Python API](../../actions/python.md). This means that you can modify the code to your own needs. All video utilities are based on [FFmpeg](https://ffmpeg.org/) and require installation, which happens automatically when you use them for the first time.

## Convert an EXR or PNG image sequence to MP4
Whether for daily reviews, as an email attachment or for a quick preview. Mp4 videos can be played by any player and are indispensable in everyday production. This is a useful Anchorpoint feature that allows you to convert PNG or EXR sequences to MP4 video. It can also convert raw video like MOV or AVI.

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/jaK3Ru86CcA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## Change the audio in a video file

Replacing the audio of a video usually means opening the video in an editing program, changing the audio track, and re-exporting. If you are unlucky, you don't even have the source files. Here is how to change the audio without having to re-export your video.

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9K9qPiNlWe4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>