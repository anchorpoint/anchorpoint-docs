# Folders and Tasks from CSV files

> ℹ️ Requires a Team plan

You can create folders and tasks from a CSV file, that you created from spreadsheet applications such as Microsoft Excel, Wiki tools such as Notion or project management applications such as ClickUp or Monday.com. 
Anchorpoint will also create Attributes from the columns in the CSV file. If you re-import the CSV, Anchorpoint will merge new entries with existing tasks/ folders. For tasks, the name of the task list has to match with the name of the csv file.

Make sure to enable the CSV importer in your Workspace Settings / Actions

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/0742cda7cc644283aa98436620548920?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Settings

**Match Names**
Pick the row of the CSV file which will be used to create the task/ folder names

**Match Attributes**
Shows all rows of the CSV file and a dropdown with all Attributes in Anchorpoint. 
If you want to match the Member Attribute, the username or email in the CSV has to match the Anchorpoint username or email. 

**Overwrite existing Attribute Values**
If you changed values (e.g. a tag, a member or a description) in your e.g. project management application, you can overwrite the values in Anchorpoint, when the option is enabled. If not, Anchorpoint will not change tasks/ folders with existing Attributes.
