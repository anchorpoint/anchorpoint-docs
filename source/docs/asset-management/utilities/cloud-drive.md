# Cloud Drive

> ℹ️ Deprecated. Not actively maintained and will be replaced in the future.

Anchorpoint has a Cloud Drive Action that can mount various cloud storage providers (AWS, Backblaze, Wasabi or Azure Blob Storage) as a network drive. These are often cheaper and more flexible than ready-made solutions like Dropbox or Google Drive. For example, you can choose the data center where the data is stored. 
Data is stored in so-called buckets or containers. You can imagine this like a virtual hard drive.

### Caching

In the past, you may work with VPN and encountered the issue that it slows you down. That’s not the issue with a cloud drive. A remote cloud drive, which is mounted using Anchorpoint, operates with a file cache. Let’s say you copy files from your Desktop to the cloud drive. Under the hood, files are copied into a cache and then uploaded. Or when you open a file, e.g. in Blender, all linked textures are downloaded and stored in the cache. So when you open the same file the next time, the textures don’t need to be downloaded again. That gives you almost the same speed as working on your local disk.

## Troubleshooting

### I have created folders and they do not show up in Cloud Drive
A cloud drive does not recognize empty folders. You have to put files there so that the folders where the files are in are also uploaded.

### I cannot delete empty folders when mounting a cloud drive
Cloud storage providers like Wasabi offer features like object lock and versioning. These features can block certain operations like deleting or renaming. Turn them off and try again.

### My computer crashes on mounting a drive
We have occasionally produced problems with antivirus software in the past. Turn off your antivirus software temporarily and try again.

### The cloud drive feels slow
When you are on Windows, sometimes updating WinFSP can help. Download [WinFSP](https://winfsp.dev/rel/) and run the installer. It can also help to update the [Rclone executable](https://rclone.org/downloads/) on Windows (it's located in C:\Users\"username"\Documents\Anchorpoint\actions\rclone) and macOS.

### I can not connect to the cloud
Check the ransomware settings in Windows. Sometimes it may also be because certain connections to cloud servers are not allowed in your country. Check your VPN settings here.

### I get installation errors
Check your ransomware settings. On Windows you can also install [WinFsp](https://winfsp.dev/rel/) (which is the driver for the FUSE system, that is required for the cloud drive) manually.

### Cannot load settings
Anchorpoint cannot read a settings file that also contains the configuration password. Do the following:

1. Go to "C:\Users\USERNAME\AppData\Roaming\Anchorpoint Software\Anchorpoint\actions"
2. Remove the "settings.json" file (make a backup copy before)
3. Try to mount the drive again.

You have to enter the configuration password again. If you can't find it anymore, just open the old settings file and search for: "encryption_password".