# Drive mapping

> ℹ️ Requires a Team plan

Whether you reached the path length limit on Windows by having a deep folder structure or you work in a team using Dropbox and everyone has their Dropbox folder in a different place, you can map the drive letter. This will show up next to all drives and look like an actual hard drive.

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/N7inQVh1rJw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>
