# ZIP

> ℹ️ Requires a Team plan

With the Zip utility, you can perform basic zipping and unzipping of .zip and .rar archives. Furthermore, it provides advanced filter options to avoid certain file types or old file versions when you want to create an archive of your project.

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/8388a0fd7c934518bd55739bb7ce3ca7?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Settings
Settings for Zip and Unzip can be accesses by clicking on the little gear icon on the context menu.
### Zip settings
Ignore files
Exclude certain file typed (e.g. blend1) from the archive

Ignore folders 
Exclude certain folder names (e.g. auto-save, caches...) from the archive

Archive name 
The name of the archive

Exclude old incremental saves
If you use incremental version stacks such as asset_v01, asset_v02 and so on, Anchorpoint will only include the latest increment in that archive and ignore the rest. Usually you don't want to archive older versions, but keep only the latest.

### Unzip settings
Delete archive after unpacking
If the unzip procedere was successful, Anchorpoint will remove the .zip or .rar file, to clean up space.