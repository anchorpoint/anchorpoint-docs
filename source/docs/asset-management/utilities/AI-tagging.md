# Tag assets with AI

Visual assets such as images or 3D-models can be tagged using OpenAI's GPT-4o mini-model to create a searchable asset library. In addition, folders can be summarized by reviewing the contents and adding high-level tags that summarize what is contained within. 
Tags are created as attributes that are searchable by all team members using Anchorpoint. 

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/dabaa975b02241868da6fd2340aad4dd?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### About the developer
This Action is a community contribution and was developed by Aleksandr Shimanov aka Hermesis Trismegistus and requires an OpenAI [API key](https://platform.openai.com/settings/organization/api-keys). Please consider a [donation](https://ko-fi.com/hermesistrismegistus), if you liked this Action. 
If you would like to provide feedback, join our [Discord](https://discord.com/invite/ZPyPzvx) server and mention @hermesis to reach out to the developer directly. 

## Installation

1. Go to "Workspace Settings" / "Actions" and click on "Import"
2. Paste this Repository URL `https://github.com/Hermesiss/anchorpoint-action-openai-tagger`
3. Click on "Connect Repository"
4. When the repository is connected, hover over the imported Action package called "OpenAI file tagging" and click on "Settings"
5. Create an API Key on [OpenAI](https://platform.openai.com/settings/organization/api-keys) and paste it
6. Click on "Apply" and the Action is ready to use

### Note on OpenAI API keys and costs

You must have an OpenAI account and deposit at least $10. An active ChatGPT subscription is not required. In terms of cost, using GPT-4o mini is inexpensive. Tagging a set of 100 images will probably cost between $0.01 - $0.02. The Action will show a popup with a cost estimate for each trigger.

## Usage

You can select any image or 3D model that has a thumbnail and select "Tag File Content with AI" from the context menu. By default, 3 attributes are created:
- AI types: The category of the asset, such as texture, model, etc.
- AI Genres: A style genre such as casual, nature, cyberpunk.
- AI objects: It will try to recognise objects in an image such as cats, dogs, trees or tables.

If you select a folder, you can select "Tag Folder with AI" from the context menu. The difference between tagging files and tagging folders is that when you tag a folder, the action reads all the file and sub-folder names in the selected folder and makes a summary. It doesn't upload the whole folder contents to OpenAI. By default it will create these 3 attributes:
- AI engines: If it detects a game engine related file, it will display it here.
- AI-Types: Similar to AI-Types in file tags
- AI-Genres: Similar to AI-Genres in file tags 

You can disable certain attributes in the Action settings by clicking the cogwheel icon next to the Action entry in the context menu. This will also save tokens, resulting in cost savings.