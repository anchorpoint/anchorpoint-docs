# Art assets basics

Anchorpoint works like a normal file browser, can manage metadata and has several utilities to organize art assets (Blender, Cinema 4D, Maya, Zbrush, Substance or similar).

It does not duplicate files and does not create a cryptic database. Anchorpoint stores metadata in the cloud so it can be shared with other team members. Here you can find more information about [metadata](../1-overview/1-Production-and-metadata.md).

## Working with referenced art assets

<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/d972f81ca5de47ef8982c37d21a12314?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

This video showcases a basic workflow when working with 3D assets in a Git repository or a shared drive such as Dropbox or Google Drive, that need to be referenced in another location. You can extend this workflow with [Attributes](attributes.md) (tags, comments etc.) and [Templates](templates.md).

## Thumbails

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe src="https://www.loom.com/embed/c6d0b1e5370f4bf98855e93b90aecebf?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

Anchorpoint can natively display thumbnails for the following file formats: C4D, Blend, Max, OBJ, FBX, GLTF, SBSAR, SBS (requires an active Substance installation), PSD, PSB, HDR, EXR, AI, Uasset, Umap and all other common text, image and video formats. 

If a thumbnail cannot be displayed, you can use the **Replace Thumbnail** feature in the context menu to set a thumbnail on each file. You can also access it by selecting a file and pressing **SHIFT+S**.


:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Browser options
   :glob:

   basics/*
:::