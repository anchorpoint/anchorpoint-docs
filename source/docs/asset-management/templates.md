# Templates

> ℹ️ Requires a Team plan

Create reusable templates for your project and folder structures. Setup naming conventions, so you never have to rename a file or folder again. 

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7v8gId1GeBQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
## Tokens
If you want to keep consistent naming conventions, you can use tokens e.g. "[name]" in your file and folder names. Anchorpoint will rename these tokens to normal filenames by showing you a textfield to enter the proper name. Besides that, some tokens can be automatically resolved:

- [YYYY] -> The current year
- [YYYYMM] -> The current year and month
- [YYYY-MM] -> The current year and month, separated by a "-"
- [YYYYMMDD] -> The current year, month and day
- [YYYY-MM-DD] -> The current year, month and day, separated by a "-"
- [ProjectFolder] -> The name of the root project folder
- [ParentFolder] -> The name of the folder in which the template was instantiated (by using Create from Template)
-  [ParentParentFolder] -> The name of the parent folder in which the template was instantiated (by using Create from Template)
- [ParentParentParentFolder] -> The name of the grandparent folder in which the template was instantiated (by using Create from Template)
- [User] -> The name of the member creating the folder from the template
- [UserInitials] -> The initials for the user name e.g. Mat Newman will be MN
- [Increment] -> Looks at the number of folders and files and adjusts the number e.g. 0010, 0020, 0030
- [Inc####] -> Looks at the number of folders and files and adjusts the number like this: 0001, 0002
- [Inc###] -> Looks at the number of folders and files and adjusts the number like this: 001, 002
- [Inc##] -> Looks at the number of folders and files and adjusts the number like this: 01, 02
- [Inc#] -> Looks at the number of folders and files and adjusts the number like this: 1, 2

## ## Sharing templates with your team
If you want your team members to use templates too, they must have access to the template folder. This is best located on a NAS or a Dropbox that points to the same path via Map to Drive. Anchorpoint stores two different paths for Windows and macOS for templates. 

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe src="https://www.loom.com/embed/4801495ca8334340800df8f2e88d833c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>