# Attributes

> ℹ️ Requires a Team plan

Set tags, statuses, deadlines or add notes using Attributes. They are an additional interface element on your file or folder. Attributes are stored in views. Think like adding a column in an Excel spreadsheet.


<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/52c5726cb7f74de4ac6484f9530a8755?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Where can I use Attributes
You can create Attributes on any file, folder or task.


### How are Attributes stored
Attributes are stored in our cloud. Every Attribute you create is global. This means that it can be reused anywhere in a project.
For example, if you have a "Status" Attribute, you can use it in your asset or shot folder and don't have to create it twice.

## Access control
Members can only add/rename and delete attributes or tags if they are either a Workspace Admin or a Project Admin. You can give members Project Admin rights in the "Project Settings" / "Members" by clicking on the small shield icon. 

Members who don't have Project Admin rights can only assign existing attributes to tasks, files or folders.

## Filtering
Attributes can be filtered. You can either use the **Quick Find** function (CTRL-F) to quickly search for names, tags or descriptions.
If you want to build a more advanced filter, you can click on the Filter button, which will show you all available Attributes. Attributes do not have to be visible to be filtered.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/9de961bbd55243e0a63ccf5dfb103c1b?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Filtering file types globally
If you want to filter out certain file types (e.g. blend1, meta, tmp) that are created by applications but clutter your browser, you can do so in the application settings. Go to "Workspace Settings" / "Application" and scroll down to "Hidden Files". Add the file types you want to hide.

![](attachments/hidden-files.png)
These file types are hidden from the browser and search results for all projects.
## Share your view with your team
In Anchorpoint you can specify that everyone has the same view of a folder. If you change the view (e.g. from list to grid) or create a filter, you can pass these settings to your team via **Save your view for everyone**.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/650a823b413140ee9bb6d41791afce6b?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Export as CSV
You can also export your attributes on files, folders and tasks as CSV for other tools like Notion, Excel or Airtable. Click on the "..." button in the top right corner of the view settings and select "Export as CSV".

Anchorpoint exports all visible entries with all visible attributes and creates the CSV file in the same folder.

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Attribute options
   :glob:

   attributes/*
:::