# Sharing file locations

Sending a folder or file path to a team member is pretty daunthing. Filepaths are different on Windows and MacOS and when you work on a Dropbox, not everybody is syncing their files to a folder with the same path. 
Anchorpoint allows you to use app links, that make location sharing as easy as sharing a website link. Open the context menu on any file or folder and choose **Copy App Link**. You can also press **Ctrl-Shift-C**. 
Then, paste the link in Slack, Notion or any web application.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/3c34807dee414686b3b453045f9e25b3?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>


### Opening an app link
Once a user clicks on a link, Anchorpoint will be opened and navigates to the file or folder. The member has to be part of the project and the project has to be connected to the project files, otherwise Anchorpoint won't be able to find the correct file.

