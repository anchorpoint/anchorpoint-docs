# Stacking

Stacking allows you to save additional subfolders, when you want to organize assets. Anchorpoint supports stacking based on incremental saves and image sequences.


<div style="position: relative; padding-bottom: 62%; height: 0;"><iframe src="https://www.loom.com/embed/e4042718603a4d9db1dca7a2afcda96a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Creating referenced files
This is also called publishing. You can also create a referenced file from an incremental version stack, which you can then load in other projects. In Cinema 4D, for example, this is an XREF, in Maya a referenced model and in Blender a link.

In Anchorpoint you have to activate this function in the actions first. Go to the **Workspace Settings** / **Actions** and activate **Create References File**. Then you get **Create Referenced File** as context menu entry.

You can set in the settings (if you click on the gear) if the referenced files should be placed in a special folder and if an appendix (i.e. a _published) should be added to the file name.

After executing the action, a source file attribute is also created, which contains the name of the original file.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/9bb08902702642538bc686ecb1914ad3?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

  ### Using relative filepaths
  Instead of using absolute filepaths to create your referenced file, you can also use relative ones, starting with a "../". For example
  - "../" will save your file to the parent folder
  - "../published" will save your file to a folder called "published", which is in the parent folder
  - "../../" will save your file in the parent folder of the parent folder. This will basically go two folders up.