# Color management

To correctly display renderings saved via EXR files, Anchorpoint supports ACES color profiles. You can use the existing configuration or choose your own configuration file. 
Anchorpoint saves this setting per project, so no one in the team has to worry about it. Only the "Display" setting is saved locally, because it depends on local configurations like a calibrated monitor.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/b8d4624fd47f47c585be9a6c706cd20c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

To detect the correct color profile of an EXR file Anchorpoint uses the following rule:

1. if the color profile is mentioned in the file name, this is used (e.g. render_acescg.exr).
2. if point 1 is not given, then the setting from the dropdown "Color Config" is used

## External configurations
Anchorpoint supports OCIO configurations which can be downloaded e.g. [here](https://opencolorio.readthedocs.io/en/latest/quick_start/downloads.html). Make sure that everyone in the team has access to this configuration. Configurations can also be stored as environment variables. This is often used in pipelines to share configurations between different applications.