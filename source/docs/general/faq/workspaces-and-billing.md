# Subscription management

This article aims to provide a clear overview of how to create and manage workspaces, understand billing connections, and efficiently add or manage members within your workspace.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/ab1677bb593747a0a38cc62b5143f166?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Workspaces Overview

- A workspace serves as the central hub for your projects and team collaboration, similar to a Slack workspace or a Discord server.
- You can view and switch between workspaces you belong to from the top left corner. Each workspace typically represents your studio or organization.

## Managing Members

- Within a workspace, there is one owner, who is responsible for subscription and billing. Owners can invite members, assign roles (admin or member), and activate or deactivate users.
- Adding or managing members does not directly affect billing. Your subscription dictates the number of members you can include in your workspace.
- Members need to be explicitly assigned to projects within the workspace; simply inviting them does not grant access to any project resources.

## Billing and Subscriptions

- Changes in workspace membership do not impact your billing. Adjustments to your team size, such as adding more seats or reducing them, are managed in the "Plans" section.
- You can modify your subscription to add or reduce seats, choosing between annual or monthly payment options. Any changes require management through the customer portal.

## Assigning Members to Projects

- Assign invited members to specific projects for them to access relevant project folders or Git repositories.
- Members can be assigned to projects at the time of invitation or when creating new projects.