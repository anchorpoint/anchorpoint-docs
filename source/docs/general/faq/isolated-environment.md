# Isolated environment

If you want to work with Anchorpoint on a system where the Internet connection is restricted (e.g. generally not allowed), but you want Anchorpoint to remain connected to the server, you will need to open the following ports in your firewall.

## Server backend
This is necessary to access the metadata server. You must open port 443. Anchorpoint uses the transfer protocol gRPC (which is based on HTTP/2) to transfer the metadata to the backend. The https requests (REST API) are used for account management. 

`https://cloud.anchorpoint.app/`

`grpc--cloud.anchorpoint.app`

## Web pages
This is required to authenticate to Anchorpoint the first time you use the application.

**Our website**

`https://www.anchorpoint.app/`

**Anchorpoint's authentication service "Auth0"**

`https://anchorpoint.eu.auth0.com/`

## Auto updates
If you want the Anchorpoint updater to fetch and install new updates. (Optional)

`https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/RELEASE`
