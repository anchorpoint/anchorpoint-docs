# Confusion around Git

Git is complex and due to its huge community you will hear a lot of half-knowledge and false statements in online forums. Here is a basic understanding of Git that will help you clarify support requests. 

## Git is not the same as GitHub
Git is the version control system. GitHub is a cloud platform based on the Git version control system. Anchorpoint can connect to and push files to GitHub because both are based on the Git system. Next to GitHub there are also alternatives such as Azure DevOps, Bitbucket, GitLab and Gitea.

## What are file limits on Git?
Git [does not have hard file limits](https://www.anchorpoint.app/blog/scaling-git-to-1tb-of-files-with-gitlab-and-anchorpoint-using-git-lfs). You may have heard of repository limits coming from cloud platforms like GitHub or GitLab. If you use Anchorpoint to configure Git LFS for you, you should never run into these limitations.
Unfortunately, GitHub also has [limits on individual files](https://docs.github.com/en/repositories/working-with-files/managing-large-files/about-git-large-file-storage) (even with using Anchorpoint). Don't ask us why. If this is an issue for you, consider another Git platform such as GitLab or Azure DevOps. 

## What is Git LFS and why do I need it?
Git LFS is an extension to Git that can handle binary files. If you push binary files without Git LFS, you will quickly hit the limits of your cloud storage providers. For example, [GitLab has a limit of 10GB](https://docs.gitlab.com/ee/user/usage_quotas) per repository and 100MB per file. When you use Git LFS, [these limits don't exist](https://www.anchorpoint.app/blog/scaling-git-to-1tb-of-files-with-gitlab-and-anchorpoint-using-git-lfs). There is no reason not to use Git LFS.

When you use Anchorpoint, the Git LFS is handled automatically. You do not need to worry about it. Other Git clients support Git LFS as well.


## Do I always need a Git remote server (e.g. GitHub)?
No, you can work without a server. In Anchorpoint, you can select the "No Remote" option when creating a Git repository. This may be sufficient if you are working alone and just need a version control system to go back to if something breaks. Also keep in mind that if your computer goes down, you won't have a backup system and you won't be able to share those files with a team.


## Can I put a Git repository in a Dropbox, GDrive etc?
NEVER DO THIS unless you know exactly what you are doing? Sync tools like Dropbox will erroneously change your files in your Git repository, and Git will think you made those changes. You will end up with a lot of falsely changed files. If you want to share files with your team, use a Git cloud platform such as Azure DevOps or GitHub.


## Is GitHub the best solution for version control?
It's definitely the most popular and well-known, but that doesn't mean it's the best for large binaries. In fact, it has [hard limits on file size](https://docs.github.com/en/repositories/working-with-files/managing-large-files/about-git-large-file-storage), which can be a dealbreaker if you have large binaries. We recommend to take a look at alternatives such as Azure DevOps or a self hosted GitLab or [Gitea solution](https://www.anchorpoint.app/blog/install-and-configure-gitea-for-lfs), which don't suffer from these limitations. 


## Can I use another Git client like SourceTree together with Anchorpoint?
Yes, you can. But keep in mind that certain features, like selective checkout or cleaning up the Git LFS cache, are only available in Anchorpoint. Another Git client might mess things up, because it does not have these features. If you don't use these Anchorpoint exclusive Git features, you'll be fine with another Git client. There is also no problem using Anchorpoint file locking.

What you can always use is the git command line, because it understands everything.


## I see errors in Anchorpoint and don't know what to do
First, check our Git [troubleshooting](../../version-control/troubleshooting.md) page to help yourself. Many issues occur in combination with a Git cloud platform, so you might check the subtopics there as well. 
