# Self-hosting

> ℹ️ Requires an Organization plan

Anchorpoint never uploads your production files to our cloud server. It only processes metadata. This requires Anchorpoint to communicate with our cloud. If your organization's security requirements do not allow external communication or prohibit storing metadata on external servers, you can host the Anchorpoint server yourself.

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Self hosting topics

   self-hosting/server-setup
   self-hosting/self-host-cli
   self-hosting/sso-setup
   self-hosting/managing-users
   self-hosting/user-login
   self-hosting/metrics
:::
### How the Anchorpoint cloud server works

Anchorpoint communicates with a server to exchange metadata and project information. Metadata includes:
- File locking information
- Attributes, comments, and images attached to comments (coming soon)
- Real-time updates and notifications

Project information includes:
- Workspace information
- License quotas (but no payment information)
- Projects
- Member information (including their email addresses, not login credentials)
- Project icons

All this information is stored on a server hosted by the Infrastructure as a Service (IaaS) provider Amazon Web Services (AWS). The corresponding data centers are located in Frankfurt, Germany, with instances running on eu-central-1a, eu-central-1b, and eu-central-1c. This server also communicates with the user authentication system [Auth0](https://auth0.com/) for managing login credentials and the payment processing provider [Stripe](https://stripe.com/en-de) for managing payment information.

The Anchorpoint server never stores production data, which is typically located on a Git server of your choice or a shared folder that is synced through solutions such as your NAS, Dropbox, Google Drive, or similar. Read more about application infrastructure and security in our [security brief](https://www.anchorpoint.app/policies/security).

### Git servers

As mentioned above, Anchorpoint does not store production data. If you want to use Anchorpoint with Git, you will need to install a self-hosted [Gitea](https://www.anchorpoint.app/blog/install-and-configure-gitea-for-lfs) or GitLab instance and configure it properly for Git LFS. Again, our Solutions Architect can help.

## Self-hosting the Anchorpoint server

If you need complete control over your data flow and storage, the Anchorpoint server can also be self-hosted on your own VPC, such as an Amazon EC2 instance, or in your local network. The self-hosted server never talks to our cloud or any of our systems for maximum compliance.
If you are on an organization plan that includes the self-hosting option, our team will assist you in setting up this server, support and updates.

## Components

The Anchorpoint Server consists of 6 components, that are bundled in a Docker Compose created by our self-hosting cli tool.

1. The Anchorpoint Backend as a metadata server, which processes Anchorpoint metadata and interacts directly with the Anchorpoint Desktop Client and the database.
2. An auth provider (Keycloak) for user authentication. It also enables SSO using providers such as Azure Active Directory.
3. A reverse proxy (Traefik) to serve different containers over the same ports.
4. A Message Broker (RabbitMQ)
5. A database (PostgreSQL) to store the metadata from the Anchorpoint backend and userdata from the auth provider
6. An object storage (such as AWS S3 or minio) for storing project thumbnails and other binary data

## Application Architecture

The application architecture is similar to the [Anchorpoint Cloud Solution.](https://www.anchorpoint.app/policies/security) Unlike the cloud solution, the self-hosted server does not send or store data outside of your environment. Nor does it have:

- A connection to our payment processor
- A connection to our user identity management system

There are also limitations to the desktop application, which does not have the option to start a self-hosted trial or upgrade plans. However you can request a trial license for the self-hosted server to test the self-hosted environment with the desktop application.