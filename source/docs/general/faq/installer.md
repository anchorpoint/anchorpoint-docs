# Installer
On Windows, Anchorpoint is installed per default in the appdata folder. On macOS you have to move it to the applications folder via drag and drop.
## Custom MSI installer for Windows
Anchorpoint's MSI installer is for IT departments that want to automatically install Anchorpoint on multiple Windows machines with more control. This is the case if you have a computer pool in your office where multiple people log on to the computer at different times (e.g. shared office, university lab).

### How does it work
Anchorpoint is typically installed in the application in the current user's appdata directory. 
Anchorpoint also provides the ability to create an MSI installer that installs itself into the program directory of any Windows device on which it is run. 
Each time a new user logs on to that device, the installed program will automatically install Anchorpoint for the current user in their appdata directory on that device. If the same user logs in on the same computer again, nothing happens, because the application is already installed.

### Technical details
Anchorpoint uses Squirrel for Windows as an installer framework. If you would like to learn more about Machine-wide Installs in Squirrel, please see the [Squirrel Machine-wide Installs documentation](https://github.com/Squirrel/Squirrel.Windows/blob/develop/docs/using/machine-wide-installs.md).
### Where can I get the MSI installer?
Contact us via support@anchorpoint.app and we provide you with a link to the MSI installer.
### How do updates work with the MSI installer?
The Anchorpoint update process is the same as a normal installation. Squirrel will update the Anchorpoint installation after the user launches the application.

To avoid updating the application with each new user login, you can also update the MSI installation on your Windows devices with a new version of Anchorpoint.