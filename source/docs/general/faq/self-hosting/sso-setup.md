# SSO Setup in Keycloak

> ℹ️ Requires an Organization plan

In this guide explains how to setup another identity provider for `Single Sign On` via Keycloak.

1. Login with the keycloak admin account under SERVER_URL/auth/admin. You can find the password for the keycloak admin account in your `.env` file under `KEYCLOAK_PASSWORD` in your installation folder.
2. Change the realm to the `anchorpoint` realm on the top right where `Keycloak` is selected by default.

    ![sso rleam selection](attachments/sso_realm_selection.webp)
3. Setup your Identify Provider
    1. Click on `Identity providers` on the right sidebar menu.
    2. Setup your identity provider: 
        - `OpenID Connect` provider setup guide [here](https://medium.com/@ayoubchamchi/keycloak-and-idp-configuration-a4afff4bb12d), 
        - `SAML` provider setup guide [here](https://doc.psono.com/admin/configuration/saml-keycloak.html)
        -  `Azure AD` provider setup guide [here](https://docs.virtimo.net/de/bpc-docs/4.2/core/admin/identity_provider/idp_keycloak_microsoft.html)
        - You can also find other guides by searching for "keycloak idp \<name of the idp\>" on your search engine
4. Create a new Authentication flow for user account linking
    1. Click on `Authentication` on the left sidebar
    2. Click on `Create flow`
    3. Set the name to `auto-link-browser` and the description to `link existing user accounts`. Set the `Flow type` to `Basic flow` and click on `Create`
    4. Click on `Add execution` and choose `Create User If Unique` and click `Add`
    5. Set the `Requirement` of the `Create User If Unique` to `Alternative`
    6. Click `Add Step` and choose `Automatically set existing user` and click `Add`
    7. Set the `Requirement` of the `Automatically set existing user` to `Alternative`
    8. Open the created Identify Provider from step 3 and scroll down to set `First login flow override` to `auto-link-browser` and press `save`

    ![sso auto link browser flow](attachments/sso_auto_link_browser_flow.webp)
5. Create another new Authentication flow for the Anchorpoint client
    1. Click on `Authentication` on the left sidebar
    2. Click on `Create flow`
    3. Set the name to `ap-client-browser` and the description to `auth flow for Anchorpoint client browser login`. Set the `Flow type` to `Basic flow` and click on `Create`
    4. Click on `Add execution` and choose `Cookie` and click `Add`
    5. Set the `Requirement` of the `Cookie` to `Alternative`
    6. Click `Add Step` and choose `Identity Provider Redirector` and click `Add`
    7. Set the `Requirement` of the `Identity Provider Redirector` to `Alternative`
    8. Click on the `Settings` icon of the `Identity Provider Redirector` and set `Alias` and `Default Identity Provider` to the name of the identity provider you created in step 3 and click `Save`

    ![sso ap client browser flow](attachments/sso_ap_client_browser_flow.webp)
6. Adjust the default browser Authentication flow
    1. Click on `Authentication` on the left sidebar
    2. Click on `browser` flow (usually the first entry in the list)
    3. Click on `Settings` icon of the `Identity Provider Redirector` and set `Alias` and `Default Identity Provider` to the name of the identity provider you created in step 3 and click `Save`
    4. Make sure that `Identity Provider Redirector` requirement is set to `Alternative`
7. Overwrite the Authentication flow of the anchorpoint-desktop-client
    1. Click on `Clients` in the right sidebar
    2. Click on the `anchorpoint-desktop-client-...` client to edit it
    3. Click on the `Advanced` tab
    4. Scroll all the way down to `Authentication flow overrides` and choose `ap-client-browser` for `Browser Flow` and click on `Save`

