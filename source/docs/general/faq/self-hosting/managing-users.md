# Managing users

> ℹ️ Requires an Organization plan

Once you have installed all components on your server and added a valid license, the first step is to create user accounts. Open your SERVERURL/dashboard and log in as the main sysadmin.

Username: admin@anchorpoint.app 
Password: admin

You will be asked to change the password after the first login. The next step is to add your users using their email addresses.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/00a83f02ff0745598904749bd3adc8e0?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

The sys-admin account (the account used to log in to the dashboard) cannot be used with the Anchorpoint application. For that, you need a normal user account. If you want to login to the Anchorpoint client on the same device as you setup the user invite, please logout from the dashboard when you are finished with user invites.