# Self hosting cli tool

> ℹ️ Requires an Organization plan

Download our cli tool for linux [here](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/SelfHosted/ap-self-hosted-cli/ap-self-hosted-cli-linux-amd64). Make sure to make it executable (chmod +x) on your machine.
Here are the cli tool commands that are used to setup and maintain the Anchorpoint self hosted docker compose stack.

## install

Install Anchorpoint self-host stack

### Synopsis

Install and configure the Anchorpoint docker-compose self-host stack with all the necessary components like:
	
	- AP-Backend
	- Traefik
	- RabbitMQ
	- Keycloak
	- Postgres (optional)
	- Minio (optional)

```
selfhost-cli install [flags]
```

### Options

```
      --domain string               Domain name or IP for the installation (env AP_DOMAIN)
  -h, --help                        help for install
      --install_dir string          Installation directory (env AP_INSTALL_DIR)
      --lets_encrypt string         Use Let´s Encrypt (only used when ssl is true) (env AP_LETS_ENCRYPT)
      --lets_encrypt_email string   Email for Let's Encrypt (optional) (env AP_LETS_ENCRYPT_EMAIL)
      --license string              License key for installation (env AP_LICENSE)
      --minio                       Install Minio (env AP_MINIO) (default true)
      --non-interactive             Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE)
      --postgres                    Install Postgres (env AP_POSTGRES) (default true)
      --ssl                         Enable SSL for the installation (env AP_SSL)
```

## restart_ap_backend

Restart the ap_backend container

### Synopsis

Restart the ap_backend container from the existing ap stack installation

```
selfhost-cli restart_ap_backend [flags]
```

### Options

```
  -h, --help                 help for restart_ap_backend
      --install_dir string   Installation directory (env AP_INSTALL_DIR)
      --license string       License key for installation (env AP_LICENSE)
      --non-interactive      Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE) (default true)
```  

## start

Starts the Anchorpoint self-host stack

### Synopsis

Starts the Anchorpoint self-host stack from the install directory created by the install command.

```
selfhost-cli start [flags]
```

### Options

```
  -h, --help                 help for start
      --install_dir string   Installation directory (env AP_INSTALL_DIR)
      --non-interactive      Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE)
```

## stop

Stops the Anchorpoint self-host stack without data removal

### Synopsis

Stops the Anchorpoint self-host stack and removes its containers. It does not remove the install directory or any data directories.

```
selfhost-cli stop [flags]
```

### Options

```
  -h, --help                 help for stop
      --install_dir string   Installation directory (env AP_INSTALL_DIR)
      --non-interactive      Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE)
```

## check_update

Check if update for Anchorpoint self-host stack is available

### Synopsis

Check if an update for the Anchorpoint docker-compose self-host stack for e.g.
	
	- AP-Backend
	- AP-Client
	- Compose Files
	
	is available

```
selfhost-cli check_update [flags]
```

### Options

```
  -h, --help                 help for check_update
      --install_dir string   Installation directory (env AP_INSTALL_DIR)
      --license string       License key for installation (env AP_LICENSE)
      --non-interactive      Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE) (default true)
```

## update

Update Anchorpoint self-host stack

### Synopsis

Update and configure the Anchorpoint docker-compose self-host stack with all the necessary components like:
	
	- AP-Backend
	- AP-Client
	- Compose Files

```
selfhost-cli update [flags]
```

### Options

```
  -h, --help                 help for update
      --install_dir string   Installation directory (env AP_INSTALL_DIR)
      --license string       License key for installation (env AP_LICENSE)
      --non-interactive      Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE)
```

## update_license

Update the license

### Synopsis

Update the license for the Anchorpoint self-host stack.

```
selfhost-cli update_license [flags]
```

### Options

```
  -h, --help                 help for update_license
      --install_dir string   Installation directory (env AP_INSTALL_DIR)
      --license string       License key for installation (env AP_LICENSE)
      --non-interactive      Enable non-interactive mode for the installation (env AP_NON_INTERACTIVE)
```

