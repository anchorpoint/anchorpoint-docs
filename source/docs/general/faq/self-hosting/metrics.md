## Metrics

> ℹ️ Requires an Organization plan

In this section you get an overview over the grafana dashboards and how you can search in the collected log files of all docker containers. Therefore you have to login to the grafana dashboard via SERVERURL/grafana and use the `GRAFANA_USER` and `GRAFANA_PASSWORD` that where generated in your `installDir/.env` file.

## Dashboards Overview

You can find the overview over all dashboards under SERVERURL/grafana/dashboards. You can click on any dashboard to view it.

### Docker Containers

In this dashboard you can see all metrics of the current running docker containers on the device. This could show more containers if you also run any other docker containers on your device. You can see if any container spikes in cpu or memory usage here.

![Docker Containers](attachments/docker_container_dashboard.webp)

### Docker Host

This dashboard shows the docker host metrics.

![Docker Host](attachments/docker_container_host.webp)

### Monitor Service

The monitor service dashboard shows prometheus metrics.

![Monitor Service](attachments/monitor_service.webp)

### Node Exporter Full

The node exporter dashboard shows all metrics of the device that are exported by the node exporter container. Here you can also check disk space and network metrics of the device.

![Node Exporter Full](attachments/node_exporter_full.webp)

### RabbitMQ-Overview

The rabbit mq dashboard shows internal stats of the rabbit mq docker container running in the stack. The most valuable metrics here are the `Consumers` and `Channels` which represent in our case channels for connected users to the ap_backend docker container. This should normally not exceed the number of members that are currently connected to your stack.

![Node Exporter Full](attachments/rabbit_mq_overview.webp)

## Searching in logs

Under SERVERURL/grafana/explore you can search metrics via `Prometheus` data source or logs via the `Loki` datasource. In the `Loki` datasource you have to choose a label and value. If you e.g. want to show logs for the ap_backend container you have to select label container and value ap_backend, specify a time range on the top right and press `Run query` to search for logs in your selected time range.

![Loki Search](attachments/loki_search.webp)