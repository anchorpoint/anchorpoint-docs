# Login as a team member

> ℹ️ Requires an Organization plan

Once you have received your account information, you can download the application from SERVER_URL/download and log in to the self-hosted Anchorpoint server. Your account information should include
- The URL to the server
- Your username (as an email)
- Your temporary password

Make sure you can see the login screen. If not, you need to log out by clicking on the "..." menu and selecting "Account Settings" -> Logout from Account.

If you can see the login screen, click on the connector icon in the bottom right corner. Then enter your URL and restart the application. Anchorpoint is now in "self-hosted" mode.
Once the application has restarted, click on the main login button and follow the instructions in your web browser.

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/cf6a980607bf4c5caa78701ca5f94753?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Quit self-hosting mode

If for any reason you need to use the normal Anchorpoint Cloud version, you can exit self-hosted mode by clicking the connector icon in the bottom right corner and then clicking "Disconnect".