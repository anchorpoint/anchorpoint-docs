# Server Setup

> ℹ️ Requires an Organization plan

## Technical Requirements

The server can be installed on any system that meets the following requirements. In addition, a user identity management service such as Azure AD or LDAP can be provided for authentication.

### AWS or other Cloud Provider

- One EC2 instance (at least T3 large for about 15 users)
- S3 storage for metadata such as project icons
- PostgreSQL (e.g. AWS RDS) (optional, default is a postgres server running inside the docker compose stack)
- Email service (optional)

### None Cloud Provider

- Machine running latest version of Linux (e.g. Ubuntu Server, CentOS, Alpine Linux)
- Docker-engine version > 23.0.5 
- Docker-compose version > 2.18.0

### Other Requirements

Make sure your firewall supports HTTP2 connections. Anchorpoint uses the gRPC protocol to communicate with the server, which is based on HTTP2. We highly recommend that you start an Anchorpoint cloud trial to evaluate whether it will work in your environment.

## Getting started

In the following sections we describe the setup process using our cli tool. We have a look at optional components that can be used and how to setup alternative solutions (e.g. for the database).

### Licensing

Licensing depends on the number of users you will have. You need to [contact us](mailto:support@anchorpoint.app?subject=Organization%20licensing%20question) for a quote. You can also request a free trial license to test the self hosted environment. We also offer volume discounts if your number of users is higher than 25. If you agree, we will send you a payment link. Once the payment is made, we will send you the license key.

### Cli Tool

Download our cli tool for linux [here](self-host-cli). A documentation of all its commands can be found [here](self-host-cli). The commands will be used in the following sections.

### Install the stack

Create a folder where you want to install the docker compose stack into. Start the cli with the [install command](../self-host-cli#install). Provide your trial or subscription license key that you received from us. Set the install directory that you have created before. 

Choose the domain where you want the backend to be reachable e.g. anchorpoint.example.com or a network IP address (e.g. 192.168.178.100). Do NOT add http:// or https:// at the start of the domain.

If you want to use https you have to enable SSL or use e.g. a load balancer for ssl termination on your cloud provider or reverse proxy. If you enable ssl you will be asked if you want to use [Let's Encrypt](https://letsencrypt.org/) to generate an SSL certificate and store it in the config/letsencrypt directory in your install directory. If you provide an email address for [Let's Encrypt](https://letsencrypt.org/) you will get notified about any certificate issues. If you want to provide your own ssl certificates please choose no and follow the guide in ["How to use your own ssl certificates"](#how-to-use-your-own-ssl-certificates) in the next section.

MinIO is the s3 bucket alternative that can be installed in the stack and will save its data to the data/minio directory in your install directory by default. If you plan to use another s3 provider you will have to choose "no" and adjust the environment file that will be generated in the install directory. Check the next section for more information.

Postgres is the database server that will handle the Anchorpoint database and the Keycloak database. By default, its data is stored in the data/postgres directory in your install directory. If you plan to use another postgres server you will have to choose "no" and adjust the environment file that will be generated. Check the next section for more information.

If you want to see metrics and search logs you can use grafana, prometheus and loki which can be auto installed via the cli tool. You can access grafana via SERVERURL/grafana after the stack is started. You can checkout our [metrics documentation](./metrics.md) for more information about metrics and logs.

Check all your inputs and finish the install process. The cli tool will download the latest package and setup the docker-compose files and the needed directories for you. Before starting the stack please read the next section to make sure that your environment is correctly setup for your usecase.

### Adjust the environment file before starting the stack

The [install command](../self-host-cli#install) will generate an `.env` file in your selected install directory. This environment file contains variables for each component of the stack. You can find a description for each section of the `.env` file here. Do not share this `.env` file as it contains many credentials.

| Name                          | Default Value                           | Description                                           |
|-------------------------------|-----------------------------------------|-------------------------------------------------------|
| **Stack Global**              |                                         |                                                       |
| `COMPOSE_PROJECT_NAME`        | `ap_stack`                              | The name of the stack for Docker Compose.             |
| **Domain**                    |                                         |                                                       |
| `DOMAIN_NAME`                 | Domain from cli install                 | The domain or IP address where the stack is hosted.   |
| `HTTP_PORT`                   | `8080` or `443` from cli install        | The port for HTTP / HTTPS traffic.                    |
| `GRPC_PORT`                   | `9090`                                  | The port for gRPC traffic.                            |
| `HTTP_SCHEME`                 | `http` or `https` from cli install      | The HTTP scheme used (http or https).                 |
| **Traefik**                   |                                         |                                                       |
| `TRAEFIK_DATA_PATH`           | `./data/traefik`                        | Path to store Traefik data.                           |
| `LETS_ENCRYPT_EMAIL`          | Set from cli install when provided      | (Optional) Email for Let's Encrypt notifications.     |
| **Keycloak**                  |                                         |                                                       |
| `KEYCLOAK_USER`               | `admin`                                 | Default admin user for Keycloak.                      |
| `KEYCLOAK_PASSWORD`           | Auto generated from cli install         | Default admin password for Keycloak.                  |
| `ADMIN_CLI_CLIENT_SECRET`     | Auto generated from cli install         | Secret for the admin CLI client.                      |
| `DASHBOARD_CLIENT_SECRET`     | Auto generated from cli install         | Secret for the dashboard client.                      |
| **Anchorpoint Backend**       |                                         |                                                       |
| `LICENSE_KEY`                 | Set from cli install                    | License key for the Anchorpoint Backend.              |
| `DEFAULT_WORKSPACE_ID`        | Set from cli install                    | Identifier for the default workspace all users get invited to from the dashboard. |
| `DEFAULT_WORKSPACE_NAME`      | `AP Workspace`                          | Name of the default workspace on first creation.      |
| `DASHBOARD_SESSION_SECRET`    | Auto generated from cli install         | Secret for dashboard session.                         |
| `CRASH_REPORTING`             | false                                   | Allow crash reporting to our servers for crashes in the desktop client. Note that crashes attach log data that can contain sensitive information |
| `EMAIL_HOST`                  |                                         | Smpt email host                                       |
| `EMAIL_PORT`                  | `25`                                    | Smpt email port                                       |
| `EMAIL_USER`                  |                                         | Smpt email username                                   |
| `EMAIL_PASSWORD`              |                                         | Smpt email password                                   |
| `AP_UPDATES_DATA_PATH`        | `./data/ap_backend/updates`             | Path to provide client updates data.                  |
| **Postgres**                  |                                         |                                                       |
| `POSTGRES_PASSWORD`           | Auto generated from cli install         | Password for the PostgreSQL database.                 |
| `POSTGRES_USER`               | `postgresuser`                          | User for the PostgreSQL database.                     |
| `POSTGRES_ADDRESS`            | `postgres`                              | Address of the PostgreSQL database.                   |
| `POSTGRES_PORT`               | `5432`                                  | Port for the PostgreSQL database.                     |
| `POSTGRES_DB`                 | `ap`                                    | Database name for PostgreSQL.                         |
| `POSTGRES_DATA_PATH`          | `./data/postgres`                       | Path to store PostgreSQL data.                        |
| **RabbitMq**                  |                                         |                                                       |
| `RABBITMQ_USER`               | `aprabbitmq`                            | User for RabbitMQ.                                    |
| `RABBITMQ_PASSWORD`           | Auto generated from cli install         | Password for RabbitMQ.                                |
| `RABBITMQ_ERLANG_COOKIE`      | Auto generated from cli install         | Cookie secret for RabbitMQ.                           |
| **MinIO**                     |                                         |                                                       |
| `MINIO_ACCESS_KEY`            | `apminio`                               | Access key for MinIO.                                 |
| `MINIO_SECRET_KEY`            | Auto generated from cli install         | Secret key for MinIO.                                 |
| `MINIO_DOMAIN_NAME`           | Domain from cli install                 | Domain name for MinIO.                                |
| `MINIO_PORT`                  | `9000`                                  | Port for MinIO.                                       |
| `MINIO_DASHBOARD_PORT`        | `9001`                                  | Dashboard port for MinIO.                             |
| `MINIO_DATA_PATH`             | `./data/minio`                          | Path to store MinIO data.                             |
| `MINIO_CREATE_BUCKET`         | `true`                                  | Whether to create a MinIO bucket on startup.          |
| `MINIO_CREATE_BUCKET_WITH_POLICY` | `true`                              | Whether to create a MinIO bucket with a policy.       |
| `MINIO_BUCKET_AS_PATH_STYLE`  | `true`                                  | Whether MinIO uses path-style access.                 |
| **Other S3 Provider**         |                                         |                                                       |
| `S3_ACCESS_KEY`               |                                         | Access key for S3 storage.                            |
| `S3_SECRET_KEY`               |                                         | Secret key for S3 storage.                            |
| `S3_SERVER_URL`               | `https://s3.eu-central-1.amazonaws.com` | URL for S3 server.                                    |
| `S3_INTERNAL_URL`             | `s3.eu-central-1.amazonaws.com`         | Internal URL for S3 server.                           |
| `S3_EXTERNAL_URL`             | `s3.eu-central-1.amazonaws.com`         | External URL for S3 server.                           |
| `S3_USE_SSL`                  | `true`                                  | Whether to use SSL for S3 connections.                |
| `S3_BUCKET`                   | `ap`                                    | Bucket name for S3 storage.                           |
| `S3_REGION`                   |                                         | Region for S3 storage.                                |
| `S3_CREATE_BUCKET`            | `false`                                 | Whether to create an S3 bucket on startup.            |
| `S3_CREATE_BUCKET_WITH_POLICY`| `false`                                 | Whether to create an S3 bucket with a policy.         |
| `S3_BUCKET_AS_PATH_STYLE`     | `false`                                 | Whether S3 uses path-style access.                    |
| **Grafana**                   |                                         |                                                       |
| `GRAFANA_USER_ID`             | `1000`                                  | UserID for grafana container.                         |
| `GRAFANA_GROUP_ID`            | `1000`                                  | GroupID for grafana container.                        |
| `GRAFANA_USER`                | `admin`                                 | User for grafana login.                               |
| `GRAFANA_PASSWORD`            | Auto generated from cli install         | Password for grafana login.                           |
| `GRAFANA_DATA_PATH`           | `./data/grafana`                        | Path to store grafana data.                           |
| `PROMETHEUS_DATA_PATH`        | `./data/prometheus`                     | Path to store prometheus data.                        |
| `LOKI_DATA_PATH`              | `./data/loki`                           | Path to store loki data.                              |

### How to use your own ssl certificates

If you selected to use your own ssl certificates (self signed also possible) in the stack, you have to place your certificates in the data/traefik/certs directory and  adjust the data/traefik/certs/dynamic_conf.yaml file to reference all your certificates. An example file content could look like this:

```
tls:
  certificates:
    - certFile: /data/traefik/certs/cert1.crt
      keyFile: /data/traefik/certs/cert1.key
    - certFile: /data/traefik/certs/cert2.crt
      keyFile: /data/traefik/certs/cert2.key
```

Note that if you have intermediate certificates you have to create one crt file with first the server certificate followed by any intermediate certificates in the same file.
You can also find more information about the dynamic_conf.yaml on the traefik documentation [here](https://doc.traefik.io/traefik/https/tls/).

### How to send emails for user mentions

If you want the Anchorpoint backend to send emails when a user is invited or mentioned in a comment you have to setup the `EMAIL_` environment variables for smtp. You can also adjust the email templates from config/ap_backend/templates/email directory.

### How to store data on a different path

If you want to change the data paths of Postgres, MinIO, Traefik, or the Anchorpoint Backend Client update folder, you can adjust the according `..._PATH` environment variables before starting the stack.

### How to use other S3 provider instead of MinIO

If you want to use S3 from your cloud provider of choice, do not choose minio in the cli [install command](../self-host-cli#install). Setup a bucket on your cloud provider and make sure that you set the bucket policy to:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObjectVersion",
                "s3:GetObject"
            ],
            "Resource": "{bucket_resource_id}/*/public/*"
        }
    ]
}
```

and that you create a policy for the user of the credentials (S3_ACCESS_KEY / S3_SECRET_KEY) with:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": [
                "{bucket_resource_id}",
                "{bucket_resource_id}/*"
            ]
        }
    ]
}
```

After you created the bucket please set the according `S3_` environment variables in the `.env`. To test if your setup is working correctly you can first check the logs of the ap_backend container if the connection to the bucket could be created after you started the stack. You can then change your avatar in the Anchorpoint desktop client and check if a file is uploaded into the bucket. To test if the credentials and their policies are working correctly, setup a project in the Anchorpoint desktop client and change the project's icon. Check if a file was uploaded to the bucket.

### How to use a custom Postgres server

If you want to use your own postgres database server, do not choose postgres in the cli [install command](../self-host-cli#install). Create two databases `ap` and `keycloak` on your database server. Setup the according `POSTGRES_` environment variables in the `.env` file before starting the stack.

### How to make additional adjustments to the docker compose file

If you want to adjust the generated docker-compose file created by the cli tool, please use overwrites by creating a new yaml file next to the existing docker-compose file. If you directly change the original docker-compose file the cli update command will overwrite your changes. The cli [start command](../self-host-cli#start) will consider all compose files in your installation directory when starting the stack.

### Start the stack

To start the stack you must use the cli tool [start command](../self-host-cli#start). Internally it will use `docker compose up -d` but it will also respect configration overwrites. An overview about the state of the containers will be printed. The `ap_backend` container will only start if the depending containers get into a `healthy` state. After all containers are started you can check if the Anchorpoint web dashboard is reachable under `http{s}://{your_domain}/dashboard`. Be aware that it might take a few minutes for the dashboard to be reachable.

### Troubleshooting start problems

If the containers do not start, or you cannot reach them via your provided domain or IP address, first check the container log outputs. You can use `docker ps -a` to view all running containers. Copy the container id and use `docker logs {container_id}` to print the latest container log outputs.

Also check that your DNS records are setup correctly if you are using a custom domain and that your firewall allows connections on the http / https port, the gprc port, and the MinIO ports.

### Setup your SSO provider in Keycloak (optional)

Checkout our guide for SSO provider in Keycloak [here](sso-setup).

### Setup user accounts

Checkout our guide for managing users in your self hosted environment [here](managing-users). After you set up the user accounts, login via the Anchorpoint desktop client as described [here](user-login).

## How to update the stack

To update the stack, run the cli tool [update command](../self-host-cli#update). It will download the latest version of the stack, including the latest Anchorpoint backend and client versions. The update will overwrite files in the installation directory, but will not change anything in your data directories. You can also use the [check_update command](../self-host-cli#check-update) to check if there is a new version available.

After the update is finsihed you can restart the stack by running the cli [start command](../self-host-cli#start) again. Note that while updating, the Anchorpoint clients will be in the offline mode. We generally recommend updating the stack after work.

## How to update your license

You can update the self-hosting license by using the cli tool [update_license command](../self-host-cli#update-license). The command will stop and remove the ap_backend container, patch the `.env` file `LICENSE_KEY` environment variable and recreate the ap_backend container for you. Note that this will also result in a short downtime while the ap_backend container is not running. Similar to updating, we recommend updating the license when no users are currently using the application.

## How to stop the stack

To stop the stack you can use the cli tool [stop command](../self-host-cli#stop) or use `docker-compose stop` in your installation directory.

## Data that should be backed up regularly

You should regularly backup the data directories in your install directory. Also. do a backup if you changed the data paths from their current location. You can restore the Anchorpoint backend state by restoring the database and MinIO directories.