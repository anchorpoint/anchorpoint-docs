# Frequently asked questions

This section answers frequently asked support questions from emails and the Discord forum. It is also used for troubleshooting. If this page does not help you, feel free to [contact our support](mailto:support@anchorpoint.app). 
## How does Anchorpoint handle files?
Anchorpoint works like a file browser and Git client. We don't upload any production files to our cloud. Read more on [production and metadata](file-organization/production-metadata.md).

## What are projects in Anchorpoint?
Projects in Anchorpoint are similar to projects in any project management software. In Anchorpoint, each project requires a folder for project files. In the case of Git, a project is equivalent to a Git repository.
### How do I move a project?
You simply need to move the entire folder containing the .approj file to another location. Then open the project by double-clicking the .approj file, and Anchorpoint will adjust to the new location. 
### How do I change the project folder?
You may connected Anchorpoint with a wrong project folder or a wrong location for your Git repository and want to fix that. The best idea is to remove your project in "Project Settings", then "Delete Project". Keep in mind that this will disconnect you from all previously created attributes, tasks, and comments. If you are using Git, you will also need to delete the hidden .git folder and the .gitignore and .gitattributes file.

## How to fix login issues?
These are possible ways to deal with these problems. Here are some lessons learned from the past.
### Antivirus software
Anchorpoint is a signed application (on Windows and macOS) that is usually allowed by antivirus software. We have found in the past that applications like Kaspersky often block Anchorpoint from accessing the Internet. Disable Kaspersky software and then try to log in to Anchorpoint.
### Incorrect time in the operating system
Sometimes the login fails if the time in your operating system does not match the global time. Activate the synchronization of the time with the internet.
### Isolated environment
If you are working on a system with restricted Internet access via a firewall or proxy, check our settings for [isolated environments](faq/isolated-environment.md).
### Run the installer again
Sometimes a reinstallation of the application can help when you cannot log in.
### "Syncing Database" does not stop (Windows)
After startup or login you will see the loading screen with "Syncing Database", "Please be patient. In rare cases, this can take longer than usual." If this condition is permanent, you can do the following:
1. Go to: C:\Users\"USERNAME"\AppData\Local\Anchorpoint\app-"LATESTVERSIONNUMBER"\scripts\win
2. Double click on "clear_settings_and_database.bat"
### My web browser tells me that I am logged in but Anchorpoint does not start
Click on the website permissions and remove the entry: **ap:// links**
![Anchorpoint links](https://cdn.loom.com/images/originals/79caadd23f79436ead9ad1d155adbda9.jpg)


### I see "Free Plan" or "Trial" while I am part of a licensed workspace. What should I do?
This problem can occur if you previously logged in with a different account. When Anchorpoint starts, it may use the wrong account to log in. Do the following:
1. Log out of Anchorpoint
2. Clear browser cookies
3. Re-login to Anchorpoint with the correct account

## How to fix issues with Git?
We have created an extra page [Git troubleshooting](https://docs.anchorpoint.app/docs/version-control/troubleshooting/) for this purpose.

## How to troubleshoot Anchorpoint startup problems?

### The application does not launch after an update
In rare cases, the updater may fail to write certain files, rendering the application unusable. The solution is to simply [download](https://www.anchorpoint.app/download) and reinstall Anchorpoint.

### The application shows a white window and does not start (Windows)
Sometimes there are problems with multiple monitors that the application does not start properly. This is how you can fix the error: 
1. Go to: C:\Users\"USERNAME"\AppData\Local\Anchorpoint\app-"LATESTVERSIONNUMBER"\scripts\win
2. Double click on "clear_window_settings.bat"

The next time you can try to launch the application normally. 

## How can I work offline?
Anchorpoint has an offline mode that is activated when there is no Internet connection or when the [Anchorpoint server is unavailable](https://anchorpoint.statusgator.app/). During offline mode, the following features are unavailable:

- Creating new projects
- Inviting members to your workspace and your projects
- File locking
- Changing workspace settings
- Creating an account and logging into the application for the first time
 
The following features are working, but will not sync in real time. Once issues has been resolved, syncing will be activated again.
 
- Attribute changes
- Reviews
- Tasks
 
All other features like Git, file browsing, or actions should work normally. 

If the Anchorpoint server is unavailable, you will not be able to create accounts or log in to the application for the first time. Anchorpoint will disable offline mode as soon as the server can be reached. If you are experiencing problems that did not occur previously, perform a database resync under "Workspace Settings" / "Troubleshooting" -> "Resync Database".

:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: Further topics
   :glob:

   faq/*
:::