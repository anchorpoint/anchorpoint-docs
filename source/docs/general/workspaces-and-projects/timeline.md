# Timeline

Timeline reflects the complete course of the project. It shows comments, [attribute changes](../../asset-management/attributes.md) and @mentions. From the timeline you can go directly to your file and don't have to browse there. 
If you set an attribute on a file or folder, this entry will be reflected in the timeline.

The timeline also shows Git commits if your project was created as a [Git repository](../../version-control/first-steps.md).

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/ece81e97de1d4dc3919ebc5a24777aa7?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

When you work in a Git project, each timeline entry is a version in Git (a commit). If you click on it, you can see the files that were present in the Git commit.

## Attributes (tags, status, assignments)

The timeline can also display attribute changes. For example, you can see who changed the status of an asset. Activate the attribute in the timeline filter.
![Timeline filter](https://cdn.loom.com/images/originals/82232da72a9e4a538954f6f93de5da55.jpg?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9jZG4ubG9vbS5jb20vaW1hZ2VzL29yaWdpbmFscy84MjIzMmRhNzJhOWU0YTUzODk1NGY2ZjkzZGU1ZGE1NS5qcGciLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2ODcyNjEzNTd9fX1dfQ__&Key-Pair-Id=APKAJQIC5BGSW7XXK7FQ&Signature=E5uikhnT5UGs7y8jisma2IZ5qR%7E%7EvAe0aj-ra3pLGS4P3fDLonttujBC-eOSBMHSRIPwE8Xyg3dVddbuPybpH%7EygCVahgUPKDPHA8SY2ToWDJrB7fFDB1rpsaRNYEkRLT0jmacAFVVpVtmNYHoL2oU4mUda4O09ibCgsdUT2fkP1XYlh5lEb7q0hcbF0Tix1JxOCgAriS7rvtYUNRji5eo9pEsK47b%7EUD0DW-VQwPMyCuXYrpODwH1N99z8zjLvA2H%7E8nCOoASZf8ZUIa6uqU2v0Z4aLmOF5Vb9LLJZCjJOBzWPDSNYqr4O4F9hpGAhLnR1kzMixo-CVhn8Wy6TAuA__)

## Search
You can also quickly browse entries in the Timeline. Click on the search field or press CTRL-F to activate the search in the Timeline. 

The Timeline can search for comments, attributes text, members and file names.