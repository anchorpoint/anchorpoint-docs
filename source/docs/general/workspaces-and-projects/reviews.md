# Reviews

> ℹ️ Requires a Team plan

You can leave a comment on any file and get a discussion going. For team members to join the discussion, they must be added to a project.

Use the drawing tools in the preview view to draw on images. Each drawing is saved as a comment. In a video, drawings are assigned to a frame. In the video player you can click on the corresponding position and get to the frame that contains the comment.

<div style="position: relative; padding-bottom: 56.162246489859605%; height: 0;"><iframe src="https://www.loom.com/embed/1d399f6c5e17478f8fdabbc7df47de66?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>