# Assign members to projects

> ℹ️ Requires a Team plan

You can create projects in Anchorpoint and assign participants from your workspace. Within a project you can assign tasks, hold reviews, comment and tag files.

If you work with a project folder that is located on a NAS, Dropbox or similar, you have to make sure that everyone has access to this folder.
If you are working with a Git repository, you need to make sure that each person also has access to the repository in your Git server (GitHub, Azure DevOps, Gitea, etc.).


<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/26c3721813c94aa5bae1e53245ee332a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Project settings
You can give your members permission to edit project settings and manage Attributes. This includes adding and removing new members, as well as changing names or other settings. Click on the name of a member and activate the option "Project Admin".

If a member does not have Project Admin permissions, he or she cannot create/rename and delete new attribute fields or tags. However, they can assign existing fields to tasks, folders, and files.

Workspace Admins automatically have the right to change project settings.

<img class="content-image-middle" src="../../images/projectAdmin.webp">

Matthäus has all admin rights, but Catherine does not in this case. You can change this settings in "Project Settings" / "Members".

## User management in your workspace
If you have an Anchorpoint team plan, you can use it to manage users within your workspace. A workspace then contains the individual projects.
In the "Workspace Settings" you can add or remove new users under "Workspace Members". If you deactivate a user, his seat becomes free and you can assign it to another user.

