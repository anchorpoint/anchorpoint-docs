# Archiving

Projects can be archived so that they no longer appear in the project list. The project is frozen, so to speak, and retains all timeline entries as well as the project members. For those, the project also disappears from the project list.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/27c67fcfd10e412a9ef295f3e6b38c8c?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Archive a project
Projects can be archived under Settings & Members.

## Restore a project
In the Workspace Settings under Projects, you will find the complete overview of all projects (active and archived) in your workspace. There you can reactivate archived projects.




