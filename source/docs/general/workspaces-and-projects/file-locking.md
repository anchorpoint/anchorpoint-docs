# File locking

> ℹ️ Requires a Team plan

Anchorpoint allows to lock files for other team members to block conflicts (overwriting each other). File locking is possible in any project, both in a Git repository and on a Dropbox, Google Drive etc.

If a file is locked it is write protected for others if they also use Anchorpoint. A user thumbnail shows who locked the file.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/382fd09e6b114a1dbe6101292601ea7a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Lock files
Just select one or more files and choose **Lock** in the context menu. Immediately the locks are transferred to all participants that are currently using Anchorpoint.

### Git projects
In a Git project, binary files are automatically locked as soon as they are modified. So you don't have to lock them manually. The locks are removed as soon as the files are pushed to the remote server. If you want to apply this to text files as well, you need to adjust the auto locking in "Project Settings"/"Git"

<img class="content-image-middle" src="../../images/lockingText.webp">

In this case .unity and .cs files would be also locked automatically.

## Unlock files
If you have created the lock yourself, you can right-click and **Unlock** to unlock it for others.

You can also unlock the files of other users, for example if they have gone on vacation and forgot to unlock their files. In this case, after the unlock in Timeline entry is created to document who has unlocked the files. This way it is easier to find out who has to talk to whom in case of file conflicts.

## Write protection
If files are read-only, they cannot be easily overwritten by others. Some applications (Unreal Engine, Visual Studio Code) allow a force override, which can write files despite lock. But they show a corresponding warning. 

### File locking for Unity projects
Unity does not respect the read-only property by default, but there is a fix available. Read more about [Git with Unity](../../version-control/first-steps/unity.md)

Write protection is not possible on a NAS (mounted drive) and on the cloud drive.