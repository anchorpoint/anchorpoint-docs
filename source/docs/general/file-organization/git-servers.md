# Git servers

> ℹ️ Requires a Pro or Team plan

Anchorpoint is able to connect to any Git server, whether it's GitHub, GitLab, Bitbucket, Azure Devops or a self-hosted Gitea server. You don't need to configure things like LFS, because Anchorpoint does it for you.

The only thing you need to take care of is to make sure that your team members also have an account on that Git server. You also need to invite them to your workspace.

## GitHub
GitHub is by far the most popular Git cloud provider. It's a good start for smaller Unity and WebGL projects.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/-8RV6OiYTFw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## Azure DevOps
Microsoft Azure DevOps is a Git server and a viable alternative to GitHub, GitLab, and Bitbucket. It provides [unlimited storage](https://devblogs.microsoft.com/devops/announcing-git-lfs-on-all-vso-git-repos/) for LFS, which is a blessing for game development with Unity and Unreal. The free plan allows up to 5 users, which is great to get started. To get the most out of Azure DevOps, use our [Azure DevOps integration](../../general/integrations/azure-devops.md)

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/migQ_qqR59M?si=ne2rPOtCR8t244Y1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## Git LFS
Git LFS is the acronym for Git's large file storage. It's an addition to the Git system to manage heavy binary files, like PSD, Uasset, blend, EXR, MP4 etc.

Git is designed for software developers who deal primarily with text files. They are pretty light. When connecting to a Git server, the whole version history is downloaded to your computer, because Git is a distributed version control system and allows you to work offline. This is okay for text files, but not for heavy binary files. Git LFS allows you to control that better, so only the binary files that you really need are being downloaded.

Don't upload binary files without using Git LFS, because you will reach the limits of the Git hosting service. Anchorpoint takes care of that.