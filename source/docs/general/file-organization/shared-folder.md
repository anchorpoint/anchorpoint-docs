# Shared folder

Anchorpoint behaves like an ordinary file browser. You can see it as Windows Explorer on steroids, because it allows collaboration and additional management features. Therefore it works perfectly with Dropbox, Google Drive, One Drive, Nextcloud or your local NAS in the studio. 

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/0e0726ca77114b92a5bdec76cdaa723f?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Virtual files
Tools like Dropbox or Google Drive allow you to keep files online only. This means that you can see something on your computer, but the file is not physically there. When you open it, it has to be downloaded first. These are called virtual files. Anchorpoint will show them with a cloud icon. To view thumbnails, click on the cloud icon in the top right corner. It will trigger the download of the virtual file.