# Your files in Anchorpoint

This section explains how Anchorpoint handles production and metadata so you don't have to worry about your files. 

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/9a0fabf305a0485cb656c85648e01a92?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## What happens to my project files?
Your production data, e.g. your Unreal and Unity project, code, videos, images, 3D models, Blender or Photoshop files etc. are never modified by Anchorpoint. This data is then mostly on your Git repository or a shared folder such as on your hard drive, Dropbox, Google Drive or NAS. Anchorpoint adds metadata such as file locks, tags and annotations on top of that. 

## Where does Anchorpoint store its metadata?
Anchorpoint processes metadata (tags, file locks, attributes or shared Git config values) in the Anchorpoint cloud. This allows for team collaboration or synchronization across multiple computers. Anchorpoint stores a .approj file in your project so it knows where your project files (described above) are located. Anchorpoint never uploads any production data to the Anchorpoint cloud.

## What is the .approj file?
You can think of the .approj file as the location for all of Anchorpoint's metadata. So this file should always be in the main folder of your project. Also when you archive your project (e.g. zip it) you should include this .approj file. This way Anchorpoint can always restore your metadata in the future and you will have all the info about how your project went besides your project files.

Technically, the .approj file does not store any real data. It always retrieves them from the Anchorpoint cloud. There they are stored indefinitely.

## Working offline
Anchorpoint can work offline, with the limitation that the metadata services such as file locking, will not work.  While offline, you can still:
- Commit changes to Git
- Browse your project files
- Run Python scripts (as long as they don't require metadata services)
- Edit Attributes such as tags or descriptions, but they won't be synchronized