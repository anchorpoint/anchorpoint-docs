# Workspaces and projects

Each project belongs to a workspace. A subscription is bound to a workspace. The number of subscription seats determines how many team members can be in a workspace. This also means how many team members are allowed to work on a project that belongs to this workspace.

When you start Anchorpoint for the first time, a workspace is automatically created for you. Every user has his own workspace. 
If you want to collaborate with others you can:
- Invite people to your workspace
- Join another workspace you have been invited to.

## Manage projects and assign members

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/214bb4bf79b448b49122d1cbfdab52dc?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Invite members to your workspace

![Inviting to your workspace](https://cdn.loom.com/images/originals/9567762e8ee1414f97d0d6c71c8b3864.jpg?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9jZG4ubG9vbS5jb20vaW1hZ2VzL29yaWdpbmFscy85NTY3NzYyZThlZTE0MTRmOTdkMGQ2YzcxYzhiMzg2NC5qcGciLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2ODk5NDkwODd9fX1dfQ__&Key-Pair-Id=APKAJQIC5BGSW7XXK7FQ&Signature=nowX%7ENCyH9HbAIEjLPnqB4GfeckTIHWb-2DmDHW5j%7EwNVhL1QNuRcV18CCW-b9QLjQ1WdQoLasSvLpeM4hj%7E7jqVGA6f-JPOpvD9cTCFi7sY97oAJA2tN-DWp8FFFkvBeu23XGfaGUDvgmDqrF3lDRlli9FAuxzcHGN6TCw-nKccxlmIRZbfPJKNMa9GhjKZ6eBWZyIAuwXjYsx5XwfUXrqKN-Jw1iLWt4zQt5BIWgLc3xIgfdYMhbxaT-7206YaFo7O3qvM6AqJ3XBb3NTuNot02BbVoBBeOlYmMdh6usSCI1-LIJUjl97iBpmo5rjHCKAEfkE5fDlY6ws%7EwrZIfQ__)

The first step is to invite member to your workspace. This is the same procedure as when you invite members to a Microsoft Organization, a Slack workspace or a Discord server. Just click on Invite Member and add them via email address. Your members will then receive an email that they have been invited to a workspace.

## Roles
Participants in a workspace have certain access permissions. There are owners, admins and members.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe src="https://www.loom.com/embed/a7f3cdc346c14dac8f01b0fdc099cee9?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

### Member
This is the default role. A member can:
- Work on projects and edit metadata (tags, comments, versions and reviews).
- Create a project

A member cannot see all participants of a workspace. 

Members can also optionally get the right to change the project settings. This allows them to assign other members from the workspace to a project.

### Admin
An admin can do everything a member can do, plus
- Add new members to the workspace (as long as the subscription allows it)
- Remove members from the workspace
- Add and remove members from projects
- Edit workspace settings (workspace name, actions, projects)

### Owner 
The role Owner is assigned only once in the workspace. An owner has the same rights as an admin, but can also edit the subscirption settings. This includes:

- Add or remove seats
- End the subscription

The email address of the owner is also the one that is bound to the subscription.


## Open projects from Windows Explorer or macOS Finder
When Anchorpoint creates a project and connects to a folder on your hard drive, it creates an .approj file, which is also visible in Windows Explorer. Anyone who has access to that project, can open it from Windows Explorer by simply clicking on the .approj file. This is also useful when you want to move the project folder to another place. As long as the .approj file is included, Anchorpoint can always connect to the project files.

<img class="content-image-middle" src="https://cdn.loom.com/images/originals/ee94ab9e902748c2abaedbf7511309d9.jpg?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9jZG4ubG9vbS5jb20vaW1hZ2VzL29yaWdpbmFscy9lZTk0YWI5ZTkwMjc0OGMyYWJhZWRiZjc1MTEzMDlkOS5qcGciLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2NjgwMDYxNDF9fX1dfQ__&Key-Pair-Id=APKAJQIC5BGSW7XXK7FQ&Signature=b-U1Up0uM0-EzVfxXondi4m6-B3Ldt0pK6xJTDCN9PucM4m7W19L6djDQ7nhjsLQ7mrzGWqSYPzq4mBVAtqSUKgiZO4K0Kh7BK8QDuee0jqYEuMD13eco9Mx5uNxPMBAVzHNmu-vGnZApyE515iFyFUi6M3X3-eXzsQpupJI4AW6Djb6UEk3HscIrGB4-MlN2r-e7fwjCT-2b1A4W%7E34wH3%7EOx%7Em%7EH8USP8gBVSioYG-BtN3kehf4vebCFXGvNugUw-y9r6mN0PJtJVpKKYN1OnXqDpkHyg3XupBusHNglkDuNyA-qfVXNzIbnInz6FAOBGvQEVGt3vTXFh5svEQaw__">
You can open the Anchorpoint project by double clicking on the .approj file


:::{eval-rst}
.. toctree::
   :maxdepth: 1
   :caption: General
   :glob:

   workspaces-and-projects/assign-members-to-projects
   workspaces-and-projects/reviews
   workspaces-and-projects/file-locking
   workspaces-and-projects/timeline
   workspaces-and-projects/archiving
:::