# Unity Engine

This plugin is currently in beta. Feel free to talk to our developers on our [Discord server](https://discord.com/invite/ZPyPzvx).

The Unity plugin allows you to see changed and locked files in the Unity project browser as well as to commit your files from the Unity Editor. If you work in a team it's highly recommended to use the plugin, because it also takes care that Unity handles file locking correctly. Without the plugin, Unity is ignoring file locking and will overwrite locked files, which can lead to merge conflicts. In short, the plugin allows you to:

- Prevent merge conflicts. Unity will respect locked files and won't save them over.
- See the status of a changed an locked file in the Unity Project Browser
- See all changed files in a dedicated Window
- Allow you to commit and revert changes
- Open the Anchorpoint browser from anywhere in the Unity Project Browser by right-clicking on an asset

Unity can communicate with the Anchorpoint desktop application via an IPC connection. When you open the plugin window for the first time, it will ask you to connect to Anchorpoint.

During the commit process, you will need to wait for Anchorpoint to commit your files and check for updates in the Git repository. Once Anchorpoint starts committing your files in the background, you can continue working on your project.
## Plugin performance
It is recommended to keep the Anchorpoint project open (it can be minimized) when working in Unreal Engine. This way changes will be noticed more quickly. If you switch to another Anchorpoint project or have all projects closed (e.g. by being on the project overview page), change detection will be much slower. You can also manually click on the refresh button to update the changes view.

### Beta status of the plugin
As of today, the plugin is in beta and must be manually downloaded and installed as a Unity package. We are aiming for a release version to be distributed via the Unity Asset Store in April 2025.

## Installing the plugin
[Download](https://www.dropbox.com/s/k25xktbze5v8wiq/ap-unity.zip?e=1&dl=1) the Unity package, unzip it and import it via the package manager.

Open the Unity Package Manager via Windows/Package Manager. Click on the plus sign in the top left corner and select "Install package from disc...".

After that, you can click on Window/ Anchorpoint to open up the Anchorpoint plugin UI in Unity.

## Contribution
If you would like to contribute to the development of our plugin, feel free to fork the repository and create pull requests.