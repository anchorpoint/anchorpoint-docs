# GitLab

> ℹ️ Requires a Team plan

GitLab is complete development plattform which is famous for their CI/CD features. The integrations allows you to create repositories and add members to a GitLab group, directly from Anchorpoint. Each member needs a GitLab account.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/ee773429cd764b769c4af87d63c2f5f3?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

## Groups in GitLab
Adding members requires a group on GitLab. GitLab groups are similar to Anchorpoint workspaces. To make sure the integration works properly, have the same users with the same email address in a GitLab group, that you have in an Anchorpoint workspace. Make also sure that GitLab usernames are the same (without @something.com) like the member's email address. 

## Usage
If the integration is active, you can create Git repositories directly from Anchorpoint. To do this, create a project in Anchorpoint and select the **Git Repository** option. Under Remote Settings, then select **New GitLab Repository**. In the next step, add the members. These members will also be added to the repository on GitLab. All members need an active GitLab account first. 

## Project deletion
If you remove a project on Anchorpoint, it is not automatically removed from GitLab. You have to remove it manually there.
