# Gitea (self-hosted)

> ℹ️ Requires a Team plan

Gitea is a lightweight, self-hosted Git server that you can use on your local network (e.g. on a QNAP NAS) or host in the cloud on an AWS, Digital Ocean, or Linode instance. Learn how to setup your [own Gitea server](https://www.anchorpoint.app/blog/install-and-configure-gitea-for-lfs).

Anchorpoint's integration with Gitea allows you to create repositories and add members directly from Anchorpoint, so you don't need to use the Gitea interface. 

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/a36cc2b88b314b5bb767131c29a2dcdd?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

## Requirements
All your members must be added manually on Gitea. This only needs to be done once, after you have set up the Gitea server. Your members should login once to change their password.

Gitea uses usernames instead of email addresses to identify its users. When adding members to Gitea, make sure their username is similar to their email. For example, "catherine.brown@gmail.com" should have the username "cathereine.brown". To match Anchorpoint users to Gitea users, Anchorpoint only looks at the email that comes before the @.

