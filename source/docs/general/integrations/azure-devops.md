# Azure DevOps

> ℹ️ Requires a Team plan

Microsoft [Azure DevOps](https://azure.microsoft.com/en-us/products/devops/) is an alternative to GitHub, allowing you to store files in a Git repository in the cloud. This integration allows you to create new repositories and add members directly from Anchorpoint, so you don't need to use the Azure web interface.

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/8947831032004017994f305c9d44ee0a?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

## Requirements

- Every members needs to have an Azure DevOps account
- All members need to be in the same Azure DevOps organization
- If you have more than 5 members, make sure you subscribed to enough licenses on Azure DevOps

## Configuration

1. Open the **Workspace Settings** / **Integrations** and search for the Azure DevOps integration. Then click on **Connect**. Anchorpoint will open the web browser so you can log into Azure DevOps.

2. Follow the instructions in Anchorpoint.

If something goes wrong you can take a look at the [troubleshooting page](../../version-control/troubleshooting/azure-devops.md)

## Usage

If the integration is active, you can create Git repositories directly from Anchorpoint. To do this, create a project in Anchorpoint and select the "Git Repository" option. Under Remote Settings, then select "New Azure DevOps Repository".
In the next step, add the members. These members will also be added to the project on Azure DevOps.

Your members will receive two emails. One from Azure DevOps and one from Anchorpoint. First, they need to open the Azure DevOps email, accept the invite and create an account on DevOps. Then they need to open Anchorpoint to access the project.

### Project deletion

If you remove a project on Anchorpoint, it is not automatically removed from Azure DevOps. You have to remove it manually there.