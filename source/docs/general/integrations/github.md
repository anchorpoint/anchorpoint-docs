# GitHub

> ℹ️ Requires a Team plan

Use GitHub, the most popular Git hosting platform, to store your project files in the cloud. The integration allows you to create repositories from Anchorpoint. 

<div class="video_wrapper"><iframe src="https://www.loom.com/embed/2c8d1f568fa146539cc35cd540610714?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" width="100%" height="100%" frameBorder="0" allowFullScreen
  webkitallowfullscreen="true"
  mozallowfullscreen="true"></iframe></div>

## Limitations when adding members
GitLab uses usernames instead of email addresses to identify their users. This makes it hard for Anchorpoint to match. If a username is totally different from the email address, Anchorpoint will not be able to add a user to a GitHub repository. So you have to add members manually on GitHub after you added them in Anchorpoint.
However, if you members are creating a new GitHub account, make sure that the username is exactly the same as their email (without the @something.com). In this case, Anchorpoint will be able to match GitHub username and Anchorpoint email.

## Create an organization on GitHub
GitHub allows you to create repositories right after you create an account. To use the Anchorpoint integration, you must first create an organization on GitHub. This organization will correspond to an Anchorpoint workspace. All repositories created in Anchorpoint will be part of this GitHub organization.

## Usage
If the integration is active, you can create Git repositories directly from Anchorpoint. To do this, create a project in Anchorpoint and select the **Git Repository** option. Under Remote Settings, then select **New GitHub Repository**. In the next step, add the members. Anchorpoint tries to add them on GitHub if their username is similar to their email address. If Anchorpoint fails to do so, it will show an information, so that you can add them manually.

## Project deletion
If you remove a project on Anchorpoint, it is not automatically removed from GitHub. You have to remove it manually there.