# GitLab (self-hosted)

> ℹ️ Requires a Team plan

The self-hosted version of GitLab offers the same features as the cloud version of GitLab, but with more control. This is particularly useful for configuring Git LFS. 

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/ef2323b70d2f43b1a609ced60297645b?hide_owner=true&hide_share=true&hide_title=true&hideEmbedTopBar=true" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>

## Requirements
For a self-hosted GitLab instance, users are identified by their email addresses during the invitation process. When adding members to GitLab, ensure that their email address is valid and corresponds to the same email address used in Anchorpoint. For instance, if a user's email is "catherine.brown@gmail.com", this same email should be used in both GitLab and Anchorpoint. If you wish to utilize email invitations for your self-hosted GitLab instance, you must correctly configure the [GitLab SMTP settings](https://docs.gitlab.com/omnibus/settings/smtp.html).

In instances where public user signup is disabled, it's important to note that users invited via email must first be created by a GitLab administrator. For more information on this, refer to the GitLab documentation on [user creation](https://docs.gitlab.com/ee/user/profile/account/create_accounts.html).
