# Unreal Engine

This plugin is currently in beta. Feel free to talk to our developers on our [Discord server](https://discord.com/invite/ZPyPzvx).

Anchorpoint provides a plugin for Unreal Engine that allows revision control to be handled directly from the Unreal editor. If you're working in a team, it's highly recommended that you use the plugin, as it gives you more control over your modified files.
Unreal Engine can communicate with the Anchorpoint desktop application via an IPC connection. This allows you to

- Pull, revert and reset project files in Anchorpoint without closing Unreal Engine.
- Check for unsaved files before committing changes
- Open the Anchorpoint Browser from any asset in the Unreal Editor
- Instantly check for locked files

In addition, the Anchorpoint revision control plugin for Unreal Engine allows you to

- Run checkout workflows and lock files exclusively
- See who locked what file in the Unreal Editor
- Run blueprint diffs to see what you have changed
- View individual file history, resolve merge conflicts and revert individual files
- Commit your changes from the Unreal Editor

During the commit process, you will need to wait for Anchorpoint to commit your files and check for updates in the Git repository. Once Anchorpoint starts committing your files in the background, you can continue working on your project.

## Plugin performance
It is recommended to keep the Anchorpoint project open (it can be minimized) when working in Unreal Engine. This way changes will be noticed more quickly. If you switch to another Anchorpoint project or have all projects closed (e.g. by being on the project overview page), change detection will be much slower.

## Installing the plugin
Download the plugin and place it to your project/plugins directory. 

Download Links v0.28
- [Windows](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/internal/unreal/AP_Unreal_plugin_v0_28_Windows.zip)
- [macOS](https://s3.eu-central-1.amazonaws.com/releases.anchorpoint.app/internal/unreal/AP_Unreal_plugin_v0_28_Mac.zip)

Make sure that the plugin is not part of the .gitignore, so that it is submitted to the main repository. Make also sure, to whitelist the the Anchorpoint Unreal plugin, such as:
`!**/Plugins/ap-unreal/**
If you see warnings, that your Unreal project requires the Anchorpoint plugin, that usually means that it's not in your Plugins directory of your project. Your team members can then download and install the plugin manually.