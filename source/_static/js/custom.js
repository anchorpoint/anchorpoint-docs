function scrollIntoViewIfNeededCustom(target) {
    if (target.getBoundingClientRect().bottom > window.innerHeight) {
        target.scrollIntoView(false);
    }

    if (target.getBoundingClientRect().top < 0) {
        target.scrollIntoView();
    }
}

window.addEventListener("load", function () {
    var elems = document.getElementsByClassName('current-page');
    var elem = elems[elems.length - 1];
    scrollIntoViewIfNeededCustom(elem);
});

//matomo
var _paq = window._paq = window._paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
_paq.push(["setCookieDomain", "*.www.anchorpoint.app"]);
_paq.push(["setDomains", ["*.www.anchorpoint.app", "*.docs.anchorpoint.app"]]);
_paq.push(["setDoNotTrack", true]);
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function () {
    var u = "https://track.anchorpoint.app/";
    _paq.push(['setTrackerUrl', u + 'matomo.php']);
    _paq.push(['setSiteId', '2']);
    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
    g.async = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
})();


// Ahrefs Analytics
(function() {
    var ahrefsScript = document.createElement('script');
    ahrefsScript.src = "https://analytics.ahrefs.com/analytics.js";
    ahrefsScript.setAttribute('data-key', 'vk3uPNoAusT+aiX2LLxIjA');
    ahrefsScript.async = true;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ahrefsScript, s);
})();
