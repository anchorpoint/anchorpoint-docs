# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

current_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0, os.path.join(current_dir, "../python"))

# -- Project information -----------------------------------------------------

project = 'Anchorpoint'
copyright = '2025, Anchorpoint Software GmbH'
author = 'Anchorpoint Software GmbH'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.duration',
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.autosummary",
    "sphinxext.opengraph",
    'sphinx_reredirects',
    'sphinx_copybutton',
    'myst_parser'
]

#Redirects
redirects = {
     "Overview/Intro/index": "../../docs/1-Overview/1-Intro/",
     "Overview/Changelog/index": "../../docs/1-Overview/2-Changelog/",
     "Actions/Reference/index": "../../../docs/5-Actions/4-YAML/",

     "docs/1-overview/1-Production-and-metadata/" : "../../../../docs/general/file-organization/production-metadata",
     "docs/2-manage-files/3-Git-servers/" : "../../../../docs/general/file-organization/git-servers",
     "docs/2-manage-files/1-Use-your-own-storage/" : "../../../../docs/general/file-organization/shared-folder",
     "docs/1-overview/2-FAQs/" : "../../../docs/general/faq/",
     "docs/1-overview/3-Integrations/" : "../../../docs/general/Integrations/",
     "docs/1-overview/integrations/azure-devops/" : "../../../../docs/general/integrations/azure-devops/",
     "docs/1-overview/integrations/gitea/" : "../../../../docs/general/integrations/gitea/",
     "docs/1-overview/integrations/github/" : "../../../../docs/general/integrations/github/",
     "docs/1-overview/integrations/gitlab/" : "../../../../docs/general/integrations/gitlab/",
     "docs/1-overview/integrations/gitlab_self-hosted/" : "../../../../docs/general/integrations/gitlab-self-hosted/",
     "docs/3-work-in-a-team/1-Workspaces-&-Projects/" : "../../../docs/general/workspaces-and-projects",
     "docs/3-work-in-a-team/projects/1-Assign-members-to-projects/" : "../../../../docs/general/workspaces-and-projects/assign-members-to-projects/",
     "docs/3-work-in-a-team/projects/5-File-locking/" : "../../../../docs/general/workspaces-and-projects/file-locking/",
     "docs/3-work-in-a-team/projects/3-Archives/" : "../../../../docs/general/workspaces-and-projects/archiving/",
     "docs/3-work-in-a-team/2-Timeline/" : "../../../../docs/general/workspaces-and-projects/timeline/",
     "docs/3-work-in-a-team/5-Reviews/" : "../../../../docs/general/workspaces-and-projects/reviews/",

     "docs/3-work-in-a-team/3-Version-Control using Git/" : "../../../docs/version-control/first-steps",
     "docs/3-work-in-a-team/git/1-Git-existing-projects/" : "../../../../docs/version-control/first-steps/existing-projects",
     "docs/3-work-in-a-team/git/1-Git-with-Unity/" : "../../../../docs/version-control/first-steps/unity",
     "docs/3-work-in-a-team/git/1-Git-with-Unreal-Engine/" : "../../../../docs/version-control/first-steps/unreal",
     "docs/3-work-in-a-team/git/1-Git-basics/" : "../../../../docs/version-control/features/",
     "docs/3-work-in-a-team/git/5-Git-troubleshooting/" : "../../../../docs/version-control/troubleshooting",
     "docs/3-work-in-a-team/git/2-Git-commands/" : "../../../../docs/version-control/features/commands",
     "docs/3-work-in-a-team/git/2-Git-settings/" : "../../../../docs/version-control/features/settings",
     "docs/3-work-in-a-team/git/3-Branching/" : "../../../../docs/version-control/features/branching",
     "docs/3-work-in-a-team/git/3-Selective-download/" : "../../../../docs/version-control/features/selective-checkout",
     "docs/3-work-in-a-team/git/4-Resolving-conflicts/" : "../../../../docs/version-control/features/resolving-conflicts",

     "docs/3-work-in-a-team/projects/4-Asset-libraries/" : "../../../../docs/asset-management/attributes/asset-libraries",
     "docs/4-assets-and-tasks/3-Tasks/" : "../../../../docs/asset-management/attributes/tasks",
     "docs/3-work-in-a-team/projects/2-Share-file-locations/" : "../../../../docs/asset-management/file-browser/app-links",
     "docs/3-work-in-a-team/projects/1-Color-management/" : "../../../../docs/asset-management/file-browser/color-management",
     "docs/3-work-in-a-team/4-Version-Control using inc save/" : "../../../../docs/asset-management/file-browser/stacking",
     "docs/4-assets-and-tasks/2-Attributes/" : "../../../docs/asset-management/attributes",
     "docs/2-manage-files/4-File-browsing/" : "../../../docs/asset-management/file-browser",
     "docs/4-assets-and-tasks/1-Templates/" : "../../../docs/asset-management/templates",

     "docs/2-manage-files/2-Cloud-NAS/" : "../../../../docs/asset-management/utilities/cloud-drive",
     "docs/5-utilities/2-Drive-mapping/" : "../../../../docs/asset-management/utilities/drive-mapping",
     "docs/5-utilities/1-video-conversion/" : "../../../../docs/asset-management/utilities/video-conversion",

     "docs/6-actions/1-The-Action-System/" : "../../../docs/actions/create-actions",
     "docs/6-actions/2-Create-Actions/" : "../../../docs/actions/create-actions",

     "docs/asset-management/file-browser/" : "../../../../docs/asset-management/basics",
     "docs/asset-management/file-browser/app-links/" : "../../../../docs/asset-management/basics/app-links",
     "docs/asset-management/file-browser/color-management/" : "../../../../docs/asset-management/basics/color-management",
     "docs/asset-management/file-browser/stacking/" : "../../../../docs/asset-management/basics/stacking",



}

myst_enable_extensions = [
    "colon_fence", 
    "html_image"
]

autodoc_member_order = 'bysource'

source_suffix = ['.rst', '.md']

suppress_warnings = ["myst.header"]

master_doc = "index"

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

html_title = 'Documentation'

html_favicon = 'favicon.svg'

html_add_permalinks = '#'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_css_files = [
    'css/custom.css',
]

html_js_files = [
    'js/custom.js',
]
html_show_sphinx = False
html_show_sourcelink = False

# list of options: https://pradyunsg.me/furo/customisation/colors/
# https://github.com/pradyunsg/furo/tree/main/src/furo/assets/styles/variables
html_theme_options = {
    "light_css_variables": {
        "color-brand-primary": "#0c81df",
        "color-brand-content": "#0c81df",
    },
    "dark_css_variables": {
        "color-foreground-primary": "#ffffffff",
        "color-foreground-secondary": "#bdbdbd",
        "color-brand-primary": "#90caf9",
        "color-brand-content": "#eee",
        "color-background-primary": "#1D1E20;",
        "color-sidebar-background": "#1D1E20;",
        "color-sidebar-search-background": "#1D1E20;",
        "color-sidebar-item-background--hover":"#323439",
        "color-sidebar-item-expander-background--hover":"#323439",
        "color-sidebar-link-text--top-level":"#eeeeee",
        "color-api-background":"#323439",
        "color-sidebar-item-background--current":"#323439",
        "sidebar-item-spacing-horizontal": "1.2rem",
        "sidebar-search-input-spacing-horizontal":"0rem"
    },
    "light_logo": "logo/logo.png",
    "dark_logo": "logo/logo_dark.png",
    "sidebar_hide_name": True
}

pygments_dark_style = "material"

#open graph setup
ogp_site_url = "https://docs.anchorpoint.app"
ogp_use_first_image = True
ogp_image = "http://docs.anchorpoint.app/_static/logo/logo_dark.png"
ogp_description_length = 200
