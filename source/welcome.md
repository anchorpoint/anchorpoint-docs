# Getting Started

Anchorpoint is the version control and asset management application for your creative team. You can see it to a Git client and file browser on steroids.

Anchorpoint is actively developed. Every month we release an [update](https://www.anchorpoint.app/releases) which includes updated features, UX enhancements and bug fixes.

## Most popular

<div class="grid-container">
  <div class="grid-item"><a href="https://docs.anchorpoint.app/docs/general/workspaces-and-projects"><img src="https://docs.anchorpoint.app/_static/images/workspaces.svg"></a></div>
  <div class="grid-item"><a href="https://docs.anchorpoint.app/docs/general/file-organization"><img src="https://docs.anchorpoint.app/_static/images/files.svg"></a></div>
  <div class="grid-item"><a href="https://docs.anchorpoint.app/docs/version-control/first-steps/unreal"><img src="https://docs.anchorpoint.app/_static/images/unreal.svg"></a></div>
  <div class="grid-item"><a href="https://docs.anchorpoint.app/docs/version-control/first-steps/unity"><img src="https://docs.anchorpoint.app/_static/images/unity.svg"></a></div>  
  <div class="grid-item"><a href="https://docs.anchorpoint.app/docs/general/workspaces-and-projects/reviews"><img src="https://docs.anchorpoint.app/_static/images/reviews.svg"></a></div>
  <div class="grid-item"><a href="https://docs.anchorpoint.app/docs/asset-management/templates"><img src="https://docs.anchorpoint.app/_static/images/folders.svg"></a></div>
</div>

## Further links and contact options

- [Discord server](https://discord.com/invite/ZPyPzvx) for talking to the developers
- [YouTube](https://www.youtube.com/channel/UCUizw9l4DjYOm-s9-ZD5ipw) channel for tutorials
- [Twitter](https://twitter.com/anchorpoint_hq) for development updates
- [LinkedIn](https://www.linkedin.com/company/anchorpoint-software) for company updates
- [GitHub](https://github.com/Anchorpoint-Software) for Python API examples

- [Support email](mailto:support@anchorpoint.app) for product related questions
- [Contact email](mailto:contact@anchorpoint.app) for questions regarding pricing
