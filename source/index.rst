.. include:: welcome.md
   :parser: myst_parser.sphinx_

.. toctree::
   :hidden:
   :caption: General
   :glob:

   welcome.md
   docs/general/file-organization
   docs/general/workspaces-and-projects
   docs/general/Integrations
   docs/general/faq

.. toctree::
   :hidden:
   :caption: Version Control
   :glob:

   docs/version-control/first-steps
   docs/version-control/features
   docs/version-control/servers
   docs/version-control/troubleshooting

.. toctree::
   :hidden:
   :caption: Asset Management
   :glob:

   docs/asset-management/basics
   docs/asset-management/templates
   docs/asset-management/attributes
   docs/asset-management/utilities

.. toctree::
   :hidden:
   :caption: For developers
   :glob:

   docs/actions/create-actions
   docs/actions/yaml
   docs/actions/python
   docs/actions/cli