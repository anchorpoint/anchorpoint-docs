# Anchorpoint Documentation

Currently you can find the content for the anchorpoint desktop client in this repository in the source/docs folder.

## Requirements
- Python 3.9 (at least 3.7)

## Sphinx
The site is generated with sphinx. You can run it with execution of generate_documentation.bat on windows or generate_documentation.sh on linux based systems.

## Gitlab Pages
The site is hosted via Gitlab Pages and can be reached via docs.anchorpoint.app. The `.gitlbal-ci.yml` contains the ci description. Every push on master triggers a ci build and publishes the new version.

## How to open Documentation after Build
Option 1: Open the index.html in build\dirhtml\ (node that each subfolder has an index.html that needs to be clicked after clicking a link to a subpage)
Option 2: run command: "cd build\dirhtml && python -m http.server 7800" in repo root. The documentation is reachable via http://localhost:7800