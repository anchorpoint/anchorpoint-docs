from __future__ import annotations
import apsync
import anchorpoint
import typing
import apsync

__all__ = [
    "APConfig",
    "Action",
    "ActionButtonType",
    "AnchorpointSettings",
    "ApIntegration",
    "AttributeChange",
    "BrowseType",
    "ChangeSource",
    "ConflictDetails",
    "Context",
    "Dialog",
    "DialogEntry",
    "DropdownEntry",
    "IntegrationAction",
    "IntegrationList",
    "LockDisabler",
    "Progress",
    "ProjectType",
    "ProjectTypeList",
    "RemoteFolderEntry",
    "SettingsList",
    "TimelineChannelAction",
    "TimelineChannelActionList",
    "TimelineChannelEntry",
    "TimelineChannelEntryDetails",
    "TimelineChannelEntryVCDetails",
    "TimelineChannelInfo",
    "TimelineChannelVCInfo",
    "Type",
    "UI",
    "VCBranch",
    "VCBranchList",
    "VCChange",
    "VCChangeList",
    "VCConflictHandling",
    "VCFileStatus",
    "VCGetChangesInfo",
    "VCPendingChange",
    "VCPendingChangeList",
    "VCPendingChangesInfo",
    "check_application",
    "close_create_project_dialog",
    "close_timeline_sidebar",
    "copy_files_to_clipboard",
    "create_action",
    "create_app_link",
    "delete_timeline_channel_entries",
    "enable_timeline_channel_action",
    "evaluate_locks",
    "get_api",
    "get_application_dir",
    "get_config",
    "get_context",
    "get_locks",
    "get_timeline_update_count",
    "join_project_path",
    "lock",
    "log_error",
    "open_integration_preferences",
    "open_timeline",
    "refresh_timeline_channel",
    "reload_create_project_dialog",
    "reload_timeline_entries",
    "set_timeline_update_count",
    "show_create_project_dialog",
    "stop_timeline_channel_action_processing",
    "temp_dir",
    "timeline_channel_action_processing",
    "unlock",
    "update_locks",
    "update_timeline_channel_entries",
    "update_timeline_last_seen",
    "vc_load_pending_changes",
    "vc_resolve_conflicts"
]


class APConfig():
    """
    The anchorpoint config provides some default configurations from Anchorpoint to python.
    """
    gcs_client_id = '394852126893-tole6s0o3rlhjcqn90um5ht676hfmppi.apps.googleusercontent.com'
    gcs_key = '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'
    github_client_id = '9b822117f5fb1a1a2826'
    github_client_key = 'e6fe02939081592ae70fe8766d7c9ac75ea40ddc'
    gitlab_client_id = '4febc909c3ea82f0a5d832c8053f4f93fb0170484a78dfe18695c888f5fb655f'
    gitlab_client_key = 'gloas-0a0a02dacb1a7ca8a6c3ab016d747d333d7f670731fad07a259d345c67601168'
    pass
class Action():
    def __init__(self) -> None: ...
    @property
    def author(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @author.setter
    def author(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def category(self) -> str:
        """
        :type: str
        """
    @category.setter
    def category(self, arg1: str) -> None:
        pass
    @property
    def description(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @description.setter
    def description(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def file_registration(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @file_registration.setter
    def file_registration(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def folder_registration(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @folder_registration.setter
    def folder_registration(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def icon(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def id(self) -> str:
        """
        :type: str
        """
    @id.setter
    def id(self, arg1: str) -> None:
        pass
    @property
    def is_python(self) -> bool:
        """
        :type: bool
        """
    @is_python.setter
    def is_python(self, arg1: bool) -> None:
        pass
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        pass
    @property
    def script(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @script.setter
    def script(self, arg1: typing.Optional[str]) -> None:
        pass
    pass
class ActionButtonType():
    """
    Members:

      Primary

      Secondary

      SecondaryText

      Danger
    """
    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    Danger: anchorpoint.ActionButtonType # value = <ActionButtonType.Danger: 3>
    Primary: anchorpoint.ActionButtonType # value = <ActionButtonType.Primary: 0>
    Secondary: anchorpoint.ActionButtonType # value = <ActionButtonType.Secondary: 1>
    SecondaryText: anchorpoint.ActionButtonType # value = <ActionButtonType.SecondaryText: 2>
    __members__: dict # value = {'Primary': <ActionButtonType.Primary: 0>, 'Secondary': <ActionButtonType.Secondary: 1>, 'SecondaryText': <ActionButtonType.SecondaryText: 2>, 'Danger': <ActionButtonType.Danger: 3>}
    pass
class AnchorpointSettings():
    """
    Creates a settings entry in the Anchorpoint preferences.

    This class is a base class and needs to be extended. The get_dialog method should be implemented.
    """
    def __init__(self) -> None: ...
    def get_dialog(self) -> Dialog: 
        """
        Needs to be implemented and return a dialog for the settings.
        """
    @property
    def icon(self) -> typing.Optional[str]:
        """
                    Sets the icon of the settings tab.
                

        :type: typing.Optional[str]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the icon of the settings tab.
        """
    @property
    def name(self) -> str:
        """
                    Sets the name of the settings tab shown in the Anchorpoint preferences.
                

        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        """
        Sets the name of the settings tab shown in the Anchorpoint preferences.
        """
    @property
    def priority(self) -> int:
        """
                    Sets the priority of the settings shown in the Anchorpoint preferences. A higher value results in an entry shown more at the top of the list.
                

        :type: int
        """
    @priority.setter
    def priority(self, arg1: int) -> None:
        """
        Sets the priority of the settings shown in the Anchorpoint preferences. A higher value results in an entry shown more at the top of the list.
        """
    pass
class ApIntegration():
    """
    Creates an integration shown in the integrations overview in the Anchorpoint preferences.

    This class is a base class and needs to be extended. The following methods need to be implemented:
        execute_preferences_action
    """
    def __init__(self) -> None: ...
    def add_create_project_action(self, arg0: IntegrationAction) -> None: 
        """
        Add an integration action shown in the integration project create dialog.
        """
    def add_preferences_action(self, arg0: IntegrationAction) -> None: 
        """
        Add an integration action shown in the integration preferences.
        """
    def clear_create_project_actions(self) -> None: 
        """
        Clear all integration actions registered for project create dialog.
        """
    def clear_preferences_actions(self) -> None: 
        """
        Clear all integration actions registered for preferences.
        """
    def get_create_project_actions(self) -> typing.List[IntegrationAction]: 
        """
        Get all integration actions registered for project create dialog.
        """
    def get_preferences_actions(self) -> typing.List[IntegrationAction]: 
        """
        Get all integration actions registered for preferences.
        """
    def start_auth(self) -> None: 
        """
        Needs to be called when starting an auth process
        """
    def start_update(self) -> None: 
        """
        Call this after updating integration properties to trigger an ui update
        """
    @property
    def dashboard_icon(self) -> typing.Optional[str]:
        """
                    The integration icon shown in the dashboard.
                

        :type: typing.Optional[str]
        """
    @dashboard_icon.setter
    def dashboard_icon(self, arg1: typing.Optional[str]) -> None:
        """
        The integration icon shown in the dashboard.
        """
    @property
    def description(self) -> str:
        """
                    The description of the integration. For example shown in the integration preferences overview.
                

        :type: str
        """
    @description.setter
    def description(self, arg1: str) -> None:
        """
        The description of the integration. For example shown in the integration preferences overview.
        """
    @property
    def is_connected(self) -> bool:
        """
                    Indicates if the integration is connected (e.g. integration that has oauth connection indicating if it is connected or needs reconnect.)
                

        :type: bool
        """
    @is_connected.setter
    def is_connected(self, arg1: bool) -> None:
        """
        Indicates if the integration is connected (e.g. integration that has oauth connection indicating if it is connected or needs reconnect.)
        """
    @property
    def is_setup(self) -> bool:
        """
                    Indicates if the integration is already setup (e.g. connected to a remote account or the path to a depending application is set.)
                

        :type: bool
        """
    @is_setup.setter
    def is_setup(self, arg1: bool) -> None:
        """
        Indicates if the integration is already setup (e.g. connected to a remote account or the path to a depending application is set.)
        """
    @property
    def name(self) -> str:
        """
                    The name of the integration.
                

        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        """
        The name of the integration.
        """
    @property
    def preferences_icon(self) -> typing.Optional[str]:
        """
                    The integration icon shown in the preferences.
                

        :type: typing.Optional[str]
        """
    @preferences_icon.setter
    def preferences_icon(self, arg1: typing.Optional[str]) -> None:
        """
        The integration icon shown in the preferences.
        """
    @property
    def priority(self) -> int:
        """
                    Sets the priority of the integration for display in lists. A higher value results in an entry shown more at the top of a list.
                

        :type: int
        """
    @priority.setter
    def priority(self, arg1: int) -> None:
        """
        Sets the priority of the integration for display in lists. A higher value results in an entry shown more at the top of a list.
        """
    @property
    def tags(self) -> typing.List[str]:
        """
                    Tags set where the integration will be visible in the application.
                    For example the tag "git" will show options of the integration in the project creation or join dialog.
                

        :type: typing.List[str]
        """
    @tags.setter
    def tags(self, arg1: typing.List[str]) -> None:
        """
        Tags set where the integration will be visible in the application.
        For example the tag "git" will show options of the integration in the project creation or join dialog.
        """
    pass
class AttributeChange():
    """
    Represents a value change of an attribute called :attr:`~anchorpoint.AttributeChange.name`. Access the :attr:`~anchorpoint.AttributeChange.old_value` and the new :attr:`~anchorpoint.AttributeChange.value`.
    Using the :attr:`~anchorpoint.AttributeChange.type` property you can check the type of the attribute (e.g. :attr:`~apsync.AttributeType.single_choice_tag`).
    The :attr:`~anchorpoint.AttributeChange.source` value can be used to identify the source of the trigger:

    - :attr:`~anchorpoint.ChangeSource.You`: The change was made by yourself
    - :attr:`~anchorpoint.ChangeSource.Action`: The change was made by an action on your machine
    - :attr:`~anchorpoint.ChangeSource.Other`: The change was made by someone else

    For :attr:`~anchorpoint.ChangeSource.Other`, you can check the client_name (e.g. UnrealEngine) with :attr:`~anchorpoint.AttributeChange.source_value`

    Attributes:
        path (str): The path of the object the attribute was changed for - or None
        task_id (str): The id of the task the attribute was changed for - or None
        object_type (:class:`~apsync.ObjectType`): The type of the object
        name (str): The name of the Attribute
        type (:class:`~apsync.AttributeType`): The type of the attribute
        value (object): The new value of the attribute
        old_value (object): The old value of the attribute
        source (:class:`~anchorpoint.ChangeSource`): The source of the change
        source_value (str): The client_name of the :attr:`~anchorpoint.ChangeSource.Other` source, if any
    """
    def __init__(self) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def object_type(self) -> apsync.ObjectType:
        """
        :type: apsync.ObjectType
        """
    @property
    def old_value(self) -> object:
        """
        :type: object
        """
    @property
    def path(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def source(self) -> ChangeSource:
        """
        :type: ChangeSource
        """
    @property
    def source_value(self) -> str:
        """
        :type: str
        """
    @property
    def task_id(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def type(self) -> apsync.AttributeType:
        """
        :type: apsync.AttributeType
        """
    @property
    def value(self) -> object:
        """
        :type: object
        """
    pass
class BrowseType():
    """
    Members:

      File : The open dialog is filtering for files

      Folder : The open dialog is filtering for folders
    """
    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    File: anchorpoint.BrowseType # value = <BrowseType.File: 1>
    Folder: anchorpoint.BrowseType # value = <BrowseType.Folder: 2>
    __members__: dict # value = {'File': <BrowseType.File: 1>, 'Folder': <BrowseType.Folder: 2>}
    pass
class ChangeSource():
    """
    Members:

      You : The change was made by yourself

      Action : The change was made by an action on your machine

      Other : The change was made by someone else
    """
    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    Action: anchorpoint.ChangeSource # value = <ChangeSource.Action: 2>
    Other: anchorpoint.ChangeSource # value = <ChangeSource.Other: 1>
    You: anchorpoint.ChangeSource # value = <ChangeSource.You: 0>
    __members__: dict # value = {'You': <ChangeSource.You: 0>, 'Action': <ChangeSource.Action: 2>, 'Other': <ChangeSource.Other: 1>}
    pass
class ConflictDetails():
    def __init__(self) -> None: ...
    @property
    def current_branch(self) -> str:
        """
        :type: str
        """
    @current_branch.setter
    def current_branch(self, arg0: str) -> None:
        pass
    @property
    def current_change(self) -> VCChange:
        """
        :type: VCChange
        """
    @current_change.setter
    def current_change(self, arg0: VCChange) -> None:
        pass
    @property
    def current_entry(self) -> TimelineChannelEntry:
        """
        :type: TimelineChannelEntry
        """
    @current_entry.setter
    def current_entry(self, arg0: TimelineChannelEntry) -> None:
        pass
    @property
    def incoming_branch(self) -> str:
        """
        :type: str
        """
    @incoming_branch.setter
    def incoming_branch(self, arg0: str) -> None:
        pass
    @property
    def incoming_change(self) -> VCChange:
        """
        :type: VCChange
        """
    @incoming_change.setter
    def incoming_change(self, arg0: VCChange) -> None:
        pass
    @property
    def incoming_entry(self) -> TimelineChannelEntry:
        """
        :type: TimelineChannelEntry
        """
    @incoming_entry.setter
    def incoming_entry(self, arg0: TimelineChannelEntry) -> None:
        pass
    @property
    def is_text(self) -> bool:
        """
        :type: bool
        """
    @is_text.setter
    def is_text(self, arg0: bool) -> None:
        pass
    pass
class Context():
    """
    The context object is your link between Anchorpoint and the world of actions.
                   It provides you with important information such as the list of selected files or folders, for example.
                   Furthermore, there is a lot of information from the active YAML file, such as defined inputs that the user has entered.

                   In addition, the context offers a variety of helpful functions that simplify working with actions in Anchorpoint.

                   You never create the context yourself, instead you can always retrieve it via the get_context() function.

                   Examples:
                       Retrieving the active context instance is simple:
                           >>> import anchorpoint
                           >>> ctx = anchorpoint.get_context()

                       Access an input "my_input" from a YAML file by providing the key to the inputs dictionary like so:
                           >>> ctx.inputs["my_input"]
                           42

                       Iterate over all selected files:
                           >>> for f in ctx.selected_files:
                           >>>     print(f)

                   Attributes:
                       type (Type): Type.File if the action is running on a file selection, Type.Folder when running on a folder selection.

                       inputs (dict): A dictionary that passes the inputs from the YAML file to the script. The key to the dictionary is the name of the input.

                       path (str): The absolute path to the current file or folder. For a multi selection this is the file / folder the context menu was invoked on.

                       relative_path (str): Project only, the path of the file / folder relative to the project root

                       project_path (str): The project path, if any

                       yaml_dir (str): The directory where the action's YAML lives

                       folder (str): The parent folder of path

                       filename (str): File only, the name of the file without the path and without the extension

                       suffix (str): The suffix of the file (e.g. "blend")

                       username (str): The username of the current user, e.g. "John Doe"

                       email (str): The email of the current user

                       workspace_id (str): The unique id of the workspace the action is executed in

                       selected_files (list): The list of selected files (absolute paths)

                       selected_folders (list): The list of selected folders (absolute paths)

                       selected_tasks (list): The list of selected tasks

                       icon (str): The icon as set in the YAML file

                       icon_color (str): The icon color as set in the YAML file
                   
    """
    def __repr__(self) -> str: ...
    def create_project(self, path: str, name: str, workspace_id: typing.Optional[str] = None) -> object: 
        """
        Create a new project at path. Will create a new folder if necessary.

        Args:
            path (str): The path to the project
            name (str): The name of the project
            workspace_id (Optional[str]): The workspace id - or None to use the user workspace

        Example:                
            >>> ctx = anchorpoint.get_context()
            >>> ctx.create_project("C:/Projects/AP-Commercial", "Anchorpoint Commercial")
        """
    def get_dialog(self) -> typing.Optional[twain::ActionDialog]: 
        """
        Returns the active Python Dialog or None
        """
    def has_team_features(self) -> bool: 
        """
        Checks if the current user has team features in the current workspace
        """
    def install(self, package: str, upgrade: bool = False) -> None: 
        """
        Utility function to pip install a python module to the global python module directory.
        To not slow down your scripts make sure to only call this function when the module import fails

        Args:
            package (str): The name of the package to install (e.g. "Pillow")
            upgrade (bool): Set to True to upgrade the package to the latest version

        Example:                
            >>> ctx = anchorpoint.get_context()
            >>> try:
            >>>     from PIL import Image
            >>> except: 
            >>>     ctx.install("Pillow")
        """
    @staticmethod
    def instance() -> Context: 
        """
        Returns the active instance of the anchorpoint context.
        This function is deprecated, use anchorpoint.get_context() instead

        Example:                
            >>> ctx = anchorpoint.get_context()
        """
    def run_async(self, func: function, *args, **kwargs) -> None: 
        """
        Utility function to run a function asynchronously so that it does not block the Anchorpoint UI.
        Always use this for long running processes such as downloading files or running a rendering job.
        Note: Make sure to capture the Anchorpoint context object on the main thread.

        Args:
            func (Callable[[\*args, \*\*kwargs], None]): The function to run async
            args (\*args): The optional arguments for the function
            args (\*\*kwargs): The optional keyword arguments for the function

        Example:
            Run a function with arguments                
                >>> ctx = anchorpoint.get_context()
                >>> def my_func(path):
                >>>     print("async: " + path)
                >>> 
                >>> ctx.run_async(my_func, ctx.path) 
        """
    @property
    def email(self) -> str:
        """
        :type: str
        """
    @property
    def filename(self) -> str:
        """
        :type: str
        """
    @property
    def folder(self) -> str:
        """
        :type: str
        """
    @property
    def icon(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def icon_color(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def inputs(self) -> dict:
        """
        :type: dict
        """
    @property
    def path(self) -> str:
        """
        :type: str
        """
    @property
    def project_id(self) -> str:
        """
        :type: str
        """
    @property
    def project_path(self) -> str:
        """
        :type: str
        """
    @property
    def relative_path(self) -> str:
        """
        :type: str
        """
    @property
    def selected_files(self) -> typing.List[str]:
        """
        :type: typing.List[str]
        """
    @property
    def selected_folders(self) -> typing.List[str]:
        """
        :type: typing.List[str]
        """
    @property
    def selected_tasks(self) -> typing.List[apsync.Task]:
        """
        :type: typing.List[apsync.Task]
        """
    @property
    def suffix(self) -> str:
        """
        :type: str
        """
    @property
    def type(self) -> Type:
        """
        :type: Type
        """
    @property
    def user_id(self) -> str:
        """
        :type: str
        """
    @property
    def username(self) -> str:
        """
        :type: str
        """
    @property
    def workspace_id(self) -> str:
        """
        :type: str
        """
    @property
    def yaml(self) -> str:
        """
        :type: str
        """
    @property
    def yaml_dir(self) -> str:
        """
        :type: str
        """
    pass
class DialogEntry():
    class DialogEntryType():
        """
        Members:

          Text

          Input

          Checkbox

          Button

          Image

          Tags

          Separator

          SectionStart

          SectionEnd
        """
        def __eq__(self, other: object) -> bool: ...
        def __getstate__(self) -> int: ...
        def __hash__(self) -> int: ...
        def __index__(self) -> int: ...
        def __init__(self, value: int) -> None: ...
        def __int__(self) -> int: ...
        def __ne__(self, other: object) -> bool: ...
        def __repr__(self) -> str: ...
        def __setstate__(self, state: int) -> None: ...
        @property
        def name(self) -> str:
            """
            :type: str
            """
        @property
        def value(self) -> int:
            """
            :type: int
            """
        Button: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Button: 4>
        Checkbox: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Checkbox: 2>
        Image: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Image: 11>
        Input: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Input: 1>
        SectionEnd: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.SectionEnd: 7>
        SectionStart: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.SectionStart: 6>
        Separator: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Separator: 5>
        Tags: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Tags: 12>
        Text: anchorpoint.DialogEntry.DialogEntryType # value = <DialogEntryType.Text: 0>
        __members__: dict # value = {'Text': <DialogEntryType.Text: 0>, 'Input': <DialogEntryType.Input: 1>, 'Checkbox': <DialogEntryType.Checkbox: 2>, 'Button': <DialogEntryType.Button: 4>, 'Image': <DialogEntryType.Image: 11>, 'Tags': <DialogEntryType.Tags: 12>, 'Separator': <DialogEntryType.Separator: 5>, 'SectionStart': <DialogEntryType.SectionStart: 6>, 'SectionEnd': <DialogEntryType.SectionEnd: 7>}
        pass
    def __init__(self) -> None: ...
    def add_button(self, text: str, callback: typing.Callable[[Dialog], None] = None, var: typing.Optional[str] = None, enabled: bool = True, primary: bool = True) -> DialogEntry: 
        """
        Adds a button element to the interface that the user can press.

        Args:
            text (str): The text that is displayed on the button
            callback (Callable[[anchorpoint.Dialog], None]): A callback that is triggered when the user pressed the button
            var (Optional[str]): Assign a variable name to the dropdown that can be used to retrieve the value from the :class:`~anchorpoint.Dialog` again
            enabled (bool): Set to False to disable the interface element

        Examples:
            Provide a callback to get notified on changes
                >>> def my_callback(dialog: anchorpoint.Dialog, value):
                >>>     print (value)
                >>>
                >>> entries = ["Modeling", "Rigging", "Animation", "Rendering"]
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_dropdown("Animation", entries, callback=my_callback)
                >>> dialog.show()
        """
    def add_checkbox(self, default: bool = False, callback: typing.Callable[[Dialog, object], None] = None, var: typing.Optional[str] = None, enabled: bool = True, text: str = '') -> DialogEntry: 
        """
        Adds a checkbox element to the interface.

        Args:
            default (bool): True, if the checkbox should be checked, False otherwise
            callback (Callable[[anchorpoint.Dialog, object], None]): A callback that is triggered when the check state changes
            var (Optional[str]): Assign a variable name to the checkbox that can be used to retrieve the value from the :class:`~anchorpoint.Dialog` again
            enabled (bool): Set to False to disable the interface element
            text (str): The text to display next to the checkbox

        Examples:
            Provide a callback to get notified on changes
                >>> def my_callback(dialog: anchorpoint.Dialog, value):
                >>>     print (value)
                >>>
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_checkbox(True, callback=my_callback)
                >>> dialog.show()
        """
    def add_dropdown(self, default: str, values: typing.List[typing.Union[DropdownEntry, str]], callback: typing.Callable[[Dialog, object], None] = None, var: typing.Optional[str] = None, enabled: bool = True, filterable: bool = False, validate_callback: typing.Callable[[Dialog, object], typing.Optional[str]] = None) -> DialogEntry: 
        """
        Adds a dropdown element to the interface.

        Args:
            default (str): The preselected entry in the dropdown
            values (list | list[:class:`~anchorpoint.DropdownEntry`]): The list of entries in the dropdown
            callback (Callable[[anchorpoint.Dialog, object], None]): A callback that is triggered when the selected entry has changed
            var (Optional[str]): Assign a variable name to the dropdown that can be used to retrieve the value from the :class:`~anchorpoint.Dialog` again
            enabled (bool): Set to False to disable the interface element

        Examples:
            Provide a callback to get notified on changes
                >>> def my_callback(dialog: anchorpoint.Dialog, value):
                >>>     print (value)
                >>>
                >>> values = ["Modeling", "Rigging", "Animation", "Rendering"]
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_dropdown("Modeling", values, callback=my_callback)
                >>> dialog.show()
        """
    def add_empty(self) -> DialogEntry: 
        """
        Adds empty space to the Dialog. This can either be used to indent an entry or to create a new line.

        Example:
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_empty().add_text("Indent")
            >>> dialog.add_empty()
            >>> dialog.add_text("I am in a new line")
            >>> dialog.show()
        """
    def add_image(self, path: str, var: typing.Optional[str] = None, width: int = 300) -> DialogEntry: 
        """
        Adds an image to the dialog.
        """
    def add_info(self, text: str, var: typing.Optional[str] = None) -> DialogEntry: 
        """
        Adds an info element to the interface. Basic HTML syntax is supported

        Args:
            text (str): The info
            var (Optional[str]): Assign a variable name to the info text that can be used to hide the info text, for example

        Example:
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_info("Lorem <b>ipsum</b> dolor sit amet")
            >>> dialog.show()            
        """
    def add_input(self, default: str = '', placeholder: str = '', callback: typing.Callable[[Dialog, object], None] = None, var: typing.Optional[str] = None, enabled: bool = True, browse: typing.Optional[BrowseType] = None, browse_path: typing.Optional[str] = None, password: bool = False, width: int = 200, validate_callback: typing.Callable[[Dialog, object], typing.Optional[str]] = None) -> DialogEntry: 
        """
        Adds an input field to the interface.

        Args:
            default (str): The default text that is shown when the dialog shows up
            placeholder (str): A placeholder text that is shown in an empty input field
            callback (Callable[[anchorpoint.Dialog, object], None]): A callback that is triggered when the input field content changes
            var (Optional[str]): Assign a variable name to the input field that can be used to retrieve the value from the :class:`~anchorpoint.Dialog` again
            enabled (bool): Set to False to disable the interface element
            browse (Optional[anchorpoint.BrowseType]): If set, shows a 'browse' button right beside the input field. Set to anchorpoint.BrowseType.File to ask for a file input or anchorpoint.BrowseType.Folder for a folder input
            browse_path (Optional[str]): If not set, the browse utility opens in the users home location
            password (bool): Set to True to enable password mode
            width (int): The width of the input field. Default is 200
            validate_callback (Callable[[anchorpoint.Dialog, object], Optional[str]]): A callback that is triggered when the input field content changed and needs validation. Return a reason if the input field is not valid.

        Examples:
            Provide a callback to get notified on changes
                >>> def my_callback(dialog: anchorpoint.Dialog, value):
                >>>     print (value)
                >>>
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_input("My Default Value", callback=my_callback)
                >>> dialog.show()
        """
    def add_separator(self) -> DialogEntry: 
        """
        Adds a visual separator to the dialog.
        """
    def add_switch(self, default: bool = False, callback: typing.Callable[[Dialog, object], None] = None, var: typing.Optional[str] = None, enabled: bool = True, text: str = '') -> DialogEntry: 
        """
        Adds a switch element to the interface.

        Args:
            default (bool): True, if the switch should be enabled, False otherwise
            callback (Callable[[anchorpoint.Dialog, object], None]): A callback that is triggered when the switch state changes
            var (Optional[str]): Assign a variable name to the switch that can be used to retrieve the value from the :class:`~anchorpoint.Dialog` again
            enabled (bool): Set to False to disable the interface element
            text (str): The text to display next to the switch

        Examples:
            Provide a callback to get notified on changes
                >>> def my_callback(dialog: anchorpoint.Dialog, value):
                >>>     print (value)
                >>>
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_switch(True, text="Enable Feature", callback=my_callback)
                >>> dialog.show()
        """
    def add_tag_input(self, values: typing.List[str], placeholder: str = None, callback: typing.Callable[[Dialog, object], None] = None, var: typing.Optional[str] = None, enabled: bool = True, width: int = 300) -> DialogEntry: 
        """
        Adds a tag input field to the dialog.
        """
    def add_text(self, text: str, var: typing.Optional[str] = None) -> DialogEntry: 
        """
        Adds a text element to the interface. Basic HTML syntax is supported

        Args:
            text (str): The text
            var (Optional[str]): Assign a variable name to the text that can be used to hide the text, for example

        Example:
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_text("<b>Hello World</b>")
            >>> dialog.show()            
        """
    def get_value(self, var: str) -> object: 
        """
        Gets the current value of the dialog entry as identified by the assigned variable name 'var'.

        Examples:
            Checks the value of a dropdown when the users clicks the OK button
                >>> def button_pressed(dialog: anchorpoint.Dialog):
                >>>     print("The selected task is: " + dialog.get_value("task"))
                >>> 
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_dropdown(default="Modeling", values=["Modeling", "Animation"], var="task")
                >>> dialog.add_button("OK", callback=button_pressed)
                >>> dialog.show()
        """
    def hide_row(self, var: typing.Optional[str] = None, hide: bool = True) -> None: 
        """
        (Un)hides the entire row of dialog entries.

        Examples:
            Hide the OK button as long as the input field is empty
                >>> def input_callback(dialog: anchorpoint.Dialog, value):
                >>>     dialog.hide_row("ok", len(value) == 0)
                >>> 
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_input(callback=input_callback)
                >>> dialog.add_button("OK", var="ok")
                >>> dialog.show()
        """
    def set_browse_path(self, var: str, path: str) -> None: 
        """
        Sets the default browse path for the input field.

        Args:
            var (str): The variable name of the input field
            path (str): The path to set as default

        Example:
            >>> dialog.set_browse_path(var="my_input", path=my_path)
        """
    def set_dropdown_values(self, var: str, selected_value: str, entries: typing.List[typing.Union[DropdownEntry, str]]) -> None: 
        """
        Sets the available dropdown entries and the selected value of the dialog entry as identified by the assigned variable name 'var'.
        """
    def set_enabled(self, var: str, enabled: bool) -> None: 
        """
        Toggles whether or not the dialog entry is enabled. A user cannot interact with a disabled dialog entry.

        Examples:
            Enable the OK button if an input field is not empty.
                >>> def input_callback(dialog: anchorpoint.Dialog, value):
                >>>     dialog.set_enabled("ok", len(value) > 0)
                >>> 
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_input(callback=input_callback)
                >>> dialog.add_button("OK", var="ok")
                >>> dialog.show()
        """
    def set_processing(self, var: str, processing: bool, processing_text: typing.Optional[str] = None) -> None: 
        """
        Enables or disabled the processing state of a button

        Examples:
            >>> def button_callback(dialog: anchorpoint.Dialog):
            >>>     dialog.set_processing("ok", True, "Computing Values")
            >>>
            >>> dialog = anchorpoint.Dialog()
            >>> # Build the dialog
            >>> dialog.add_button("OK", var="ok", callback=button_callback)
            >>> dialog.show()
        """
    def set_value(self, var: str, value: object) -> None: 
        """
        Sets the value of the dialog entry as identified by the assigned variable name 'var'.

        Examples:
            Sets the text value of a button when a dropdown value is changed
                >>> def dropdown_changed(dialog: anchorpoint.Dialog, value):
                >>>     if value == "Blender": dialog.set_value("button", "Create Blender File")
                >>>     elif value == "Photoshop": dialog.set_value("button", "Create Photoshop File")
                >>> 
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.add_dropdown(default="Blender", values=["Blender", "Photoshop"], callback=dropdown_changed)
                >>> dialog.add_button("Create Blender File", var="button")
                >>> dialog.show()
        """
    @property
    def type(self) -> DialogEntry.DialogEntryType:
        """
        :type: DialogEntry.DialogEntryType
        """
    pass
class Dialog(DialogEntry):
    """
    Creates an Anchorpoint styled Dialog that can be used to display arbitrary user interfaces within Anchorpoint.
    A dialog consists of rows of interface elements, a title and an icon. This makes it possible to build very simple dialogs where each element starts in a new row.

    Create a Dialog with a title and three rows (a text label, an input field, and a button):

    .. image:: ../../images/dialog_example_1.png

    |

        >>> dialog = anchorpoint.Dialog()
        >>> dialog.title = "My Custom Dialog"
        >>> dialog.add_text("First Row")
        >>> dialog.add_input("Second Row")
        >>> dialog.add_button("Third Row")
        >>> dialog.show()

    The dialog also allows any number of elements to be lined up in a row. You can also use the 'tab escape character' \t to add a horizontal tab to the layout.

    Create a Dialog with a title and two rows were the first row consists out of two elements:

    .. image:: ../../images/dialog_example_2.png

    |

        >>> dialog = anchorpoint.Dialog()
        >>> dialog.title = "My Custom Dialog"
        >>> dialog.add_text("First Row:\t").add_input()
        >>> dialog.add_button("Second Row")
        >>> dialog.show()

    Most Dialog elements allow the registration of callbacks that are called when the value was changed by the user. A callback can be a simple python function that takes two parameters, the :class:`~anchorpoint.Dialog` and the value of the Dialog element.

    Register a callback that is triggered when the input element is changed by user input:

        >>> def my_callback(dialog: anchorpoint.Dialog, value):
        >>>     print ("Input has changed:" + value)
        >>> 
        >>> dialog = anchorpoint.Dialog()
        >>> dialog.add_input(callback = my_callback)
        >>> dialog.show()

    A button callback takes only one parameter, the :class:`~anchorpoint.Dialog` (because the value would always be True):

        >>> def my_callback(dialog: anchorpoint.Dialog):
        >>>     print ("Button Pressed")
        >>> 
        >>> dialog = anchorpoint.Dialog()
        >>> dialog.add_button("OK", callback = my_callback)
        >>> dialog.show()

    Of course, you can also register a lambda as a callback:

        >>> dialog = anchorpoint.Dialog()
        >>> dialog.add_button("OK", callback = lambda dialog: print("Button Pressed"))
        >>> dialog.show()

    A very common scenario is a Dialog that shows several inputs to the user. The script is evaluating the inputs when the user is clicking an "OK" button. You can assign variable names to most Dialog elements so that you can easily retrieve their values at any point in time:

    Show multiple input fields to the user and evaluate them when the user clicks the "OK" button:

        >>> def button_callback(dialog: anchorpoint.Dialog):
        >>>     input = dialog.get_value("input_field") # Retrieve the dialog value again by using the variable name "input_field"
        >>>     task = dialog.get_value("task") # Retrieve the dialog value again by using the variable name "task"
        >>> 
        >>> dialog = anchorpoint.Dialog()
        >>> dialog.add_input(var="input_field") # Assign a variable named "input_field"
        >>> dialog.add_dropdown("Animating", ["Rigging", "Animating"], var="task") # Assign a variable named "task"
        >>> dialog.add_button("OK", callback = button_callback)
        >>> dialog.show()

    You can register a callback using the callback_closed property that gets triggered whenever the Dialog closes. This is usually a good place to store :class:`~apsync.Settings`, if required:

    Show a Dialog with an input field. Store the input value when the Dialog is closed.

        >>> def dialog_closed(dialog: anchorpoint.Dialog):
        >>>     settings = apsync.Settings()
        >>>     settings.set("input", dialog.get_value("input"))
        >>>     settings.store()
        >>> 
        >>> dialog = anchorpoint.Dialog()
        >>> dialog.add_input(var="input")
        >>> dialog.callback_closed = dialog_closed
        >>> dialog.show()

    Alternatively, you can assign the :class:`~apsync.Settings` object directly to the Dialog. By that, the Dialog reads and writes directly to the :class:`~apsync.Settings`.
        
        >>> dialog = anchorpoint.Dialog()
        >>> dialog.add_input(var="input")
        >>> dialog.callback_closed = dialog_closed
        >>> settings = apsync.Settings()
        >>> dialog.show(settings)
    """
    def __init__(self) -> None: ...
    def close(self) -> None: 
        """
        Closes the Dialog.

        Examples:
            >>> def button_callback(dialog: anchorpoint.Dialog):
            >>>     dialog.close()
            >>> 
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_button("Close Me", callback=button_callback)
            >>> dialog.show()
        """
    def end_section(self) -> DialogEntry: 
        """
        Ends the past opened section, see :func:`~anchorpoint.Dialog.start_section`
        """
    def is_valid(self) -> bool: 
        """
        Gets the current valid state of the dialog based on all child entries. See also validate_callback on input field.
        """
    def load_settings(self, settings: apsync.Settings = None, store_settings_on_close: bool = True) -> None: 
        """
        Manually prefill the dialog items with the provided settings
        """
    def set_invisible_rows(self, arg0: typing.List[str]) -> None: 
        """
        Shows all but the invisible_rows in the dialog

        Examples:
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_text("This will be invisible", var="A")
            >>> dialog.add_text("This will be invisible", var="B")
            >>> dialog.add_text("This will be visible", var="C")
            >>> dialog.add_text("This will be visible")
            >>> dialog.show()
            >>> dialog.set_invisible_rows(["A", "B"])
        """
    def set_valid(self, arg0: bool) -> None: 
        """
        Sets the valid state on the entire Dialog. Usually, the valid state is based on the individual validate_callbacks of the dialog input fields. Explicitly setting the valid states overwrites this.
        """
    @typing.overload
    def show(self, settings: typing.Optional[apsync.Settings] = None, store_settings_on_close: bool = True) -> None: 
        """
        Shows the Dialog to the user. You can optionally provide a :class:`~apsync.Settings` object to persist the user input.

        Examples:
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_text("Hello Anchorpoint Users")
            >>> settings = apsync.Settings()                    
            >>> dialog.show(settings)



        Shows the Dialog to the user. You can optionally provide a :class:`~apsync.SharedSettings` object to persist the user input.

        Examples:
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_text("Hello Anchorpoint Users")
            >>> settings = apsync.Settings(project.workspace_id, "Blender Action Settings")  
            >>> dialog.show()
        """
    @typing.overload
    def show(self, settings: typing.Optional[apsync.SharedSettings] = None, store_settings_on_close: bool = True) -> None: ...
    def start_section(self, text: str, foldable: bool = True, folded: bool = True, enabled: bool = True) -> DialogEntry: 
        """
        Starts a new section. Set foldable=True to make the section foldable and set folded=False to unfold the section by default. Use :func:`~anchorpoint.Dialog.end_section` to close the last opened section

        .. image:: ../../images/dialog_example_section_folded.png

        Args:
            text (str): The section header
            foldable (bool): Toggles whether or not the section is foldable by the user
            folded (bool): Toggles whether or not the section is folded by default
            enabled (bool): Set to False to disable the interface element

        Examples:
            Add a folded section with a button element inside it
                >>> dialog = anchorpoint.Dialog()
                >>> dialog.title = "My Custom Dialog"
                >>> dialog.start_section("Advanced Features", folded = True)
                >>> dialog.add_button("An Advanced Button")
                >>> dialog.end_section()
                >>> dialog.show()
        """
    def store_settings(self) -> None: 
        """
        Stores the :class:`~apsync.Settings` or :class:`~apsync.SharedSettings` that are used by the Dialog.
        This is required when store_settings_on_close is False.

        Examples:
            >>> def button_callback(dialog: anchorpoint.Dialog):
            >>>     dialog.store_settings()
            >>>     dialog.close()
            >>>
            >>> settings = apsync.Settings()
            >>> dialog = anchorpoint.Dialog()
            >>> dialog.add_button("Close Me", callback=button_callback)
            >>> dialog.show(settings, store_settings_on_close=False)
        """
    @property
    def callback_closed(self) -> typing.Callable[[Dialog], None]:
        """
                    Register a callback that is triggered when the Dialog is closed. This is usually a good place to store :class:`~apsync.Settings`, if required
                

        :type: typing.Callable[[Dialog], None]
        """
    @callback_closed.setter
    def callback_closed(self, arg1: typing.Callable[[Dialog], None]) -> None:
        """
        Register a callback that is triggered when the Dialog is closed. This is usually a good place to store :class:`~apsync.Settings`, if required
        """
    @property
    def callback_validate_finsihed(self) -> typing.Callable[[Dialog, bool], None]:
        """
                    Register a callback that is triggered when a dialog entry validation has finished. This is usually a good place to enable or disable buttons based on dialog.is_valid()
                

        :type: typing.Callable[[Dialog, bool], None]
        """
    @callback_validate_finsihed.setter
    def callback_validate_finsihed(self, arg1: typing.Callable[[Dialog, bool], None]) -> None:
        """
        Register a callback that is triggered when a dialog entry validation has finished. This is usually a good place to enable or disable buttons based on dialog.is_valid()
        """
    @property
    def icon(self) -> typing.Optional[str]:
        """
                    Sets the icon of the Dialog. 
                

        :type: typing.Optional[str]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the icon of the Dialog. 
        """
    @property
    def icon_color(self) -> typing.Optional[str]:
        """
                    Sets the icon color of the Dialog. 
                

        :type: typing.Optional[str]
        """
    @icon_color.setter
    def icon_color(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the icon color of the Dialog. 
        """
    @property
    def name(self) -> typing.Optional[str]:
        """
                    Sets the name of the dialog.
                

        :type: typing.Optional[str]
        """
    @name.setter
    def name(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the name of the dialog.
        """
    @property
    def title(self) -> typing.Optional[str]:
        """
                    Sets the title of the dialog. 
                

        :type: typing.Optional[str]
        """
    @title.setter
    def title(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the title of the dialog. 
        """
    pass
class DropdownEntry():
    def __init__(self) -> None: ...
    @property
    def icon(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @icon.setter
    def icon(self, arg1: str) -> None:
        pass
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        pass
    @property
    def use_icon_color(self) -> bool:
        """
        :type: bool
        """
    @use_icon_color.setter
    def use_icon_color(self, arg0: bool) -> None:
        pass
    pass
class IntegrationAction():
    def __init__(self) -> None: ...
    @property
    def enabled(self) -> bool:
        """
        :type: bool
        """
    @enabled.setter
    def enabled(self, arg0: bool) -> None:
        pass
    @property
    def icon(self) -> typing.Optional[apsync.Icon]:
        """
                    The optional icon of the integration action shown in the ui.
                

        :type: typing.Optional[apsync.Icon]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[apsync.Icon]) -> None:
        """
        The optional icon of the integration action shown in the ui.
        """
    @property
    def identifier(self) -> str:
        """
                    The identifier of the integration action (used as a parameter by e.g. executePreferencesAction)
                

        :type: str
        """
    @identifier.setter
    def identifier(self, arg1: str) -> None:
        """
        The identifier of the integration action (used as a parameter by e.g. executePreferencesAction)
        """
    @property
    def name(self) -> str:
        """
                    The name of the integration action shown in the ui.
                

        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        """
        The name of the integration action shown in the ui.
        """
    @property
    def tooltip(self) -> typing.Optional[str]:
        """
                    The optional tooltip of the integration action shown on hover in the ui.
                

        :type: typing.Optional[str]
        """
    @tooltip.setter
    def tooltip(self, arg1: str) -> None:
        """
        The optional tooltip of the integration action shown on hover in the ui.
        """
    pass
class IntegrationList():
    def __init__(self) -> None: ...
    def add(self, arg0: ApIntegration) -> None: 
        """
        Add your derived ApIntegration class via this method to register it for the use in Anchorpoint.
        """
    def get_all_for_tags(self, arg0: typing.List[str]) -> typing.List[ApIntegration]: 
        """
        Get all APIntegrations that have at least one of the given tags
        """
    pass
class LockDisabler():
    def __init__(self) -> None: ...
    def enable_locking(self) -> None: 
        """
        Enables locking again
        """
    pass
class Progress():
    """
    When creating a Progress object a spinning indicator is shown to the user indicating that something is being processed.
    Additionally, a job in the Anchorpoint Job Stack is created showing title and the provided text.
    The progress indicator goes away as soon as the object is destroyed or when :func:`~anchorpoint.Progress.finish` is called explicitly.

    Attributes:
        canceled (bool): True if the user has canceled the action. Can be polled when doing a long running async job.

    Examples:
        Create a spinning progress indicator
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene")

        Report a 50% percentage to indicate progress
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene", infinite = False)
            >>> progress.report_progress(0.5)
    """
    def __init__(self, title: str, text: typing.Optional[str] = None, infinite: bool = True, cancelable: bool = False, show_loading_screen: bool = False, delay: typing.Optional[int] = None) -> None: 
        """
        Creates a Progress object that starts spinning in the Anchorpoint UI.

        Args:
            title (str): The title of the indicator
            text (Optional[str]): The text of the indicator
            infinite (bool): Set to False when you want to report progress from 0% - 100%, set to True to show a simple infinite spinning progress.
            cancelable (bool): Set to True so that the user can cancel the action in the UI. React to cancellation by polling :func:`~anchorpoint.Progress.cancelled`
            show_loading_screen (bool): Tells anchorpoint to show a global loading screen
            delay (int): Only show the progress after delay milliseconds

        Example:
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene")
        """
    def finish(self) -> None: 
        """
        Explicitly finishes the progress indicator.

        Example:
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene")
            >>> progress.finish()
        """
    def report_progress(self, percentage: float) -> None: 
        """
        Reports progress to the users by showing a loading bar. Only works if you set infinite=False.

        Args:
            percentage (float): Percentage from 0 to 1

        Example:
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene", infinite=False)
            >>> progress.report_progress(0.75)
        """
    def set_cancelable(self, arg0: bool) -> None: 
        """
        Changes if the progress indicator is cancelable
        """
    def set_text(self, text: str) -> None: 
        """
        Updates the text in the progress indicator

        Args:
            text (str): The text to set

        Example:
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene")
            >>> progress.set_text("Uploading Rendering")
        """
    def stop_progress(self) -> None: 
        """
        Falls back to a spinning progress indicator

        Example:
            >>> progress = anchorpoint.Progress("My Action", "Rendering the scene", infinite=False)
            >>> progress.report_progress(0.75)
            >>> progress.stop_progress()
        """
    @property
    def canceled(self) -> bool:
        """
                    True if the user has canceled the action. Can be polled when doing a long running async job.

                    Example:
                        >>> progress = anchorpoint.Progress("My Action", "Rendering the scene", cancelable = True)
                        >>> if progress.canceled: return
                

        :type: bool
        """
    pass
class ProjectType():
    """
    Creates an Anchorpoint project type that can be used as another project creation method within the project creation dialog.

    This class is a base class and needs to be extended. The get_dialog and create_project methods should be implemented.
    """
    def __init__(self) -> None: ...
    def get_dialog(self) -> Dialog: 
        """
        Needs to be implemented and return a dialog for additional inputs for this project type e.g. an input field for the location of the project files.
        """
    def get_project_name_candidate(self) -> str: 
        """
        Can provide a default project name for the name step in the create project dialog. The user can change this default in the name step.
        """
    def get_project_path(self) -> Dialog: 
        """
        Needs to be implemented and return a string that represents the location of the project files.
        """
    def project_created(self) -> None: 
        """
        Called after the project creation has finished.
        """
    def setup_project(self, arg0: str, arg1: Progress) -> None: 
        """
        Needs to be implemented and should create the project based on the user inputs and the already created anchorpoint project optainable via project id parameter.
        """
    @property
    def description(self) -> str:
        """
                    Sets the description of the project type shown in the create project dialog.
                

        :type: str
        """
    @description.setter
    def description(self, arg1: str) -> None:
        """
        Sets the description of the project type shown in the create project dialog.
        """
    @property
    def icon(self) -> typing.Optional[str]:
        """
                    Sets the icon of the project type shown in the create project dialog.
                

        :type: typing.Optional[str]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the icon of the project type shown in the create project dialog.
        """
    @property
    def icon_color(self) -> typing.Optional[str]:
        """
                    Sets the icon color of the project type shown in the create project dialog.
                

        :type: typing.Optional[str]
        """
    @icon_color.setter
    def icon_color(self, arg1: typing.Optional[str]) -> None:
        """
        Sets the icon color of the project type shown in the create project dialog.
        """
    @property
    def name(self) -> str:
        """
                    Sets the name of the project type shown in the create project dialog.
                

        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        """
        Sets the name of the project type shown in the create project dialog.
        """
    @property
    def pre_selected(self) -> bool:
        """
                    Sets if these project type should be preselected when the create project dialog is first shown.
                

        :type: bool
        """
    @pre_selected.setter
    def pre_selected(self, arg1: bool) -> None:
        """
        Sets if these project type should be preselected when the create project dialog is first shown.
        """
    @property
    def priority(self) -> int:
        """
                    Sets the priority of the project type shown in the create project dialog. A higher value results in an entry shown more at the top of the list.
                    The default project creation method wihtin Anchorpoint has a value of 1000. So e.g. a value of 1001 will place this project type at the top of the dialog.
                

        :type: int
        """
    @priority.setter
    def priority(self, arg1: int) -> None:
        """
        Sets the priority of the project type shown in the create project dialog. A higher value results in an entry shown more at the top of the list.
        The default project creation method wihtin Anchorpoint has a value of 1000. So e.g. a value of 1001 will place this project type at the top of the dialog.
        """
    pass
class ProjectTypeList():
    def __init__(self) -> None: ...
    def add(self, arg0: ProjectType) -> None: 
        """
        Add your derived ProjectType class via this method to register it for the create project dialog.
        """
    pass
class RemoteFolderEntry():
    def __init__(self) -> None: ...
    @property
    def is_download_root(self) -> bool:
        """
        :type: bool
        """
    @is_download_root.setter
    def is_download_root(self, arg0: bool) -> None:
        pass
    @property
    def is_remote(self) -> bool:
        """
        :type: bool
        """
    @is_remote.setter
    def is_remote(self, arg0: bool) -> None:
        pass
    @property
    def path(self) -> str:
        """
        :type: str
        """
    @path.setter
    def path(self, arg1: str) -> None:
        pass
    pass
class SettingsList():
    def __init__(self) -> None: ...
    def add(self, arg0: AnchorpointSettings) -> None: 
        """
        Add your derived AnchorpointSettings class via this method to register it for the Anchorpoint preferences.
        """
    pass
class TimelineChannelAction():
    def __init__(self) -> None: ...
    @property
    def enabled(self) -> bool:
        """
        :type: bool
        """
    @enabled.setter
    def enabled(self, arg0: bool) -> None:
        pass
    @property
    def icon(self) -> typing.Optional[apsync.Icon]:
        """
        :type: typing.Optional[apsync.Icon]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[apsync.Icon]) -> None:
        pass
    @property
    def identifier(self) -> str:
        """
        :type: str
        """
    @identifier.setter
    def identifier(self, arg1: str) -> None:
        pass
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        pass
    @property
    def tooltip(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @tooltip.setter
    def tooltip(self, arg1: str) -> None:
        pass
    @property
    def type(self) -> ActionButtonType:
        """
        :type: ActionButtonType
        """
    @type.setter
    def type(self, arg0: ActionButtonType) -> None:
        pass
    pass
class TimelineChannelActionList():
    def __bool__(self) -> bool: 
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None: 
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...
    @typing.overload
    def __getitem__(self, s: slice) -> TimelineChannelActionList: 
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> TimelineChannelAction: ...
    @typing.overload
    def __init__(self) -> None: 
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: TimelineChannelActionList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...
    @typing.overload
    def __setitem__(self, arg0: int, arg1: TimelineChannelAction) -> None: 
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: TimelineChannelActionList) -> None: ...
    def append(self, x: TimelineChannelAction) -> None: 
        """
        Add an item to the end of the list
        """
    def clear(self) -> None: 
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: TimelineChannelActionList) -> None: 
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...
    def insert(self, i: int, x: TimelineChannelAction) -> None: 
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> TimelineChannelAction: 
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> TimelineChannelAction: ...
    pass
class TimelineChannelEntry():
    def __copy__(self) -> TimelineChannelEntry: ...
    def __deepcopy__(self, arg0: dict) -> TimelineChannelEntry: 
        """
        memo
        """
    def __init__(self) -> None: ...
    @property
    def caption(self) -> str:
        """
        :type: str
        """
    @caption.setter
    def caption(self, arg1: str) -> None:
        pass
    @property
    def has_details(self) -> bool:
        """
        :type: bool
        """
    @has_details.setter
    def has_details(self, arg1: bool) -> None:
        pass
    @property
    def icon(self) -> typing.Optional[apsync.Icon]:
        """
        :type: typing.Optional[apsync.Icon]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[apsync.Icon]) -> None:
        pass
    @property
    def id(self) -> str:
        """
        :type: str
        """
    @id.setter
    def id(self, arg1: str) -> None:
        pass
    @property
    def message(self) -> str:
        """
        :type: str
        """
    @message.setter
    def message(self, arg1: str) -> None:
        pass
    @property
    def parents(self) -> typing.List[TimelineChannelEntry]:
        """
        :type: typing.List[TimelineChannelEntry]
        """
    @parents.setter
    def parents(self, arg1: typing.List[TimelineChannelEntry]) -> None:
        pass
    @property
    def path(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @path.setter
    def path(self, arg1: str) -> None:
        pass
    @property
    def time(self) -> int:
        """
        :type: int
        """
    @time.setter
    def time(self, arg1: int) -> None:
        pass
    @property
    def tooltip(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @tooltip.setter
    def tooltip(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def user_email(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @user_email.setter
    def user_email(self, arg1: str) -> None:
        pass
    pass
class TimelineChannelEntryDetails():
    def __init__(self) -> None: ...
    @property
    def actions(self) -> TimelineChannelActionList:
        """
        :type: TimelineChannelActionList
        """
    @actions.setter
    def actions(self, arg0: TimelineChannelActionList) -> None:
        pass
    @property
    def entry_actions(self) -> TimelineChannelActionList:
        """
        :type: TimelineChannelActionList
        """
    @entry_actions.setter
    def entry_actions(self, arg0: TimelineChannelActionList) -> None:
        pass
    pass
class TimelineChannelEntryVCDetails(TimelineChannelEntryDetails):
    def __init__(self) -> None: ...
    @property
    def changes(self) -> VCChangeList:
        """
        :type: VCChangeList
        """
    @changes.setter
    def changes(self, arg0: VCChangeList) -> None:
        pass
    pass
class TimelineChannelInfo():
    def __init__(self) -> None: ...
    @property
    def actions(self) -> TimelineChannelActionList:
        """
        :type: TimelineChannelActionList
        """
    @actions.setter
    def actions(self, arg0: TimelineChannelActionList) -> None:
        pass
    pass
class TimelineChannelVCInfo(TimelineChannelInfo):
    def __init__(self) -> None: ...
    @property
    def branches(self) -> VCBranchList:
        """
        :type: VCBranchList
        """
    @branches.setter
    def branches(self, arg0: VCBranchList) -> None:
        pass
    @property
    def current_branch(self) -> typing.Optional[VCBranch]:
        """
        :type: typing.Optional[VCBranch]
        """
    @current_branch.setter
    def current_branch(self, arg1: typing.Optional[VCBranch]) -> None:
        pass
    @property
    def has_stash(self) -> bool:
        """
        :type: bool
        """
    @has_stash.setter
    def has_stash(self, arg0: bool) -> None:
        pass
    @property
    def stashed_file_count(self) -> int:
        """
        :type: int
        """
    @stashed_file_count.setter
    def stashed_file_count(self, arg0: int) -> None:
        pass
    pass
class Type():
    """
    Members:

      File

      Folder

      NewFile

      NewFolder

      Drive

      NewDrive

      AddProjectFiles

      JoinProjectFiles

      Sidebar

      WorkspaceOverview

      ActionEnabled

      LoadIntegrations

      VCLoadConflictDetails

      VCLoadFiles

      GetChangesInfo

      MergeBranch

      SwitchBranch

      CreateBranch

      ResolveConflicts

      PendingChangesAction

      LoadTimelineChannelStashDetails

      LoadTimelineChannelPendingChanges

      AddLoggingData

      LocksRemoved

      TaskRemoved

      TaskChanged

      TaskCreated

      RemovedFromWorkspace

      AddedToWorkspace

      RemovedFromProject

      AddedToProject

      RemoveUserFromWorkspace

      AddUserToWorkspace

      RemoveUserFromProject

      AddUserToProject

      ShowProjectPreferences

      ShowAccountPreferences

      ShowCreateProject

      TimelineRefresh

      UnloadRemoteFolder

      LoadTimelineChannelInfo

      ApplicationStarted

      ProjectDirectoryChanged

      LoadRemoteFolders

      TimelineChannelAction

      FolderCreated

      LoadTimelineChannelEntryDetails

      ResolveDeeplink

      LoadTimelineChannelEntries

      ApplicationClosing

      LoadFirstTimelineChannelEntry

      FolderOpened

      TimeInterval

      DownloadRemoteFolder

      TimelineDetailAction

      AttributesChanged
    """
    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    ActionEnabled: anchorpoint.Type # value = <Type.ActionEnabled: 14>
    AddLoggingData: anchorpoint.Type # value = <Type.AddLoggingData: 39>
    AddProjectFiles: anchorpoint.Type # value = <Type.AddProjectFiles: 8>
    AddUserToProject: anchorpoint.Type # value = <Type.AddUserToProject: 27>
    AddUserToWorkspace: anchorpoint.Type # value = <Type.AddUserToWorkspace: 29>
    AddedToProject: anchorpoint.Type # value = <Type.AddedToProject: 31>
    AddedToWorkspace: anchorpoint.Type # value = <Type.AddedToWorkspace: 33>
    ApplicationClosing: anchorpoint.Type # value = <Type.ApplicationClosing: 18>
    ApplicationStarted: anchorpoint.Type # value = <Type.ApplicationStarted: 17>
    AttributesChanged: anchorpoint.Type # value = <Type.AttributesChanged: 12>
    CreateBranch: anchorpoint.Type # value = <Type.CreateBranch: 51>
    DownloadRemoteFolder: anchorpoint.Type # value = <Type.DownloadRemoteFolder: 22>
    Drive: anchorpoint.Type # value = <Type.Drive: 5>
    File: anchorpoint.Type # value = <Type.File: 1>
    Folder: anchorpoint.Type # value = <Type.Folder: 3>
    FolderCreated: anchorpoint.Type # value = <Type.FolderCreated: 20>
    FolderOpened: anchorpoint.Type # value = <Type.FolderOpened: 15>
    GetChangesInfo: anchorpoint.Type # value = <Type.GetChangesInfo: 54>
    JoinProjectFiles: anchorpoint.Type # value = <Type.JoinProjectFiles: 9>
    LoadFirstTimelineChannelEntry: anchorpoint.Type # value = <Type.LoadFirstTimelineChannelEntry: 41>
    LoadIntegrations: anchorpoint.Type # value = <Type.LoadIntegrations: 57>
    LoadRemoteFolders: anchorpoint.Type # value = <Type.LoadRemoteFolders: 21>
    LoadTimelineChannelEntries: anchorpoint.Type # value = <Type.LoadTimelineChannelEntries: 42>
    LoadTimelineChannelEntryDetails: anchorpoint.Type # value = <Type.LoadTimelineChannelEntryDetails: 43>
    LoadTimelineChannelInfo: anchorpoint.Type # value = <Type.LoadTimelineChannelInfo: 40>
    LoadTimelineChannelPendingChanges: anchorpoint.Type # value = <Type.LoadTimelineChannelPendingChanges: 47>
    LoadTimelineChannelStashDetails: anchorpoint.Type # value = <Type.LoadTimelineChannelStashDetails: 48>
    LocksRemoved: anchorpoint.Type # value = <Type.LocksRemoved: 38>
    MergeBranch: anchorpoint.Type # value = <Type.MergeBranch: 53>
    NewDrive: anchorpoint.Type # value = <Type.NewDrive: 6>
    NewFile: anchorpoint.Type # value = <Type.NewFile: 2>
    NewFolder: anchorpoint.Type # value = <Type.NewFolder: 4>
    PendingChangesAction: anchorpoint.Type # value = <Type.PendingChangesAction: 49>
    ProjectDirectoryChanged: anchorpoint.Type # value = <Type.ProjectDirectoryChanged: 16>
    RemoveUserFromProject: anchorpoint.Type # value = <Type.RemoveUserFromProject: 28>
    RemoveUserFromWorkspace: anchorpoint.Type # value = <Type.RemoveUserFromWorkspace: 30>
    RemovedFromProject: anchorpoint.Type # value = <Type.RemovedFromProject: 32>
    RemovedFromWorkspace: anchorpoint.Type # value = <Type.RemovedFromWorkspace: 34>
    ResolveConflicts: anchorpoint.Type # value = <Type.ResolveConflicts: 50>
    ResolveDeeplink: anchorpoint.Type # value = <Type.ResolveDeeplink: 19>
    ShowAccountPreferences: anchorpoint.Type # value = <Type.ShowAccountPreferences: 25>
    ShowCreateProject: anchorpoint.Type # value = <Type.ShowCreateProject: 24>
    ShowProjectPreferences: anchorpoint.Type # value = <Type.ShowProjectPreferences: 26>
    Sidebar: anchorpoint.Type # value = <Type.Sidebar: 10>
    SwitchBranch: anchorpoint.Type # value = <Type.SwitchBranch: 52>
    TaskChanged: anchorpoint.Type # value = <Type.TaskChanged: 36>
    TaskCreated: anchorpoint.Type # value = <Type.TaskCreated: 35>
    TaskRemoved: anchorpoint.Type # value = <Type.TaskRemoved: 37>
    TimeInterval: anchorpoint.Type # value = <Type.TimeInterval: 13>
    TimelineChannelAction: anchorpoint.Type # value = <Type.TimelineChannelAction: 44>
    TimelineDetailAction: anchorpoint.Type # value = <Type.TimelineDetailAction: 45>
    TimelineRefresh: anchorpoint.Type # value = <Type.TimelineRefresh: 46>
    UnloadRemoteFolder: anchorpoint.Type # value = <Type.UnloadRemoteFolder: 23>
    VCLoadConflictDetails: anchorpoint.Type # value = <Type.VCLoadConflictDetails: 56>
    VCLoadFiles: anchorpoint.Type # value = <Type.VCLoadFiles: 55>
    WorkspaceOverview: anchorpoint.Type # value = <Type.WorkspaceOverview: 11>
    __members__: dict # value = {'File': <Type.File: 1>, 'Folder': <Type.Folder: 3>, 'NewFile': <Type.NewFile: 2>, 'NewFolder': <Type.NewFolder: 4>, 'Drive': <Type.Drive: 5>, 'NewDrive': <Type.NewDrive: 6>, 'AddProjectFiles': <Type.AddProjectFiles: 8>, 'JoinProjectFiles': <Type.JoinProjectFiles: 9>, 'Sidebar': <Type.Sidebar: 10>, 'WorkspaceOverview': <Type.WorkspaceOverview: 11>, 'ActionEnabled': <Type.ActionEnabled: 14>, 'LoadIntegrations': <Type.LoadIntegrations: 57>, 'VCLoadConflictDetails': <Type.VCLoadConflictDetails: 56>, 'VCLoadFiles': <Type.VCLoadFiles: 55>, 'GetChangesInfo': <Type.GetChangesInfo: 54>, 'MergeBranch': <Type.MergeBranch: 53>, 'SwitchBranch': <Type.SwitchBranch: 52>, 'CreateBranch': <Type.CreateBranch: 51>, 'ResolveConflicts': <Type.ResolveConflicts: 50>, 'PendingChangesAction': <Type.PendingChangesAction: 49>, 'LoadTimelineChannelStashDetails': <Type.LoadTimelineChannelStashDetails: 48>, 'LoadTimelineChannelPendingChanges': <Type.LoadTimelineChannelPendingChanges: 47>, 'AddLoggingData': <Type.AddLoggingData: 39>, 'LocksRemoved': <Type.LocksRemoved: 38>, 'TaskRemoved': <Type.TaskRemoved: 37>, 'TaskChanged': <Type.TaskChanged: 36>, 'TaskCreated': <Type.TaskCreated: 35>, 'RemovedFromWorkspace': <Type.RemovedFromWorkspace: 34>, 'AddedToWorkspace': <Type.AddedToWorkspace: 33>, 'RemovedFromProject': <Type.RemovedFromProject: 32>, 'AddedToProject': <Type.AddedToProject: 31>, 'RemoveUserFromWorkspace': <Type.RemoveUserFromWorkspace: 30>, 'AddUserToWorkspace': <Type.AddUserToWorkspace: 29>, 'RemoveUserFromProject': <Type.RemoveUserFromProject: 28>, 'AddUserToProject': <Type.AddUserToProject: 27>, 'ShowProjectPreferences': <Type.ShowProjectPreferences: 26>, 'ShowAccountPreferences': <Type.ShowAccountPreferences: 25>, 'ShowCreateProject': <Type.ShowCreateProject: 24>, 'TimelineRefresh': <Type.TimelineRefresh: 46>, 'UnloadRemoteFolder': <Type.UnloadRemoteFolder: 23>, 'LoadTimelineChannelInfo': <Type.LoadTimelineChannelInfo: 40>, 'ApplicationStarted': <Type.ApplicationStarted: 17>, 'ProjectDirectoryChanged': <Type.ProjectDirectoryChanged: 16>, 'LoadRemoteFolders': <Type.LoadRemoteFolders: 21>, 'TimelineChannelAction': <Type.TimelineChannelAction: 44>, 'FolderCreated': <Type.FolderCreated: 20>, 'LoadTimelineChannelEntryDetails': <Type.LoadTimelineChannelEntryDetails: 43>, 'ResolveDeeplink': <Type.ResolveDeeplink: 19>, 'LoadTimelineChannelEntries': <Type.LoadTimelineChannelEntries: 42>, 'ApplicationClosing': <Type.ApplicationClosing: 18>, 'LoadFirstTimelineChannelEntry': <Type.LoadFirstTimelineChannelEntry: 41>, 'FolderOpened': <Type.FolderOpened: 15>, 'TimeInterval': <Type.TimeInterval: 13>, 'DownloadRemoteFolder': <Type.DownloadRemoteFolder: 22>, 'TimelineDetailAction': <Type.TimelineDetailAction: 45>, 'AttributesChanged': <Type.AttributesChanged: 12>}
    pass
class UI():
    class ToastType():
        """
        Members:

          Success

          Error

          Info
        """
        def __eq__(self, other: object) -> bool: ...
        def __getstate__(self) -> int: ...
        def __hash__(self) -> int: ...
        def __index__(self) -> int: ...
        def __init__(self, value: int) -> None: ...
        def __int__(self) -> int: ...
        def __ne__(self, other: object) -> bool: ...
        def __repr__(self) -> str: ...
        def __setstate__(self, state: int) -> None: ...
        @property
        def name(self) -> str:
            """
            :type: str
            """
        @property
        def value(self) -> int:
            """
            :type: int
            """
        Error: anchorpoint.UI.ToastType # value = <ToastType.Error: 1>
        Info: anchorpoint.UI.ToastType # value = <ToastType.Info: 2>
        Success: anchorpoint.UI.ToastType # value = <ToastType.Success: 0>
        __members__: dict # value = {'Success': <ToastType.Success: 0>, 'Error': <ToastType.Error: 1>, 'Info': <ToastType.Info: 2>}
        pass
    def __init__(self) -> None: ...
    def clear_console(self) -> None: 
        """
        Clears the console 

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.clear_console()
        """
    def create_tab(self, folder_path: str) -> None: 
        """
        Creates a new tab in the Anchorpoint browser.

        Args:
            folder_path (str): The folder the new tab will open

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.create_tab("C:/Users/JohnDoe/Documents")
        """
    def finish_busy(self, file: str) -> None: 
        """
        Stops the spinning wheel on the file for the Anchorpoint browser. Use it after :func:`~anchorpoint.UI.show_busy`

        Args:
            file (str): The file to stop the spinning wheel for

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.stop_busy("path/to/my/file")
        """
    def get_qml_engine(self) -> object: ...
    def hide_loading_screen(self) -> None: 
        """
        Stops the global loading screen.

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.hide_loading_screen()
        """
    def metadata_pending(self, file: str, preview: bool = True, detail: bool = False) -> None: 
        """
        Tells the UI that the preview and / or detail image of the file are pending
        """
    def navigate_to_channel_detail(self, channel_id: str, entry_id: str) -> None: 
        """
        Opens the channel detail sidebar in Anchorpoint browser for the provided channel_id and entry_id

        Args:
            channel_id (str): The channel_id of the project to navigate to
            entry_id (str): Whether to open the project in a new tab or not (default false)

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.navigate_to_project("{channel_id}", "{entry_id}")
        """
    def navigate_to_file(self, file_path: str) -> None: 
        """
        Navigates the Anchorpoint browser to the provided 'file_path'

        Args:
            file_path (str): The file to navigate to

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.navigate_to_file("C:/Users/JohnDoe/Documents/cube.blend")
        """
    def navigate_to_folder(self, folder_path: str) -> None: 
        """
        Navigates the Anchorpoint browser to the provided 'folder_path'

        Args:
            folder_path (str): The folder to navigate into

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.navigate_to_folder("C:/Users/JohnDoe/Documents")
        """
    def navigate_to_project(self, project_id: str, in_new_tab: bool = False) -> None: 
        """
        Navigates the Anchorpoint browser to the provided project_id if possible

        Args:
            project_id (str): The project_id of the project to navigate to
            in_new_tab (bool): Whether to open the project in a new tab or not (default false)

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.navigate_to_project("{project_id}", inNewTab=True)
        """
    def open_tab(self, folder_path: str) -> None: 
        """
        Opens (create + navigate) a new tab in the Anchorpoint browser.

        Args:
            folder_path (str): The folder the new tab will open

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.open_tab("C:/Users/JohnDoe/Documents")
        """
    def reload(self) -> None: 
        """
        Reloads the current view of the Anchorpoint browser. 

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.reload()
        """
    def reload_drives(self) -> None: 
        """
        Reloads the drives, e.g. after mounting a network drive

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.reload_drives()
        """
    def reload_tree(self) -> None: 
        """
        Reloads the tree, e.g. after updating virtual folders

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.reload_tree()
        """
    def replace_thumbnail(self, file: str, thumbnail: str) -> None: 
        """
        A helper function that replaces the thumbnail for both the browser (preview) and the detailled thumbnail (detail).
        Can be used instead of :func:`~apsync.attach_thumbnail`

        Args:
            file (str): The file the thumbnail should be set for
            thumbnail (str): The path to the thumbnail that should be set. The Thumbnail is downscaled for the preview

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.replace_thumbnail("path/to/my/file", "path/to/my/thumbnail")
        """
    def replace_thumbnail_tool(self, file: str) -> None: 
        """
        Invokes the replace thumbnail tool, which allows the user to take a screenshot that will be used as a thumbnail

        Args:
            file (str): The file the thumbnail should be set for

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.replace_thumbnail_tool("path/to/my/file")
        """
    def show_busy(self, file: str) -> None: 
        """
        Enables a spinning wheel on the file for the Anchorpoint browser to indicate that the file is being processed.

        Args:
            file (str): The file that is being processed

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.show_busy("path/to/my/file")
        """
    def show_console(self) -> None: 
        """
        Shows the command console to the user. A handy tool when developing actions. 

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.show_console()
        """
    def show_error(self, title: str, description: str = '', duration: int = 4000) -> None: 
        """
        Shows an error toast message within Anchorpoint

        Args:
            title (str): The title of the toast
            description (str): The description of the toast
            duration (int): The duration (in ms) the toast is shown to the user

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.show_error("Computation Failed", "The answer is NOT 42!")
        """
    def show_info(self, title: str, description: str = '', duration: int = 4000) -> None: 
        """
        Shows an info toast message within Anchorpoint

        Args:
            title (str): The title of the toast
            description (str): The description of the toast
            duration (int): The duration (in ms) the toast is shown to the user

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.show_info("Computation has started", "The answer is most likely 42")
        """
    def show_loading_screen(self, message: str) -> None: 
        """
        Enables a global loading screen that shows a message.

        Args:
            file (message): The message to show

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.show_loading_screen("Processing")
        """
    def show_success(self, title: str, description: str = '', duration: int = 4000) -> None: 
        """
        Shows a success toast message within Anchorpoint

        Args:
            title (str): The title of the toast
            description (str): The description of the toast
            duration (int): The duration (in ms) the toast is shown to the user

        Example:
            >>> ui = anchorpoint.UI()
            >>> ui.show_success("Computation Finished", "The answer is 42!")
        """
    def show_system_notification(self, title: str, message: str, callback: typing.Callable[[], None] = None, path_to_image: typing.Optional[str] = None) -> None: 
        """
        Shows a notification on the operating system

        Args:
            title (str): The title of the system notification
            message (str): The message of the system notification
            callback (Callable[[], None]): A callback that is triggered when the notification is clicked
            path_to_image (Optional[str]): optional file path to an image that will be displayed in the notification

        Example:
            >>> def my_callback():
                print("Notification callback called")
            >>>
            >>> ui = anchorpoint.UI()
            >>> ui.show_system_notification("Custom Notification", "Here is some custom notification" callback=my_callback)
        """
    pass
class VCBranch():
    def __init__(self) -> None: ...
    @property
    def author(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @author.setter
    def author(self, arg1: str) -> None:
        pass
    @property
    def moddate(self) -> object:
        """
        :type: object
        """
    @moddate.setter
    def moddate(self, arg1: object) -> None:
        pass
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        pass
    pass
class VCBranchList():
    def __bool__(self) -> bool: 
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None: 
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...
    @typing.overload
    def __getitem__(self, s: slice) -> VCBranchList: 
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> VCBranch: ...
    @typing.overload
    def __init__(self) -> None: 
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: VCBranchList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...
    @typing.overload
    def __setitem__(self, arg0: int, arg1: VCBranch) -> None: 
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: VCBranchList) -> None: ...
    def append(self, x: VCBranch) -> None: 
        """
        Add an item to the end of the list
        """
    def clear(self) -> None: 
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: VCBranchList) -> None: 
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...
    def insert(self, i: int, x: VCBranch) -> None: 
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> VCBranch: 
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> VCBranch: ...
    pass
class VCChange():
    def __copy__(self) -> VCChange: ...
    def __deepcopy__(self, arg0: dict) -> VCChange: 
        """
        memo
        """
    def __init__(self) -> None: ...
    @property
    def cachable(self) -> bool:
        """
        :type: bool
        """
    @cachable.setter
    def cachable(self, arg0: bool) -> None:
        pass
    @property
    def cached_path(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @cached_path.setter
    def cached_path(self, arg1: typing.Optional[str]) -> None:
        pass
    @property
    def path(self) -> str:
        """
        :type: str
        """
    @path.setter
    def path(self, arg1: str) -> None:
        pass
    @property
    def status(self) -> VCFileStatus:
        """
        :type: VCFileStatus
        """
    @status.setter
    def status(self, arg0: VCFileStatus) -> None:
        pass
    pass
class VCChangeList():
    def __bool__(self) -> bool: 
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None: 
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...
    @typing.overload
    def __getitem__(self, s: slice) -> VCChangeList: 
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> VCChange: ...
    @typing.overload
    def __init__(self) -> None: 
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: VCChangeList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...
    @typing.overload
    def __setitem__(self, arg0: int, arg1: VCChange) -> None: 
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: VCChangeList) -> None: ...
    def append(self, x: VCChange) -> None: 
        """
        Add an item to the end of the list
        """
    def clear(self) -> None: 
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: VCChangeList) -> None: 
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...
    def insert(self, i: int, x: VCChange) -> None: 
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> VCChange: 
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> VCChange: ...
    pass
class VCConflictHandling():
    """
    Members:

      TakeTheirs

      TakeOurs

      External

      Cancel
    """
    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    Cancel: anchorpoint.VCConflictHandling # value = <VCConflictHandling.Cancel: 3>
    External: anchorpoint.VCConflictHandling # value = <VCConflictHandling.External: 2>
    TakeOurs: anchorpoint.VCConflictHandling # value = <VCConflictHandling.TakeOurs: 0>
    TakeTheirs: anchorpoint.VCConflictHandling # value = <VCConflictHandling.TakeTheirs: 1>
    __members__: dict # value = {'TakeTheirs': <VCConflictHandling.TakeTheirs: 1>, 'TakeOurs': <VCConflictHandling.TakeOurs: 0>, 'External': <VCConflictHandling.External: 2>, 'Cancel': <VCConflictHandling.Cancel: 3>}
    pass
class VCFileStatus():
    """
    Members:

      Unknown

      New

      Deleted

      Modified

      Renamed

      Conflicted
    """
    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    Conflicted: anchorpoint.VCFileStatus # value = <VCFileStatus.Conflicted: 5>
    Deleted: anchorpoint.VCFileStatus # value = <VCFileStatus.Deleted: 2>
    Modified: anchorpoint.VCFileStatus # value = <VCFileStatus.Modified: 3>
    New: anchorpoint.VCFileStatus # value = <VCFileStatus.New: 1>
    Renamed: anchorpoint.VCFileStatus # value = <VCFileStatus.Renamed: 4>
    Unknown: anchorpoint.VCFileStatus # value = <VCFileStatus.Unknown: 0>
    __members__: dict # value = {'Unknown': <VCFileStatus.Unknown: 0>, 'New': <VCFileStatus.New: 1>, 'Deleted': <VCFileStatus.Deleted: 2>, 'Modified': <VCFileStatus.Modified: 3>, 'Renamed': <VCFileStatus.Renamed: 4>, 'Conflicted': <VCFileStatus.Conflicted: 5>}
    pass
class VCGetChangesInfo():
    def __init__(self) -> None: ...
    @property
    def modified_content(self) -> str:
        """
        :type: str
        """
    @modified_content.setter
    def modified_content(self, arg0: str) -> None:
        pass
    @property
    def modified_filepath(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @modified_filepath.setter
    def modified_filepath(self, arg0: typing.Optional[str]) -> None:
        pass
    @property
    def original_content(self) -> str:
        """
        :type: str
        """
    @original_content.setter
    def original_content(self, arg0: str) -> None:
        pass
    @property
    def original_filepath(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @original_filepath.setter
    def original_filepath(self, arg0: typing.Optional[str]) -> None:
        pass
    pass
class VCPendingChange(VCChange):
    def __init__(self) -> None: ...
    @property
    def selected(self) -> typing.Optional[bool]:
        """
        :type: typing.Optional[bool]
        """
    @selected.setter
    def selected(self, arg0: typing.Optional[bool]) -> None:
        pass
    pass
class VCPendingChangeList():
    def __bool__(self) -> bool: 
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None: 
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...
    @typing.overload
    def __getitem__(self, s: slice) -> VCPendingChangeList: 
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> VCPendingChange: ...
    @typing.overload
    def __init__(self) -> None: 
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: VCPendingChangeList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...
    @typing.overload
    def __setitem__(self, arg0: int, arg1: VCPendingChange) -> None: 
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: VCPendingChangeList) -> None: ...
    def append(self, x: VCPendingChange) -> None: 
        """
        Add an item to the end of the list
        """
    def clear(self) -> None: 
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: VCPendingChangeList) -> None: 
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...
    def insert(self, i: int, x: VCPendingChange) -> None: 
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> VCPendingChange: 
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> VCPendingChange: ...
    pass
class VCPendingChangesInfo():
    def __init__(self) -> None: ...
    @property
    def actions(self) -> TimelineChannelActionList:
        """
        :type: TimelineChannelActionList
        """
    @actions.setter
    def actions(self, arg0: TimelineChannelActionList) -> None:
        pass
    @property
    def caption(self) -> str:
        """
        :type: str
        """
    @caption.setter
    def caption(self, arg0: str) -> None:
        pass
    @property
    def changes(self) -> VCPendingChangeList:
        """
        :type: VCPendingChangeList
        """
    @changes.setter
    def changes(self, arg0: VCPendingChangeList) -> None:
        pass
    @property
    def entry_actions(self) -> TimelineChannelActionList:
        """
        :type: TimelineChannelActionList
        """
    @entry_actions.setter
    def entry_actions(self, arg0: TimelineChannelActionList) -> None:
        pass
    pass
def check_application(application_path: str, info: str = '', name: typing.Optional[str] = None) -> bool:
    """
    Verifies if the provided application can be found on the system. Returns True if the 
    application is found, shows a toast to the user if not. Set info='my info' to guide 
    the user how to fix the problem. Pass name=\"my executable\" to check the expected name 
    of the application.

    Args:
        application_path (str): The path to the application
        info (Optional[str]): Description how the user can fix the problem
        name (str): The expected name of the application

    Example:
        >>> if anchorpoint.check_application("path/to/blender.exe", "Please install blender from blender.org", "blender"):
        >>>     print ("Blender found, starting to render...")
    """
def close_create_project_dialog() -> None:
    """
    Closes the current open project dialog in Anchorpoint
    """
def close_timeline_sidebar() -> None:
    pass
def copy_files_to_clipboard(file_paths: typing.List[str]) -> None:
    """
    Copies given file paths to clipboard marked for copy operation
    Args:
        file_paths ([str]): file paths to the files to copy

    Example:
        >>> anchorpoint.copy_files_to_clipboard(["path/to/file.png"]):
    """
def create_action(yaml_path: str, action: Action) -> None:
    """
    Creates a new action
    """
def create_app_link(target: typing.Union[str, apsync.Task]) -> str:
    """
    Creates an app link for a folder, file, or task
    """
def delete_timeline_channel_entries(channel_id: str, entry_ids: typing.List[str]) -> None:
    pass
def enable_timeline_channel_action(channel_id: str, action_id: str, enable: bool = True) -> None:
    pass
def evaluate_locks(workspace_id: str, project_id: str) -> None:
    """
    Evualate Locks (e.g. make readonly if necessary)
    """
def get_api() -> apsync.Api:
    """
    Returns the active api object for the current context.

    Example:
        >>> api = anchorpoint.get_api()
    """
def get_application_dir() -> str:
    """
    Returns the application directory of Anchorpoint
    """
def get_config() -> APConfig:
    """
    Returns the anchorpoint config.

    Example:
        >>> config = anchorpoint.get_config()
    """
def get_context() -> Context:
    """
    Returns the active anchorpoint context object.

    Example:
        >>> ctx = anchorpoint.get_context()
    """
def get_locks(workspace_id: str, project_id: str) -> typing.List[apsync.Lock]:
    """
    Returns all locks for a given project

    Example:
        >>> anchorpoint.get_locks(ctx.workspace_id, ctx.project_id)
    """
def get_timeline_update_count() -> int:
    pass
def join_project_path(path: str, project_id: str, workspace_id: str) -> None:
    """
    Connects the path to the project. This is what happens when you do "Connect Project Files" in Anchorpoint.

    Args:
        path (str): The path to the project
        project_id (str): The id of the project
        workspace_id (str): The workspace id
    """
def lock(workspace_id: str, project_id: str, targets: typing.List[str], description: typing.Optional[str] = None, metadata: typing.Dict[str, str] = {}) -> None:
    """
    Locks a list of files

    Example:
        >>> files = ["C:/Project/asset.blend"]
        >>> anchorpoint.lock(ctx.workspace_id, ctx.project_id, files)
    """
def log_error(arg0: str) -> None:
    pass
def open_integration_preferences(integration_name: str) -> None:
    """
    Open the integration settings and scroll to specific integration
    """
def open_timeline() -> None:
    pass
def refresh_timeline_channel(channel_id: str) -> None:
    pass
def reload_create_project_dialog() -> None:
    """
    Reloads the create project dialog. Does nothing when dialog is not open
    """
def reload_timeline_entries() -> None:
    pass
def set_timeline_update_count(project_id: str, channel_id: str, count: int, since: int = 0) -> None:
    pass
def show_create_project_dialog(path: typing.Optional[str] = None, project_id: typing.Optional[str] = None, remote_url: typing.Optional[str] = None, tags: typing.List[str] = []) -> None:
    """
    Opens the create project dialog in Anchorpoint
    """
def stop_timeline_channel_action_processing(channel_id: str, action_id: str) -> None:
    pass
def temp_dir() -> str:
    """
    Returns a writable temporary directory
    """
def timeline_channel_action_processing(channel_id: str, action_id: str, message: typing.Optional[str] = None) -> None:
    pass
def unlock(workspace_id: str, project_id: str, targets: typing.List[str], force: bool = False) -> None:
    """
    Unlocks a list of files

    Example:
        >>> files = ["C:/Project/asset.blend"]
        >>> anchorpoint.unlock(ctx.workspace_id, ctx.project_id, files)
    """
def update_locks(workspace_id: str, project_id: str, locks: typing.List[apsync.Lock]) -> None:
    """
    Updates Locks (e.g. description, metadata)
    """
def update_timeline_channel_entries(channel_id: str, entries: typing.List[TimelineChannelEntry]) -> None:
    pass
def update_timeline_last_seen() -> None:
    pass
def vc_load_pending_changes(channel_id: str, reload: bool = False) -> None:
    pass
def vc_resolve_conflicts(channel_id: str) -> None:
    pass

