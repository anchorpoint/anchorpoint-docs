from __future__ import annotations
import apsync
import typing

__all__ = [
    "AccessLevel",
    "Api",
    "ApiVersion",
    "Attribute",
    "AttributeTag",
    "AttributeTagList",
    "AttributeTimestamp",
    "AttributeType",
    "Attributes",
    "DatabaseType",
    "GroupMember",
    "GroupMemberList",
    "Icon",
    "InternalApi",
    "IpcMessage",
    "Lock",
    "ObjectType",
    "Project",
    "Settings",
    "SharedSettings",
    "SyncError",
    "TagColor",
    "Task",
    "TaskList",
    "Tasks",
    "TemplateType",
    "TimelineChannel",
    "UserProfile",
    "VariableIdentifier",
    "WorkspaceMember",
    "WorkspaceMemberList",
    "add_attribute_tag",
    "add_attribute_tags",
    "add_timeline_channel",
    "add_timeline_entry",
    "add_user_to_project",
    "attach_thumbnail",
    "attach_thumbnails",
    "comment_file",
    "configure_daemon",
    "copy_file",
    "copy_file_from_template",
    "copy_folder",
    "copy_from_template",
    "create_next_version",
    "create_project",
    "create_project_file",
    "create_template",
    "export_project",
    "generate_thumbnail",
    "generate_thumbnails",
    "get_api",
    "get_api_version",
    "get_attribute_checked",
    "get_attribute_date",
    "get_attribute_link",
    "get_attribute_rating",
    "get_attribute_tag",
    "get_attribute_tags",
    "get_attribute_text",
    "get_client_name",
    "get_daemon_address",
    "get_file_by_id",
    "get_file_id",
    "get_folder_by_id",
    "get_folder_id",
    "get_internal_api",
    "get_next_version_path",
    "get_project",
    "get_project_by_id",
    "get_project_members",
    "get_projects",
    "get_server_url",
    "get_server_version",
    "get_thumbnail",
    "get_timeline_channel",
    "get_timeline_channels",
    "get_workspace_access",
    "get_workspace_members",
    "import_local",
    "import_project",
    "ipc_get_message",
    "ipc_has_messages",
    "ipc_publish",
    "ipc_unsubscribe",
    "is_project",
    "remove_attribute_tag",
    "remove_project",
    "remove_timeline_channel",
    "remove_user_from_project",
    "remove_user_from_workspace",
    "rename_file",
    "rename_folder",
    "resolve_variables",
    "set_account_email",
    "set_attribute_checked",
    "set_attribute_date",
    "set_attribute_link",
    "set_attribute_rating",
    "set_attribute_tag",
    "set_attribute_tags",
    "set_attribute_text",
    "set_client_name",
    "set_daemon_address",
    "set_folder_icon",
    "set_server_url",
    "toggle_version_control",
    "update_timeline_channel"
]


class AccessLevel():
    """
    Members:

      Owner

      Admin

      Member

      Guest

      NoAccess
    """

    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...

    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    Admin: apsync.AccessLevel  # value = <AccessLevel.Admin: 1>
    Guest: apsync.AccessLevel  # value = <AccessLevel.Guest: 3>
    Member: apsync.AccessLevel  # value = <AccessLevel.Member: 2>
    NoAccess: apsync.AccessLevel  # value = <AccessLevel.NoAccess: 4>
    Owner: apsync.AccessLevel  # value = <AccessLevel.Owner: 0>
    # value = {'Owner': <AccessLevel.Owner: 0>, 'Admin': <AccessLevel.Admin: 1>, 'Member': <AccessLevel.Member: 2>, 'Guest': <AccessLevel.Guest: 3>, 'NoAccess': <AccessLevel.NoAccess: 4>}
    __members__: dict
    pass


class Api():
    """
    The Anchorpoint Sync Api is your swiss army knife to communicate with the Anchorpoint Server. It is required to do all kinds of neat things, such as:

    - read and write attributes

    - get information about projects

    - read and write settings

    - and much more

    Example:                
        >>> import apsync
        >>> api = apsync.get_api()
    """

    def get_project(self) -> typing.Optional[Project]:
        """
        Returns the active project for the api.
        """

    def get_workspace(self) -> str:
        """
        Returns the active workspace id.
        """

    def set_project(self, project: typing.Optional[Project]) -> None:
        """
        Sets the active project for the api.
        """

    def set_workspace(self, workspace_id: str) -> None:
        """
        Sets the active workspace id.
        """
    @property
    def attributes(self) -> Attributes:
        """
        Access the :class:`~apsync.Attributes` api

        :type: Attributes
        """
    @property
    def tasks(self) -> Tasks:
        """
        Access the :class:`~apsync.Tasks` api

        :type: Tasks
        """
    pass


class ApiVersion():
    """
    The ApiVersion identifies the current version of the anchorpoint and apsync API version.
    Use the ApiVersion object to check for API compatibility, for example.

    Examples:
        Get the current version of the Api
            >>> apsync.get_api_version()
            >>> 1.0.0

        Check what version you are running
            >>> if get_api_version() >= apsync.ApiVersion("1.0.2"):
            >>>     print("We are running Anchorpoint 1.0.2 or greater")
    """

    def __eq__(self, arg0: ApiVersion) -> bool: ...
    def __ge__(self, arg0: ApiVersion) -> bool: ...
    def __gt__(self, arg0: ApiVersion) -> bool: ...
    @typing.overload
    def __init__(self, major: int, minor: int, patch: int) -> None: ...
    @typing.overload
    def __init__(self, version: str) -> None: ...
    def __le__(self, arg0: ApiVersion) -> bool: ...
    def __lt__(self, arg0: ApiVersion) -> bool: ...
    def __ne__(self, arg0: ApiVersion) -> bool: ...
    def __repr__(self) -> str: ...
    __hash__: typing.ClassVar[None] = None
    pass


class Attribute():
    def __init__(self) -> None: ...

    @property
    def id(self) -> str:
        """
        :type: str
        """
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg0: str) -> None:
        pass

    @property
    def rating_max(self) -> typing.Optional[int]:
        """
        :type: typing.Optional[int]
        """
    @rating_max.setter
    def rating_max(self, arg0: typing.Optional[int]) -> None:
        pass

    @property
    def tags(self) -> AttributeTagList:
        """
        :type: AttributeTagList
        """
    @tags.setter
    def tags(self, arg0: AttributeTagList) -> None:
        pass

    @property
    def type(self) -> AttributeType:
        """
        :type: AttributeType
        """
    pass


class AttributeTag():
    """
    The AttributeTag class represents a single or multiple choice tag.

    Attributes:
        id (str): The unique id of the attribute
        name (str): The name of the attribute
        color (:class:`~apsync.TagColor`): The color of the tag

    Example:
        >>> apsync.AttributeTag("Work in Progress", "yellow")
    """
    @typing.overload
    def __init__(self, name: str) -> None: ...
    @typing.overload
    def __init__(self, name: str, color: TagColor) -> None: ...
    @typing.overload
    def __init__(self, name: str, color: str) -> None: ...
    def __repr__(self) -> str: ...

    @property
    def color(self) -> TagColor:
        """
        :type: TagColor
        """
    @color.setter
    def color(self, arg0: TagColor) -> None:
        pass

    @property
    def id(self) -> str:
        """
        :type: str
        """
    @id.setter
    def id(self, arg0: str) -> None:
        pass

    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg0: str) -> None:
        pass
    pass


class AttributeTagList():
    def __bool__(self) -> bool:
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None:
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...

    @typing.overload
    def __getitem__(self, s: slice) -> AttributeTagList:
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> AttributeTag: ...

    @typing.overload
    def __init__(self) -> None:
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: AttributeTagList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...

    @typing.overload
    def __setitem__(self, arg0: int, arg1: AttributeTag) -> None:
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: AttributeTagList) -> None: ...

    def append(self, x: AttributeTag) -> None:
        """
        Add an item to the end of the list
        """

    def clear(self) -> None:
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: AttributeTagList) -> None:
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...

    def insert(self, i: int, x: AttributeTag) -> None:
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> AttributeTag:
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> AttributeTag: ...
    pass


class AttributeTimestamp():
    def __init__(self) -> None: ...
    def __repr__(self) -> str: ...

    @property
    def comment(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @comment.setter
    def comment(self, arg0: typing.Optional[str]) -> None:
        pass

    @property
    def date(self) -> int:
        """
        :type: int
        """
    @date.setter
    def date(self, arg0: int) -> None:
        pass

    @property
    def edited(self) -> bool:
        """
        :type: bool
        """
    @edited.setter
    def edited(self, arg0: bool) -> None:
        pass

    @property
    def seconds(self) -> int:
        """
        :type: int
        """
    @seconds.setter
    def seconds(self, arg0: int) -> None:
        pass

    @property
    def user_id(self) -> str:
        """
        :type: str
        """
    @user_id.setter
    def user_id(self, arg0: str) -> None:
        pass
    pass


class AttributeType():
    """
    The AttributeType class is used to identify the type of an attribute.

    Attributes:
        single_choice_tag (apsync.AttributeType): Represents a single choice tag attribute
        multiple_choice_tag (apsync.AttributeType): Represents a multiple choice tag attribute
        text (apsync.AttributeType): Represents a text
        user (apsync.user): Represents a user
        rating (apsync.AttributeType): Represents a rating
        hyperlink (apsync.AttributeType): Represents a hyperlink
        date (apsync.AttributeType): Represents a date
        checkbox (apsync.AttributeType): Represents a checkbox
        timestamp (apsync.AttributeType): Represents a timestamp / time tracking attribute

    Example:
        >>> type = apsync.AttributeType.rating
    """

    def __init__(self, type: str) -> None: ...
    def __repr__(self) -> str: ...
    # value = apsync.AttributeType(type='Checkbox')
    checkbox: apsync.AttributeType
    date: apsync.AttributeType  # value = apsync.AttributeType(type='Date')
    # value = apsync.AttributeType(type='Hyperlink')
    hyperlink: apsync.AttributeType
    # value = apsync.AttributeType(type='MultipleChoiceTag')
    multiple_choice_tag: apsync.AttributeType
    rating: apsync.AttributeType  # value = apsync.AttributeType(type='Rating')
    # value = apsync.AttributeType(type='SingleChoiceTag')
    single_choice_tag: apsync.AttributeType
    text: apsync.AttributeType  # value = apsync.AttributeType(type='Text')
    # value = apsync.AttributeType(type='Timestamp')
    timestamp: apsync.AttributeType
    user: apsync.AttributeType  # value = apsync.AttributeType(type='User')
    pass


class Attributes():
    def create_attribute(self, name: str, type: AttributeType, tags: typing.Optional[typing.Union[AttributeTagList, list]] = None, rating_max: typing.Optional[int] = None) -> Attribute:
        """
        Creates a new attribute in the workspace or project

        Args:
            name: str: The name of the attribute (e.g. Status)
            type (:class:`~apsync.AttributeType`): The attribute type
            tags (list[:class:`~apsync.AttributeTag`]): The list of tags to create - or None
            rating_max: The maximum rating, or None. Only valid for rating attributes

        Example:
            >>> tags = [apsync.AttributeTag("Work in Progress", apsync.TagColor.yellow)]
            >>> api.attributes.create_attribute("Status", apsync.AttributeType.single_choice_tag, tags=tags)
        """

    def get_attribute(self, name: str, type: typing.Optional[AttributeType] = None) -> typing.Optional[Attribute]:
        """
        Returns the attribute or None

        Args:
            name: str: The name of the attribute (e.g. Status)
            type (:class:`~apsync.AttributeType`): The attribute type to filter for or None

        Example:
            >>> attribute = api.attributes.get_attribute("Status", apsync.AttributeType.single_choice_tag)
        """

    def get_attribute_by_id(self, id: str) -> Attribute:
        """
        Returns the attribute with a given id.

        Args:
            id (str): The id of the attribute

        Example:
            >>> attribute = api.attributes.get_attribute_by_id(some_id)
        """

    def get_attribute_value(self, target: typing.Union[Task, str], attribute: typing.Union[Attribute, str]) -> object:
        """
        Retrieves the content of an attribute for a given file or folder.

        Args:
            target (str | :class:`~apsync.Task`): The target of the attribute (File, Folder, or Task)
            attribute (str | :class:`~apsync.Attribute`): The attribute (title or class)

        Returns:
            The attribute value, or None.

        Example:
            >>> value = apsync.get_attribute_value("image.png", "status")
        """

    def get_attributes(self, type: typing.Optional[AttributeType] = None) -> typing.List[Attribute]:
        """
        Returns all attributes, possibly filtered with a type.

        Args:
            type (:class:`~apsync.AttributeType`): The attribute type to filter for or None

        Example:
            >>> attributes = api.attributes.get_attributes()
        """

    def rename_attribute(self, attribute: Attribute, name: str) -> None:
        """
        Renames the attribute

        Args:
            attribute (:class:`~apsync.Attribute`): The attribute to update
            name (str): The new name

        Example:
            >>> attribute = api.attributes.get_attribute("Status")
            >>> api.attributes.rename_attribute(attribute, "State")
        """

    def set_attribute_rating_max(self, attribute: Attribute, max: int) -> None:
        """
        Sets the maximum rating for a rating attribute

        Args:
            attribute (:class:`~apsync.Attribute`): The attribute to update
            max (int): The new maximum

        Example:
            >>> attribute = api.attributes.get_attribute("Quality")
            >>> api.attributes.set_attribute_rating_max(attribute, 10)
        """

    def set_attribute_tags(self, attribute: Attribute, tags: AttributeTagList) -> None:
        """
        Sets the available tags for a single or multiple choice tag attribute

        Args:
            attribute (:class:`~apsync.Attribute`): The attribute to update
            tags (list[:class:`~apsync.AttributeTag`]): The list of tags to create

        Example:
            >>> attribute = api.attributes.get_attribute("Status")
            >>> tags = [apsync.AttributeTag("Done", "green")]
            >>> api.attributes.set_attribute_tags(attribute, tags)
        """

    def set_attribute_value(self, target: typing.Union[Task, str], attribute: typing.Union[Attribute, str], value: typing.Union[int, str, typing.List[str], AttributeTag, AttributeTagList, object, bool], update_timeline: bool = False) -> None:
        """
        Sets the value of an attribute for a given file or folder.
        Creates the attribute if it cannot be found.
        See examples on GitHub: https://github.com/Anchorpoint-Software/ap-actions/tree/main/examples/attributes

        Args:
            target (str | :class:`~apsync.Task`): Path to the file or folder
            attribute (str | :class:`~apsync.Attribute`): The attribute title or object
            value (Any): The value to set
            update_timeline (bool): True if the timeline should be notified about the update

        Examples:
            >>> api.attributes.set_attribute_value("asset.blend", "notes", "polygon count is too high")
            >>> api.attributes.set_attribute_value("asset.blend", "artist", "support@anchorpoint.app")
        """
    pass


class DatabaseType():
    def __init__(self, type: str) -> None: ...
    def __repr__(self) -> str: ...
    file: apsync.DatabaseType  # value = apsync.DatabaseType(type='file')
    folder: apsync.DatabaseType  # value = apsync.DatabaseType(type='folder')
    pass


class GroupMember():
    """
    A member of an anchorpoint workspace.
    """
    @property
    def access_level(self) -> AccessLevel:
        """
        :type: AccessLevel
        """
    @property
    def email(self) -> str:
        """
        :type: str
        """
    @property
    def id(self) -> str:
        """
        :type: str
        """
    @property
    def profile(self) -> UserProfile:
        """
        :type: UserProfile
        """
    pass


class GroupMemberList():
    def __bool__(self) -> bool:
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None:
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...

    @typing.overload
    def __getitem__(self, s: slice) -> GroupMemberList:
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> GroupMember: ...

    @typing.overload
    def __init__(self) -> None:
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: GroupMemberList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...

    @typing.overload
    def __setitem__(self, arg0: int, arg1: GroupMember) -> None:
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: GroupMemberList) -> None: ...

    def append(self, x: GroupMember) -> None:
        """
        Add an item to the end of the list
        """

    def clear(self) -> None:
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: GroupMemberList) -> None:
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...

    def insert(self, i: int, x: GroupMember) -> None:
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> GroupMember:
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> GroupMember: ...
    pass


class Icon():
    """
    The icon of a folder or task in the Anchorpoint UI. To get the path to a built-in icon right click an icon in the icon picker menu.

    Example:
        >>> icon = apsync.Icon("qrc:/icons/multimedia/old video camera.svg", "green")
    """
    @typing.overload
    def __init__(self) -> None: ...
    @typing.overload
    def __init__(self, icon_path: str) -> None: ...
    @typing.overload
    def __init__(self, icon_path: str, color: str) -> None: ...
    def __repr__(self) -> str: ...

    @property
    def color(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @color.setter
    def color(self, arg0: typing.Optional[str]) -> None:
        pass

    @property
    def path(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @path.setter
    def path(self, arg1: typing.Optional[str]) -> None:
        pass
    pass


class InternalApi():
    pass


class IpcMessage():
    def __init__(self) -> None: ...

    @property
    def body(self) -> str:
        """
        :type: str
        """
    @body.setter
    def body(self, arg1: str) -> None:
        pass

    @property
    def header(self) -> typing.Dict[str, str]:
        """
        :type: typing.Dict[str, str]
        """
    @header.setter
    def header(self, arg1: typing.Dict[str, str]) -> None:
        pass

    @property
    def kind(self) -> str:
        """
        :type: str
        """
    @kind.setter
    def kind(self, arg1: str) -> None:
        pass

    @property
    def sender(self) -> str:
        """
        :type: str
        """
    @property
    def topic(self) -> str:
        """
        :type: str
        """
    @topic.setter
    def topic(self, arg1: str) -> None:
        pass
    pass


class Lock():
    def __init__(self) -> None: ...

    @property
    def description(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @description.setter
    def description(self, arg1: typing.Optional[str]) -> None:
        pass

    @property
    def metadata(self) -> typing.Dict[str, str]:
        """
        :type: typing.Dict[str, str]
        """
    @metadata.setter
    def metadata(self, arg1: typing.Dict[str, str]) -> None:
        pass

    @property
    def owner_id(self) -> str:
        """
        :type: str
        """
    @property
    def path(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def project_id(self) -> str:
        """
        :type: str
        """
    @property
    def type(self) -> ObjectType:
        """
        :type: ObjectType
        """
    @property
    def workspace_id(self) -> str:
        """
        :type: str
        """
    pass


class ObjectType():
    """
    Members:

      File

      Folder

      Task
    """

    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...

    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    File: apsync.ObjectType  # value = <ObjectType.File: 0>
    Folder: apsync.ObjectType  # value = <ObjectType.Folder: 1>
    Task: apsync.ObjectType  # value = <ObjectType.Task: 2>
    # value = {'File': <ObjectType.File: 0>, 'Folder': <ObjectType.Folder: 1>, 'Task': <ObjectType.Task: 2>}
    __members__: dict
    pass


class Project():
    """
    The Project identifies an anchorpoint project. It provides the name and path of the project plus an optional description.
    You can provide extra metadata as a dictionary of strings.
    Create a project through the :func:`~anchorpoint.Context.create_project` function.

    Attributes:
        id (str): The unique id of the project
        workspace_id (str): The unique id of the workspace
        group_id (str): The unique id of the user group
        path (str): The path of the project root - or None.
        name (str): The name of the project
        desc (str): The description of the project

    Returns:
        The active project - or None

    Example:                
        >>> project = apsync.get_project("image.png")
        >>> print (project.name)
    """

    def __init__(self) -> None: ...
    def __repr__(self) -> str: ...

    def get_metadata(self) -> typing.Dict[str, str]:
        """
        Gets the additional metadata that is stored on the project in form of a dict[str,str]

        Example:                
            >>> project = apsync.get_project("C:/Projects/MyProject", "My Project")
            >>> project.get_metadata()
            >>> {'Aspect Ratio': '16:9', 'Client Name': 'Anchorpoint'}
        """

    def update_metadata(self, arg0: typing.Dict[str, str]) -> None:
        """
        Updates the additional metadata that is stored on the project in form of a dict[str,str]

        Example:                
            >>> project = apsync.get_project("C:/Projects/MyProject", "My Project")
            >>> metadata = project.get_metadata()
            >>> metadata["Client Name"] = "Anchorpoint"
            >>> project.update_metadata(metadata)
        """
    @property
    def archived(self) -> bool:
        """
        :type: bool
        """
    @property
    def description(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def group_id(self) -> str:
        """
        :type: str
        """
    @property
    def id(self) -> str:
        """
        :type: str
        """
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def path(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def workspace_id(self) -> str:
        """
        :type: str
        """
    pass


class Settings():
    """
    Settings store important and useful information for the user in a local database. They are not uploaded to the cloud and thus remain private.

    Note: Currently, Settings are not encrypted.
    """

    def __init__(self, name: typing.Optional[str] = None, identifier: typing.Optional[str] = None, location: typing.Optional[str] = None, user: bool = True) -> None:
        """
        By constructing a Settings object, one can choose how settings will be stored and how they will be loaded from disk.
        Settings can optionally be identified by a name. By providing a name to the class, settings will stored under given name. Hence, you can retrieve the same set of settings in all actions by passing the same name.
        Additionally, you can pass an identifier to the settings class. The identifier will scope the settings. This means you can pass the project ID as an identifier to make the set of settings unique to this project only.
        By default, all settings will be scoped per user account.
        You can overwrite the location where the settings are stored on disk.

        Args:
            name (str): Optionally pass a name to identify the settings in another action
            identifier (str): Optionally pass an identifier to scope the settings to a project, for example.
            location (str): Optionally pass a location where to store the settings.
            user (bool): Set to True to make settings available for all users.

        Example:
            >>> user_settings = apsync.Settings()
            >>> named_settings = apsync.Settings("Blender Action Settings")
        """

    def clear(self) -> None:
        """
        Clears all settings - it's like a factory reset for the settings object.

        Example:
            >>> settings = apsync.Settings()
            >>> settings.clear()
            >>> settings.store()
        """

    def contains(self, key: str) -> bool:
        """
        Checks if the settings contain a value identified by a name 'key'

        Args:
            key (str): The name of the settings value

        Example:
            >>> settings = apsync.Settings()
            >>> settings.contains("lottery numbers")
            >>> True
        """

    def get(self, key: str, default: object = '') -> object:
        """
        Returns the stored value identified by a key. Returns the default value if the key is not found.

        Args:
            key (str): The name of the settings value
            default: The default value

        Returns:
            The stored settings value or the provided default if the value is not found.

        Example:
            >>> settings = apsync.Settings()
            >>> jackpot = settings.get("lottery numbers", 4815162342)
        """

    def remove(self, key: str) -> None:
        """
        Removes value from the settings, identified by a name 'key'

        Args:
            key (str): The name of the settings value

        Example:
            >>> settings = apsync.Settings()
            >>> settings.remove("lottery numbers")
            >>> settings.store()
        """

    def set(self, key: str, value: object) -> None:
        """
        Sets a new value, identified by a name 'key'

        Args:
            key (str): The name of the settings value
            value: The value to be stored

        Example:
            >>> settings = apsync.Settings()
            >>> settings.set("lottery numbers", 4815162342)
            >>> settings.store()
        """

    def store(self) -> None:
        """
        After modifying the settings class you must call 'store' to persist your changes.

        Example:
            >>> settings = apsync.Settings()
            >>> settings.clear()
            >>> settings.store()
        """
    pass


class SharedSettings():
    """
    SharedSettings store important and useful information for the entire workspace for all users, they are uploaded to the cloud.
    Note: Currently, SharedSettings are not encrypted.
    """
    @typing.overload
    def __init__(self, workspace_id: str, identifier: str) -> None:
        """
        A SharedSettings object works the same as the :class:`~apsync.Settings` object.
        SharedSettings are stored in the cloud per workspace, hence they are shared with all users.

        Args:
            workspace_id (str): The workspace_id where the SharedSettings object lives
            identifier (str): A unique identifier of the SharedSettings in the workspace

        Example:
            >>> project = apsync.get_project(path)
            >>> if project:
            >>>     shared_settings = apsync.Settings(project.workspace_id, "Blender Action Settings")    



        A SharedSettings object works the same as the :class:`~apsync.Settings` object.
        Project based SharedSettings are stored in the cloud per project, hence they are shared with all users that have access to the project.

        Args:
            project_id (str): The project_id where the SharedSettings object lives
            workspace_id (str): The workspace_id where the SharedSettings object lives
            identifier (str): A unique identifier of the SharedSettings in the project

        Example:
            >>> project = apsync.get_project(path)
            >>> if project:
            >>>     project_shared_settings = apsync.Settings(project.project_id, project.workspace_id, "Blender Action Settings")
        """
    @typing.overload
    def __init__(self, project_id: str, workspace_id: str,
                 identifier: str) -> None: ...

    def clear(self) -> None:
        """
        Clears all settings - it's like a factory reset for the settings object.

        Example:
            >>> settings.clear()
            >>> settings.store()
        """

    def contains(self, key: str) -> bool:
        """
        Checks if the settings contain a value identified by a name 'key'

        Args:
            key (str): The name of the settings value

        Example:
            >>> settings.contains("lottery numbers")
            >>> True
        """

    def get(self, key: str, default: object = '') -> object:
        """
        Returns the stored value identified by a key. Returns the default value if the key is not found.

        Args:
            key (str): The name of the settings value
            default: The default value

        Returns:
            The stored settings value or the provided default if the value is not found.

        Example:
            >>> jackpot = settings.get("lottery numbers", 4815162342)
        """

    def remove(self, key: str) -> None:
        """
        Removes value from the settings, identified by a name 'key'

        Args:
            key (str): The name of the settings value

        Example:
            >>> settings.remove("lottery numbers")
            >>> settings.store()
        """

    def set(self, key: str, value: object) -> None:
        """
        Sets a new value, identified by a name 'key'

        Args:
            key (str): The name of the settings value
            value: The value to be stored

        Example:
            >>> settings.set("lottery numbers", 4815162342)
            >>> settings.store()
        """

    def store(self) -> None:
        """
        After modifying the settings class you must call 'store' to persist your changes.

        Example:
            >>> settings.clear()
            >>> settings.store()
        """
    pass


class SyncError(Exception, BaseException):
    pass


class TagColor():
    """
    The TagColor class represents the color of a single or multiple choice tag.
    It can be initialized with a string value but it is recommended to use the provided properties.

    Attributes:
        red (TagColor): A red TagColor
        orange (TagColor): An orange TagColor
        yellow (TagColor): A yellow TagColor
        green (TagColor): A green TagColor
        turk (TagColor): A turk TagColor
        blue (TagColor): A blue TagColor
        purple (TagColor): A purple TagColor
        grey (TagColor): A grey TagColor

    Example:
        >>> red = apsync.TagColor.red
    """
    @typing.overload
    def __init__(self) -> None: ...
    @typing.overload
    def __init__(self, color: str) -> None: ...
    def __repr__(self) -> str: ...
    blue: apsync.TagColor  # value = apsync.TagColor(color='blue')
    green: apsync.TagColor  # value = apsync.TagColor(color='green')
    grey: apsync.TagColor  # value = apsync.TagColor(color='grey')
    orange: apsync.TagColor  # value = apsync.TagColor(color='orange')
    purple: apsync.TagColor  # value = apsync.TagColor(color='purple')
    red: apsync.TagColor  # value = apsync.TagColor(color='red')
    turk: apsync.TagColor  # value = apsync.TagColor(color='turk')
    yellow: apsync.TagColor  # value = apsync.TagColor(color='yellow')
    pass


class Task():
    def __init__(self) -> None: ...

    @property
    def icon(self) -> typing.Optional[Icon]:
        """
        :type: typing.Optional[Icon]
        """
    @property
    def id(self) -> str:
        """
        :type: str
        """
    @property
    def list_id(self) -> str:
        """
        :type: str
        """
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def project_id(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def workspace_id(self) -> str:
        """
        :type: str
        """
    pass


class TaskList():
    def __init__(self) -> None: ...

    @property
    def id(self) -> str:
        """
        :type: str
        """
    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def project_id(self) -> typing.Optional[str]:
        """
        :type: typing.Optional[str]
        """
    @property
    def workspace_id(self) -> str:
        """
        :type: str
        """
    pass


class Tasks():
    def copy_task(self, task: Task) -> Task:
        """
        Copies a task

        Args:
            task (:class:`~apsync.Task`): The task to copy

        Example:
            >>> task = apsync.tasks.copy_task(task)
        """
    @typing.overload
    def create_task(self, task_list: TaskList, name: str) -> Task:
        """
        Creates a new task in a given task list

        Args:
            task_list (:class:`~apsync.TaskList`): The task list
            name (str): The name of the task

        Example:
            >>> task = apsync.tasks.create_task(task_list, "Create Rig")



        Creates a new task. Finds the task list by name and creates a new task list if it cannot find one

        Args:
            target (str): The folder used to search for task lists
            task_list_name (str): The name of the task list
            name (str): The name of the task

        Example:
            >>> task = apsync.tasks.create_task("D:/Assets", "Todos", "Create Rig")
        """
    @typing.overload
    def create_task(self, target: str, task_list_name: str,
                    name: str) -> Task: ...

    def create_task_list(self, target: str, name: str) -> TaskList:
        """
        Creates a new empty list of tasks

        Args:
            target (str): The folder where the task list will be created
            name (str): The name of the task list

        Example:
            >>> task_list = apsync.tasks.create_task_list("D:/Assets", "Todos")
        """

    def get_task(self, task_list: TaskList, name: str) -> typing.Optional[Task]:
        """
        Returns a task from a task list - or None

        Args:
            task_list (:class:`~apsync.TaskList`): The task list
            name (str): The name of the task

        Example:
            >>> task = apsync.tasks.get_task(todo_list, "Create Rig")
        """

    def get_task_by_id(self, id: str) -> Task:
        """
        Returns a task given its id

        Args:
            id (str): The id of the task

        Example:
            >>> task = apsync.tasks.task_id(task_id)
        """

    def get_task_list(self, target: str, name: str) -> typing.Optional[TaskList]:
        """
        Returns the task list or None

        Args:
            target (str): The folder used to search for task lists
            name (str): The new name of the task list

        Example:
            >>> todo_list = apsync.tasks.get_task_list("D:/Assets", "Todos")
        """

    def get_task_list_by_id(self, id: str) -> TaskList:
        """
        Returns the task list

        Args:
            id (str): The id of the task list

        Example:
            >>> todo_list = apsync.tasks.get_task_list_by_id(task_list_id)
        """

    def get_task_lists(self, target: str) -> typing.List[TaskList]:
        """
        Returns a list of all task lists in a given folder

        Args:
            target (str): The folder used to search for task lists

        Example:
            >>> all_lists = apsync.tasks.get_task_lists("D:/Assets")
        """

    def get_tasks(self, task_list: TaskList) -> typing.List[Task]:
        """
        Returns a list of all tasks of a task list

        Args:
            task_list (:class:`~apsync.TaskList`): The task list

        Example:
            >>> all_tasks = apsync.tasks.get_tasks(task_list)
        """

    def remove_task(self, task: Task) -> None:
        """
        Removes a task. Cannot be undone.

        Args:
            task (:class:`~apsync.Task`): The task to remove

        Example:
            >>> apsync.tasks.remove_task(task)
        """

    def remove_task_list(self, task_list: TaskList) -> None:
        """
        Removes a task list and deletes all tasks. Cannot be undone

        Args:
            task_list (:class:`~apsync.TaskList`): The task list to remove

        Example:
            >>> apsync.tasks.remove_task_list(task_list)
        """

    def rename_task(self, task: Task, name: str) -> None:
        """
        Renames a task

        Args:
            task (:class:`~apsync.Task`): The task to rename
            name (str): The new name of the task

        Example:
            >>> apsync.tasks.rename_task(task, "My Renamed Task")
        """

    def rename_task_list(self, task_list: TaskList, name: str) -> None:
        """
        Renames a task list

        Args:
            task_list (:class:`~apsync.TaskList`): The task list that is renamed
            name (str): The new name of the task list

        Example:
            >>> apsync.tasks.rename_task_list(task_list, "My Todos")
        """

    def set_task_icon(self, task: Task, icon: typing.Optional[Icon]) -> None:
        """
        Sets the :class:`~apsync.Icon` of a task. Set the icon to None to remove the icon

        Args:
            task (:class:`~apsync.Task`): The task
            icon (:class:`~apsync.Icon`): The icon to set

        Example:
            >>> apsync.tasks.set_task_icon(task, icon)
        """

    def set_task_icons(self, tasks: typing.List[Task], icon: typing.Optional[Icon]) -> None:
        """
        Sets the :class:`~apsync.Icon` for many tasks. Set the icon to None to remove the icon. This is faster than setting icons one by one

        Args:
            tasks (list[:class:`~apsync.Task`]): The list of tasks
            icon (:class:`~apsync.Icon`): The icon to set

        Example:
            >>> apsync.tasks.set_task_icons(tasks, icon)
        """
    pass


class TemplateType():
    """
    Members:

      File

      Folder

      Project
    """

    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...

    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    File: apsync.TemplateType  # value = <TemplateType.File: 2>
    Folder: apsync.TemplateType  # value = <TemplateType.Folder: 1>
    Project: apsync.TemplateType  # value = <TemplateType.Project: 0>
    # value = {'File': <TemplateType.File: 2>, 'Folder': <TemplateType.Folder: 1>, 'Project': <TemplateType.Project: 0>}
    __members__: dict
    pass


class TimelineChannel():
    def __init__(self) -> None: ...

    @property
    def icon(self) -> typing.Optional[Icon]:
        """
        :type: typing.Optional[Icon]
        """
    @icon.setter
    def icon(self, arg1: typing.Optional[Icon]) -> None:
        pass

    @property
    def id(self) -> str:
        """
        :type: str
        """
    @id.setter
    def id(self, arg1: str) -> None:
        pass

    @property
    def metadata(self) -> typing.Dict[str, str]:
        """
        :type: typing.Dict[str, str]
        """
    @metadata.setter
    def metadata(self, arg1: typing.Dict[str, str]) -> None:
        pass

    @property
    def name(self) -> str:
        """
        :type: str
        """
    @name.setter
    def name(self, arg1: str) -> None:
        pass
    pass


class UserProfile():
    """
    The user profile of an anchorpoint user
    """
    @property
    def firstname(self) -> str:
        """
        :type: str
        """
    @property
    def lastname(self) -> str:
        """
        :type: str
        """
    @property
    def nickname(self) -> str:
        """
        :type: str
        """
    @property
    def picture(self) -> str:
        """
        :type: str
        """
    pass


class VariableIdentifier():
    """
    Members:

      SquareBrackets

      CurlyBrackets
    """

    def __eq__(self, other: object) -> bool: ...
    def __getstate__(self) -> int: ...
    def __hash__(self) -> int: ...
    def __index__(self) -> int: ...
    def __init__(self, value: int) -> None: ...
    def __int__(self) -> int: ...
    def __ne__(self, other: object) -> bool: ...
    def __repr__(self) -> str: ...
    def __setstate__(self, state: int) -> None: ...

    @property
    def name(self) -> str:
        """
        :type: str
        """
    @property
    def value(self) -> int:
        """
        :type: int
        """
    CurlyBrackets: apsync.VariableIdentifier  # value = <VariableIdentifier.CurlyBrackets: 1>
    # value = <VariableIdentifier.SquareBrackets: 0>
    SquareBrackets: apsync.VariableIdentifier
    # value = {'SquareBrackets': <VariableIdentifier.SquareBrackets: 0>, 'CurlyBrackets': <VariableIdentifier.CurlyBrackets: 1>}
    __members__: dict
    pass


class WorkspaceMember():
    """
    A member of an anchorpoint workspace.
    """
    @property
    def access_level(self) -> AccessLevel:
        """
        :type: AccessLevel
        """
    @property
    def email(self) -> str:
        """
        :type: str
        """
    @property
    def id(self) -> str:
        """
        :type: str
        """
    @property
    def pending(self) -> bool:
        """
        :type: bool
        """
    @property
    def profile(self) -> UserProfile:
        """
        :type: UserProfile
        """
    pass


class WorkspaceMemberList():
    def __bool__(self) -> bool:
        """
        Check whether the list is nonempty
        """
    @typing.overload
    def __delitem__(self, arg0: int) -> None:
        """
        Delete the list elements at index ``i``

        Delete list elements using a slice object
        """
    @typing.overload
    def __delitem__(self, arg0: slice) -> None: ...

    @typing.overload
    def __getitem__(self, s: slice) -> WorkspaceMemberList:
        """
        Retrieve list elements using a slice object
        """
    @typing.overload
    def __getitem__(self, arg0: int) -> WorkspaceMember: ...

    @typing.overload
    def __init__(self) -> None:
        """
        Copy constructor
        """
    @typing.overload
    def __init__(self, arg0: WorkspaceMemberList) -> None: ...
    @typing.overload
    def __init__(self, arg0: typing.Iterable) -> None: ...
    def __iter__(self) -> typing.Iterator: ...
    def __len__(self) -> int: ...

    @typing.overload
    def __setitem__(self, arg0: int, arg1: WorkspaceMember) -> None:
        """
        Assign list elements using a slice object
        """
    @typing.overload
    def __setitem__(self, arg0: slice, arg1: WorkspaceMemberList) -> None: ...

    def append(self, x: WorkspaceMember) -> None:
        """
        Add an item to the end of the list
        """

    def clear(self) -> None:
        """
        Clear the contents
        """
    @typing.overload
    def extend(self, L: WorkspaceMemberList) -> None:
        """
        Extend the list by appending all the items in the given list

        Extend the list by appending all the items in the given list
        """
    @typing.overload
    def extend(self, L: typing.Iterable) -> None: ...

    def insert(self, i: int, x: WorkspaceMember) -> None:
        """
        Insert an item at a given position.
        """
    @typing.overload
    def pop(self) -> WorkspaceMember:
        """
        Remove and return the last item

        Remove and return the item at index ``i``
        """
    @typing.overload
    def pop(self, i: int) -> WorkspaceMember: ...
    pass


def add_attribute_tag(absolute_path: str, attribute_title: str, tag_name: str, type: AttributeType = AttributeType(type='MultipleChoiceTag'), workspace_id: typing.Optional[str] = None, auto_create: bool = True, tag_color: typing.Optional[TagColor] = None, update_timeline: bool = False) -> None:
    """
    Adds a tag identified by name to an attribute. If not tags are assigned, this function is equivalent to :func:`~apsync.set_attribute_tag`.
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        tag_name (str): The tag to add, identified by name
        type (apsync.AttributeType): The type of the attribute to create. Must be either apsync.AttributeType.multiple_choice_tag or apsync.AttributeType.single_choice_tag
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.add_attribute_tags("image.png", "clients", ["anchorpoint", "john doe"])
    """


def add_attribute_tags(absolute_path: str, attribute_title: str, tag_names: AttributeType, type: typing.List[str] = AttributeType(type='MultipleChoiceTag'), workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Adds a list of tags identified by name to an attribute. If not tags are assigned, this function is equivalent to :func:`~apsync.set_attribute_tags`.
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        tag_names (list): The tags to add, identified by name
        type (apsync.AttributeType): The type of the attribute to create. Must be either apsync.AttributeType.multiple_choice_tag or apsync.AttributeType.single_choice_tag
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.add_attribute_tags("image.png", "clients", ["anchorpoint", "john doe"])
    """


def add_timeline_channel(project: Project, channel: TimelineChannel) -> None:
    pass


def add_timeline_entry(path: str, text: str) -> None:
    """
    Toggles incremental version control for the folder that 'path' points to. Pass enable=True to enable version control, pass enable=False to disable it.

    Args:
        path (str): The path to the file or folder for that the timeline entry should be created
        text (str): The text that is displayed

    Examples:
        >>> apsync.add_timeline_entry("projects/acme/shots", "New Shot Created)
    """


def add_user_to_project(workspace_id: str, project_id: str, invited_from: str, email: str, access_level: AccessLevel) -> None:
    """
    Adds a user (identified by email) to the project. If the user is not yet a member of the workspace, he will be invited.

    Args:
        workspace_id (str): The workspace id
        project_id (str): The project id
        invited_from (str): Who invited the user to the project, e.g. a script. This will show up in the invitation email
        email (str): The email of the user to be added
        access_level (aps.AccessLevel): The access level the user gains for the project
    """


def attach_thumbnail(file_path: str, local_file: str, is_detail: bool = True) -> None:
    """
    Attaches the local_file as a thumbnail to file_path. Set is_detail = False to specifically set a an image for the Anchorpoint browser preview.
    Setting a downscaled version of the thumbnail for the preview is a nice optimization to improve browsing performance.
    Only use image formats such as PNG and JPEG.

    Args:
        file_path (str): Path to the file
        local_file (str): Path to the local image that should be set as the thumbnail
        is_detail (bool): Whether or not the image is for the browser preview

    Example:                
        >>> apsync.attach_thumbnail("my_scene.blend", "my_rendering.png")
    """


def attach_thumbnails(file_path: str, local_preview_file: str, local_detail_file: str) -> None:
    """
    Attach a preview and detail thumbnail to a file.
    """


def comment_file(file_path: str, comment: typing.Optional[str]) -> None:
    """
    Adds a comment by the current user to the file identified by file_path. 

    Args:
        file_path (str): The path to the file
        comment (Optional[str]): The comment to put on the file

    Example:
        >>> apsync.comment_file(file_path, "This is a nice file")
    """


def configure_daemon(arg0: str, arg1: str) -> None:
    """
    Private API. Use only for development.
    """


def copy_file(source: str, target: str, overwrite: bool = False, workspace_id: typing.Optional[str] = None) -> None:
    """
    Copies the file from source to target including all metadata (e.g. reviews).
    Raises an exeception when overwrite is False and target already exists.            

    Args:
        source (str): The source file
        target (str): The target file
        overwrite (bool): Overwrite the target file
        workspace_id (Optional[str]): Apply the changes in this workspace, or the user workspace if not set

    Example:                
        >>> apsync.copy_file(source, target, True)
    """


def copy_file_from_template(source: str, target: str, variables: typing.Dict[str, str] = {}, workspace_id: typing.Optional[str] = None) -> str:
    """
    Copies the source (template) folder to the target path and copies the metadata for the files (e.g. reviews).
    Pass a python dictionary of variables that are applied to the content of the folder.
    Raises an exeception when the target folder already exists, use os.remove to delete a folder.

    Args:
        source (str): The source path
        target (str): The target path
        variables (dict): The variables that are resolved, see example
        workspace_id (Optional[str]): Apply the changes in this workspace, or the user workspace if not set

    Returns:
        The newly created target folder 

    Example:
        >>> variables = {
        >>>     "[project]": "Commercial",
        >>>     "[client]": "Anchorpoint"
        >>> }
        >>> 
        >>> apsync.copy_file_from_template("C:/Templates/[client]_[project]", "C:/Projects/[client]_[project]", variables)
        "C:/Projects/Anchorpoint_Commercial"
    """


def copy_folder(source: str, target: str, overwrite: bool = False, workspace_id: typing.Optional[str] = None) -> None:
    """
    Copies the entire content from source_folder to the target_folder, including all metadata (e.g. attributes).
    Raises an exeception when overwrite is False and there is conflicts in the target_folder            

    Args:
        source_folder (str): The source folder
        target_folder (str): The target folder
        overwrite (bool): Overwrite target_folder
        workspace_id (Optional[str]): Apply the changes in this workspace, or the user workspace if not set

    Example:                
        >>> apsync.copy_folder(source, target, True)
    """


def copy_from_template(source: str, target: str, variables: typing.Dict[str, str] = {}, skip_root: bool = True, workspace_id: typing.Optional[str] = None) -> str:
    """
    Copies the source (template) folder to the target path and adjusts the metadata (e.g. attributes).
    Pass a python dictionary of variables that are applied to the folder and all the content of the folder.
    Raises an exeception when the target folder already exists, use os.remove to delete a folder.

    Args:
        source (str): The source path
        target (str): The target path
        variables (dict): The variables that are resolved, see example
        skip_root (bool): If true, the root folder of source is skipped
        workspace_id (Optional[str]): Apply the changes in this workspace, or the user workspace if not set

    Returns:
        The newly created target folder 

    Example:
        >>> variables = {
        >>>     "[project]": "Commercial",
        >>>     "[client]": "Anchorpoint"
        >>> }
        >>> 
        >>> apsync.copy_from_template("C:/Templates/[client]_[project]", "C:/Projects/[client]_[project]", variables)
        "C:/Projects/Anchorpoint_Commercial"
    """


def create_next_version(file_path: str) -> str:
    """
    Creates a new incremental version of file_path. 

    Args:
        file_path (str): The path to the file

    Returns:
        The path to the newly created file

    Example:
        >>> apsync.create_next_version("monkey_v001.blend")
        monkey_v002.blend
    """


def create_project(path: str, name: str, workspace_id: typing.Optional[str] = None) -> Project:
    """
    Creates a new Anchorpoint project at local_path with a given name.
    Usage is discouraged, please use :func:`~anchorpoint.Context.create_project` instead.

    Attributes:
        path (str): The path of the project root
        name (str): The name of the project
        workspace_id (Optional[str]): The workspace to create the project for, or None if the current user workspace should be used.

    Returns:
        The :class:`~apsync.Project`

    Example:                
        >>> project = apsync.create_project("C:/Projects/MyProject", "My Project")
        >>> print (project.name)
        >>> My Project
    """


def create_project_file(path: str, project_id: str, workspace_id: str) -> None:
    """
    Creates the project file (project.json) for an anchorpoint project. The project file is used to auto-detect that a given path belongs to a project.

    Attributes:
        path (str): The path of the project root
        project_id (str): The project id
        workspace_id (str): The workspace to create the project for
    """


def create_template(path: str, name: str, workspace_id: str, type: TemplateType) -> None:
    """
    Creates a new Anchorpoint template.

    Attributes:
        path (str): The path (folder or file) from where to create the template from
        name (str): The name of the template
        workspace_id (str): The workspace to create the template for
        type (apsync.TemplateType): The type of the template

    Example:                
        apsync.create_template("C:/Projects/MyProject", "My Project Template", ctx.workspace_id, apsync.TemplateType.Project)

    """


def export_project(project: Project, userIds: typing.Optional[typing.List[str]] = None) -> str:
    """
    Exports an anchorpoint project.

    Args:
        project (apsync.Project): The project to export

    Examples:
        >>> project = apsync.get_project(ctx.path)
        >>> if project:
        >>>     apsync.export_project(project)
    """


def generate_thumbnail(file_path: str, output_directory: str, with_preview: bool = True, with_detail: bool = False, workspace_id: typing.Optional[str] = None) -> bool:
    """
    Generates a thumbnail for file_path
    resulting in file_name_pt.png for preview thumbnail
    and file_name_dt.png for detail thumbnail
    in the given output directory

    Set with_preview = False to skip generating preview thumbnail
    Set with_detail = True to generate a detail thumbnail (not supported for all file formats)

    Args:
        file_path (str): Path to the file
        output_directory (str): output directory for preview and detail thumbnail
        with_preview (bool): generate preview image - default is True
        with_detail (bool): generate detail image if supported - default is False
        workspace_id: workspace id when files are not inside a project or None

    Returns:
        bool if generation was successful

    Example:
        >>> success = apsync.generate_thumbnail("some/path/my_scene.blend", "some/output/path")
    """


def generate_thumbnails(file_paths: typing.List[str], output_directory: str, with_preview: bool = True, with_detail: bool = False, workspace_id: typing.Optional[str] = None) -> typing.List[str]:
    """
    Generates thumbnails for multiple file_paths
    resulting in file_name_pt.png for each preview thumbnail
    and file_name_dt.png for each detail thumbnail
    in the given output directory

    Set with_preview = False to skip generating preview thumbnail
    Set with_detail = True to generate a detail thumbnail (not supported for all file formats)

    Args:
        file_paths (str): Path to the file (do not mix paths from multiple projects)
        output_directory (str): output directory for preview and detail thumbnail
        with_preview (bool): generate preview image - default is True
        with_detail (bool): generate detail image if supported - default is False
        workspace_id: workspace id when files are not inside a project or None

    Returns:
        list of file_paths for which generation was successful

    Example:
        >>> successful_file_paths = apsync.generate_thumbnails(list_of_paths, "some/output/path")
    """


def get_api() -> Api:
    """
    Returns the :class:`~apsync.Api` object. When writing an anchorpoint action use :func:`~anchorpoint.get_api` or, even better, :func:`~anchorpoint.get_context` instead
    """


def get_api_version() -> ApiVersion:
    """
    Returns the current version of the anchorpoint / apsync API.
    """


def get_attribute_checked(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> bool:
    """
    Retrieves the checkbox content of an attribute for a given file or folder.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Returns:
        The attribute checkbox value

    Example:                
        >>> value = apsync.get_attribute_checked("image.png", "status")
    """


def get_attribute_date(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> int:
    """
    Retrieves the date content of an attribute for a given file or folder. The date is in seconds since the epoch.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Returns:
        The attribute date value

    Example:                
        >>> value = apsync.get_attribute_date("image.png", "due date")
    """


def get_attribute_link(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> typing.Optional[str]:
    """
    Retrieves the link content of an attribute for a given file or folder.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Returns:
        The attribute link, or None.

    Example:                
        >>> value = apsync.get_attribute_link("image.png", "reference")
    """


def get_attribute_rating(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> int:
    """
    Retrieves the rating content of an attribute for a given file or folder.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Returns:
        The attribute rating.

    Example:                
        >>> value = apsync.get_attribute_rating("image.png", "rating")
    """


def get_attribute_tag(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> typing.Optional[AttributeTag]:
    """
    Retrieves the tag content of an attribute for a given file or folder.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Returns:
        The attribute tag, or None.

    Example:                
        >>> tag = apsync.get_attribute_tag("image.png", "status")
        >>> print(tag.name)
    """


def get_attribute_tags(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> typing.List[AttributeTag]:
    """
    Retrieves the list of assigned tags of a multiple or single choice tag.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Example:
        >>> tags = apsync.get_attribute_tags("image.png", "clients")
    """


def get_attribute_text(absolute_path: str, attribute_title: str, workspace_id: typing.Optional[str] = None) -> typing.Optional[str]:
    """
    Retrieves the text content of an attribute for a given file or folder.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute

    Returns:
        The attribute text, or None.

    Example:                
        >>> value = apsync.get_attribute_text("image.png", "status")
    """


def get_client_name() -> str:
    """
    Returns the name of the client
    """


def get_daemon_address() -> str:
    """
    Returns the address of the daemon
    """


def get_file_by_id(id: str, project: typing.Optional[Project] = None) -> typing.Optional[str]:
    pass


def get_file_id(arg0: str) -> str:
    pass


def get_folder_by_id(id: str, project: typing.Optional[Project] = None) -> typing.Optional[str]:
    pass


def get_folder_id(arg0: str) -> str:
    pass


def get_next_version_path(file_path: str) -> str:
    """
    Returns the path of the file that is used when running create new version

    Args:
        file_path (str): The path to the file

    Returns:
        The file path

    Example:
        >>> apsync.get_next_version_path("monkey_v001.blend")
        monkey_v002.blend
    """


def get_project(path: str) -> typing.Optional[Project]:
    """
    Returns the project (or None) for a given path (file or folder) by recursively checking for the hidden project.json on the file system.

    Args:
        path (str): Path to the file or folder

    Returns:
        The active project - or None

    Example:                
        >>> project = apsync.get_project("image.png")
        >>> print (project.name)
    """


def get_project_by_id(id: str, workspace_id: str) -> Project:
    """
    Returns the project given a project_id and workspace_id.

    Args:
        id (str): the project id
        workspace_id (str): the workspace id

    Returns:
        The project

    Example:                
        >>> project = apsync.get_project_by_id(ctx.project_id, ctx.workspace_id)
        >>> print (project.name)
    """


def get_project_members(arg0: str, arg1: str) -> GroupMemberList:
    pass


def get_projects(project: str) -> typing.List[Project]:
    """
    Returns a list of all projects in the workspace that you have access to.

    Attributes:
        workspace_id (str): The id of the workspace
    """


def get_server_url() -> str:
    """
    Returns address of the backend API
    """


def get_server_version() -> str:
    """
    Returns the version of the Anchorpoint backend
    """


def get_thumbnail(file_path: str, is_detail: bool) -> typing.Optional[str]:
    """
    Retrieves the thumbnail of file_path (or None).
    Set is_detail = False to retrieve the preview image - if any.

    Args:
        file_path (str): Path to the file
        is_detail (bool): Detail or preview image

    Returns:
        The path to the locally cached thumbnail - or None.

    Example:                
        >>> detail_thumbnail = apsync.get_thumbnail("my_scene.blend")
    """


def get_timeline_channel(project: Project, channelId: str) -> typing.Optional[TimelineChannel]:
    pass


def get_timeline_channels(project: Project) -> typing.List[TimelineChannel]:
    pass


def get_workspace_access(workspace_id: str) -> AccessLevel:
    """
    Checks the AccessLevel of the current user for the given workspace

    Args:
        workspace_id (str): The workspace id

    Examples:
        >>> level = apsync.get_workspace_access(ctx.workspace_id)
        >>> if level is apsync.AccessLevel.Admin:
        >>>     print("You have admin rights") 
    """


def get_workspace_members(workspace_id: str) -> WorkspaceMemberList:
    """
    Returns a list of all workspace members, including users that have a pending invite.
    """


def import_local(relative_path: str, reload: bool) -> object:
    """
    Utility that imports local python scripts and modules. Pass reload=True to invalidate the cache.
    This makes your script execute slower but greatly improves development because you don't have to restart Anchorpoint after altering the imported scripts.

    Args:
        relative_path (str): The relative folder path where the scripts are located
        reload (bool): Reloads the script and ignores caches.

    Examples:
        Import a file my_callbacks.py located in the folder callbacks
            >>> callbacks = apsync.import_local("callbacks/my_callbacks")
            >>> callbacks.foo()

        Import a local module utilities and use the script foo.py
            >>> util = apsync.import_local("utilities") 
            >>> util.foo.bar()

    """


def import_project(arg0: str, arg1: str, arg2: str) -> None:
    """
    Imports an anchorpoint project.
    """


def ipc_get_message(topic: str) -> typing.Optional[IpcMessage]:
    """
    Returns a new message or None if no message is available.
    """


def ipc_has_messages(topic: str) -> bool:
    """
    Checks if new messages are available.
    """


def ipc_publish(message: IpcMessage) -> None:
    """
    Publishes an IPC message
    """


def ipc_unsubscribe(topic: str) -> None:
    """
    Unsubscribes to all IPC messages given a subscription topic.
    """


def is_project(path: str, recursive: bool = False) -> bool:
    """
    Checks whether or not the given folder is a project. If recursive is True, the check is applied to all subfolders as well.

    Attributes:
        path (str): The path of the project
        recursive (bool): Apply the check to all subfolders also

    Returns:
        True if the path is a project

    Example:                
        >>> apsync.is_project("C:/Projects/MyProject")
        >>> True
    """


def remove_attribute_tag(absolute_path: str, attribute_title: str, tag_name: str, type: AttributeType = AttributeType(type='MultipleChoiceTag'), workspace_id: typing.Optional[str] = None) -> None:
    """
    Removes a tag identified by name.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        tag_name (str): The tag to add, identified by name
        type (apsync.AttributeType): The type of the attribute to create. Must be either apsync.AttributeType.multiple_choice_tag or apsync.AttributeType.single_choice_tag

    Example:                
        >>> apsync.remove_attribute_tag("image.png", "clients", "anchorpoint")
    """


def remove_project(project: Project) -> None:
    """
    Removes / Deletes a project on the workspace. Caution: This deletes the project for all users. Cannot be undone!

    Attributes:
        project (:class:`~apsync.Project`): The project that should be removed
    """


def remove_timeline_channel(project: Project, channelId: str) -> None:
    pass


def remove_user_from_project(arg0: str, arg1: str, arg2: str) -> None:
    pass


def remove_user_from_workspace(arg0: str, arg1: str) -> None:
    pass


def rename_file(source: str, target: str) -> None:
    """
    Renames the source file to the target name and adjusts the metadata (e.g. attributes).
    Raises an exeception when the target file already exists, use os.remove to delete a file.

    Args:
        source (str): The source file
        target (str): The target file

    Example:                
        >>> apsync.rename_file(source, target)
    """


def rename_folder(source: str, target: str) -> None:
    """
    Renames the source folder to the target name and adjusts the metadata (e.g. attributes).
    Raises an exeception when the target folder already exists, use os.remove to delete a folder.

    Args:
        source (str): The source file
        target (str): The target file

    Example:                
        >>> apsync.rename_folder(source, target)
    """


def resolve_variables(text: str, variables: typing.Dict[str, str]) -> str:
    """
    Takes an input text, e.g. "C:/Projects/[client]_[project]" and converts it to a resolved text, e.g. "C:/Projects/anchorpoint_commercial".
    Pass a python dictionary of variables where the values are the resolved variables (see example)

    Args:
        text (str): The source text
        variables (dict): The variables that are resolved, see example

    Returns:
        The text with resolved variables

    Example:
        >>> variables = {
        >>>     "[project]": "Commercial",
        >>>     "[client]": "Anchorpoint"
        >>> }
        >>> 
        >>> apsync.resolve_variables("C:/Projects/[client]_[project]", "C:/Projects/[client]_[project]", variables)
        "C:/Projects/Anchorpoint_Commercial"
    """


def set_account_email(arg0: str) -> None:
    """
    Sets the anchorpoint account email address

    Args:
        account (str): Your account email address
    """


def set_attribute_checked(absolute_path: str, attribute_title: str, checked: bool, workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Sets the checkbox content of an attribute for a given file or folder. 
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        checked (str): Check or uncheck the attribute
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.set_attribute_checked("image.png", "status", true)
    """


def set_attribute_date(absolute_path: str, attribute_title: str, secs_since_epoch: int, workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Sets the date content of an attribute for a given file or folder. The date is in seconds since the epoch.
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        secs_since_epoch (int): The date to set in seconds since the epoch
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.set_attribute_date("image.png", "due date", int(time.time()))
    """


def set_attribute_link(absolute_path: str, attribute_title: str, link: str, workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Sets the link content of an attribute for a given file or folder. A link can point to a website as well as to a file or folder.
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        link (str): The link to set
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.set_attribute_link("image.png", "status", "www.anchorpoint.app")
    """


def set_attribute_rating(absolute_path: str, attribute_title: str, rating: int, workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Sets the rating content of an attribute for a given file or folder. 
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        rating (int): The rating to set
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Returns:
        The attribute rating, or None.

    Example:                
        >>> apsync.set_attribute_rating("image.png", "rating", 3)
    """


def set_attribute_tag(absolute_path: str, attribute_title: str, tag_name: str, type: AttributeType = AttributeType(type='SingleChoiceTag'), workspace_id: typing.Optional[str] = None, auto_create: bool = True, tag_color: typing.Optional[TagColor] = None, update_timeline: bool = False) -> None:
    """
    Sets the tag of an attribute for a given file or folder. 
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        tag_name (str): The name of the tag to set - will create a new tag if unknown
        type (apsync.AttributeType): The type of attribute
        auto_create (bool): Automatically create the attribute if it not exists
        tag_color (apsync.TagColor): The color of the created tag - or None to choose a random color
        update_timeline (bool): True if the timeline should be notified about the update

    Example:
        >>> apsync.set_attribute_tag("image.png", "status", "Work in Progress")
    """


def set_attribute_tags(absolute_path: str, attribute_title: str, tag_names: typing.List[str], type: AttributeType = AttributeType(type='MultipleChoiceTag'), workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Sets the link content of an attribute for a given file or folder. Overwrites existing tags of the attribute.
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        tag_names (list): The tags to set, identified by name
        type (apsync.AttributeType): The type of the attribute to create. Must be either apsync.AttributeType.multiple_choice_tag or apsync.AttributeType.single_choice_tag
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.set_attribute_tags("image.png", "clients", ["anchorpoint", "john doe"])
    """


def set_attribute_text(absolute_path: str, attribute_title: str, text: str, workspace_id: typing.Optional[str] = None, auto_create: bool = True, update_timeline: bool = False) -> None:
    """
    Sets the text content of an attribute for a given file or folder. 
    Creates the attribute if it cannot be found and auto_create is true.

    Args:
        absolute_path (str): Path to the file or folder
        attribute_title (str): Title of the attribute
        text (str): The text to set
        auto_create (bool): Automatically create the attribute if it not exists
        update_timeline (bool): True if the timeline should be notified about the update

    Example:                
        >>> apsync.set_attribute_text("image.png", "status", "my super text")
    """


def set_client_name(arg0: str) -> None:
    """
    Sets the current client name (e.g. blender)

    Example:                
        >>> api = apsync.Api.instance()
        >>> api.set_client_name("blender")
    """


def set_daemon_address(arg0: str) -> None:
    """
    Sets the anchorpoint daemon / clientd address

    Args:
        address (str): The address
    """


def set_folder_icon(folder_path: str, icon: typing.Optional[Icon]) -> None:
    pass


def set_server_url(arg0: str) -> None:
    """
    Sets the anchorpoint server api url

    Args:
        url (str): The url to the Anchorpoint server
    """


def toggle_version_control(path: str, enable: bool) -> None:
    """
    Toggles incremental version control for the folder that 'path' points to. Pass enable=True to enable version control, pass enable=False to disable it.

    Args:
        path (str): The path to the folder
        enable (bool): Whether version control should be enabled or disabled

    Examples:
        Enable version control on a folder:
            >>> apsync.toggle_version_control("my/asset/folder", True)

        Check whether or not version control is enabled on a folder:
            >>> folder = apsync.get_folder("my/asset/folder")
            >>> if folder: print(folder.versioning_enabled)
            True
    """


def update_timeline_channel(project: Project, channel: TimelineChannel) -> None:
    pass
